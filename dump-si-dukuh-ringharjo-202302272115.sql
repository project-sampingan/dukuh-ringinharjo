-- MySQL dump 10.10
--
-- Host: localhost    Database: si-dukuh-ringharjo
-- ------------------------------------------------------
-- Server version	5.5.5-10.4.8-MariaDB

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `activities`
--

DROP TABLE IF EXISTS `activities`;
CREATE TABLE `activities` (
  `id` bigint(20) unsigned auto_increment,
  `inhabitant_id` bigint(20) unsigned,
  `nama_kegiatan` varchar(255),
  `status` varchar(255),
  `deskripsi` varchar(255),
  `created_at` timestamp,
  `updated_at` timestamp,
  PRIMARY KEY (`id`),
  KEY `activities_inhabitant_id_foreign` (`inhabitant_id`)
)/*! engine=InnoDB */;

--
-- Dumping data for table `activities`
--


/*!40000 ALTER TABLE `activities` DISABLE KEYS */;
LOCK TABLES `activities` WRITE;
UNLOCK TABLES;
/*!40000 ALTER TABLE `activities` ENABLE KEYS */;

--
-- Table structure for table `card_family`
--

DROP TABLE IF EXISTS `card_family`;
CREATE TABLE `card_family` (
  `id` bigint(20) unsigned auto_increment,
  `family_id` bigint(20) unsigned,
  `no_reg` varchar(255),
  `nama_anggota_keluarga` varchar(255),
  `status_dlm_keluarga` varchar(255),
  `status_dlm_perkawinan` varchar(255),
  `jenis_kelamin` varchar(255),
  `lahir_umur` date,
  `pendidikan` varchar(255),
  `pekerjaan` varchar(255),
  `created_at` timestamp,
  `updated_at` timestamp,
  PRIMARY KEY (`id`),
  KEY `card_family_family_id_foreign` (`family_id`)
)/*! engine=InnoDB */;

--
-- Dumping data for table `card_family`
--


/*!40000 ALTER TABLE `card_family` DISABLE KEYS */;
LOCK TABLES `card_family` WRITE;
UNLOCK TABLES;
/*!40000 ALTER TABLE `card_family` ENABLE KEYS */;

--
-- Table structure for table `failed_jobs`
--

DROP TABLE IF EXISTS `failed_jobs`;
CREATE TABLE `failed_jobs` (
  `id` bigint(20) unsigned auto_increment,
  `uuid` varchar(255),
  `connection` text,
  `queue` text,
  `payload` longtext,
  `exception` longtext,
  `failed_at` timestamp DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE `failed_jobs_uuid_unique` (`uuid`)
)/*! engine=InnoDB */;

--
-- Dumping data for table `failed_jobs`
--


/*!40000 ALTER TABLE `failed_jobs` DISABLE KEYS */;
LOCK TABLES `failed_jobs` WRITE;
UNLOCK TABLES;
/*!40000 ALTER TABLE `failed_jobs` ENABLE KEYS */;

--
-- Table structure for table `families`
--

DROP TABLE IF EXISTS `families`;
CREATE TABLE `families` (
  `id` bigint(20) unsigned auto_increment,
  `desa` varchar(255),
  `rt` int(11),
  `rw` int(11),
  `dusun` varchar(255),
  `kel` varchar(255),
  `kep` varchar(255),
  `kab` varchar(255),
  `prov` varchar(255),
  `nama_kk` varchar(255),
  `ttl_anggota` varchar(255),
  `jml_lk` int(11),
  `jml_pr` int(11),
  `jml_kk` int(11),
  `jml_balita` int(11),
  `jml_anak` int(11),
  `jml_pus` int(11),
  `jml_wus` int(11),
  `jml_buta` int(11),
  `jml_bumil` int(11),
  `jml_busu` int(11),
  `jml_lansia` int(11),
  `makanan_pokok` varchar(255),
  `jamban` varchar(255),
  `sumber_air` varchar(255),
  `pembuangan_sampah` varchar(255),
  `pembuangan_air_limbah` varchar(255),
  `stiker_p4k` varchar(255),
  `kriteria_rumah` varchar(255),
  `up2k` varchar(255),
  `kukl` varchar(255),
  `created_at` timestamp,
  `updated_at` timestamp,
  PRIMARY KEY (`id`)
)/*! engine=InnoDB */;

--
-- Dumping data for table `families`
--


/*!40000 ALTER TABLE `families` DISABLE KEYS */;
LOCK TABLES `families` WRITE;
UNLOCK TABLES;
/*!40000 ALTER TABLE `families` ENABLE KEYS */;

--
-- Table structure for table `inhabitants`
--

DROP TABLE IF EXISTS `inhabitants`;
CREATE TABLE `inhabitants` (
  `id` bigint(20) unsigned auto_increment,
  `desa` varchar(255),
  `nama_kk` varchar(255),
  `no_reg` varchar(255),
  `nik` bigint(20),
  `nama` varchar(255),
  `jabatan` varchar(255),
  `jenis_kelamin` varchar(255),
  `tempat_lahir` varchar(255),
  `tanggal_lahir` date,
  `status_kawin` varchar(255),
  `status_dlm_keluarga` varchar(255),
  `agama` varchar(255),
  `alamat` varchar(255),
  `pendidikan` varchar(255),
  `pekerjaan` varchar(255),
  `akseptor_kb` varchar(255),
  `posyandu` varchar(255),
  `bina_keluarga_balita` varchar(255),
  `paud` varchar(255),
  `koperasi` varchar(255),
  `created_at` timestamp,
  `updated_at` timestamp,
  PRIMARY KEY (`id`)
)/*! engine=InnoDB */;

--
-- Dumping data for table `inhabitants`
--


/*!40000 ALTER TABLE `inhabitants` DISABLE KEYS */;
LOCK TABLES `inhabitants` WRITE;
UNLOCK TABLES;
/*!40000 ALTER TABLE `inhabitants` ENABLE KEYS */;

--
-- Table structure for table `m_dasawisma`
--

DROP TABLE IF EXISTS `m_dasawisma`;
CREATE TABLE `m_dasawisma` (
  `id` int(11) auto_increment,
  `name` varchar(200),
  `created_at` datetime DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime,
  PRIMARY KEY (`id`)
)/*! engine=InnoDB */;

--
-- Dumping data for table `m_dasawisma`
--


/*!40000 ALTER TABLE `m_dasawisma` DISABLE KEYS */;
LOCK TABLES `m_dasawisma` WRITE;
INSERT INTO `m_dasawisma` VALUES (1,'Anggrek5','2023-02-27 13:41:01','2023-02-27 15:18:01');
UNLOCK TABLES;
/*!40000 ALTER TABLE `m_dasawisma` ENABLE KEYS */;

--
-- Table structure for table `migrations`
--

DROP TABLE IF EXISTS `migrations`;
CREATE TABLE `migrations` (
  `id` int(10) unsigned auto_increment,
  `migration` varchar(255),
  `batch` int(11),
  PRIMARY KEY (`id`)
)/*! engine=InnoDB */;

--
-- Dumping data for table `migrations`
--


/*!40000 ALTER TABLE `migrations` DISABLE KEYS */;
LOCK TABLES `migrations` WRITE;
INSERT INTO `migrations` VALUES (1,'2014_10_12_000000_create_users_table',1),(2,'2014_10_12_100000_create_password_resets_table',1),(3,'2014_10_12_200000_add_two_factor_columns_to_users_table',1),(4,'2019_08_19_000000_create_failed_jobs_table',1),(5,'2019_12_14_000001_create_personal_access_tokens_table',1),(6,'2023_01_08_014600_create_sessions_table',1),(7,'2023_01_08_090141_create_families_table',1),(8,'2023_01_10_152047_create_card_family_table',1),(9,'2023_01_23_193349_create_inhabitants_table',1),(10,'2023_01_23_193600_create_activities_table',1);
UNLOCK TABLES;
/*!40000 ALTER TABLE `migrations` ENABLE KEYS */;

--
-- Table structure for table `password_resets`
--

DROP TABLE IF EXISTS `password_resets`;
CREATE TABLE `password_resets` (
  `email` varchar(255),
  `token` varchar(255),
  `created_at` timestamp,
  KEY `password_resets_email_index` (`email`)
)/*! engine=InnoDB */;

--
-- Dumping data for table `password_resets`
--


/*!40000 ALTER TABLE `password_resets` DISABLE KEYS */;
LOCK TABLES `password_resets` WRITE;
UNLOCK TABLES;
/*!40000 ALTER TABLE `password_resets` ENABLE KEYS */;

--
-- Table structure for table `personal_access_tokens`
--

DROP TABLE IF EXISTS `personal_access_tokens`;
CREATE TABLE `personal_access_tokens` (
  `id` bigint(20) unsigned auto_increment,
  `tokenable_type` varchar(255),
  `tokenable_id` bigint(20) unsigned,
  `name` varchar(255),
  `token` varchar(64),
  `abilities` text,
  `last_used_at` timestamp,
  `expires_at` timestamp,
  `created_at` timestamp,
  `updated_at` timestamp,
  PRIMARY KEY (`id`),
  UNIQUE `personal_access_tokens_token_unique` (`token`),
  KEY `personal_access_tokens_tokenable_type_tokenable_id_index` (`tokenable_type`,`tokenable_id`)
)/*! engine=InnoDB */;

--
-- Dumping data for table `personal_access_tokens`
--


/*!40000 ALTER TABLE `personal_access_tokens` DISABLE KEYS */;
LOCK TABLES `personal_access_tokens` WRITE;
UNLOCK TABLES;
/*!40000 ALTER TABLE `personal_access_tokens` ENABLE KEYS */;

--
-- Table structure for table `sessions`
--

DROP TABLE IF EXISTS `sessions`;
CREATE TABLE `sessions` (
  `id` varchar(255),
  `user_id` bigint(20) unsigned,
  `ip_address` varchar(45),
  `user_agent` text,
  `payload` longtext,
  `last_activity` int(11),
  PRIMARY KEY (`id`),
  KEY `sessions_user_id_index` (`user_id`),
  KEY `sessions_last_activity_index` (`last_activity`)
)/*! engine=InnoDB */;

--
-- Dumping data for table `sessions`
--


/*!40000 ALTER TABLE `sessions` DISABLE KEYS */;
LOCK TABLES `sessions` WRITE;
INSERT INTO `sessions` VALUES ('4oJ2pzW8fSbMfY5wUrWtomVpATe0W6zjMW3KPBn7',1,'127.0.0.1','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/110.0.0.0 Safari/537.36','YTo2OntzOjY6Il90b2tlbiI7czo0MDoiY2RJMTlPcDJjbDlRUUh5VjdodUVtTVV3aTBtT0JnQmJhRDlMemFyNyI7czo5OiJfcHJldmlvdXMiO2E6MTp7czozOiJ1cmwiO3M6Mzc6Imh0dHA6Ly8xMjcuMC4wLjE6ODAwMC9tYXN0ZXJkYXNhd2lzbWEiO31zOjY6Il9mbGFzaCI7YToyOntzOjM6Im9sZCI7YTozNjp7aTowO3M6MTg6ImFsZXJ0LmNvbmZpZy50aXRsZSI7aToxO3M6MTc6ImFsZXJ0LmNvbmZpZy50ZXh0IjtpOjI7czoxODoiYWxlcnQuY29uZmlnLnRpbWVyIjtpOjM7czoxODoiYWxlcnQuY29uZmlnLndpZHRoIjtpOjQ7czoyMzoiYWxlcnQuY29uZmlnLmhlaWdodEF1dG8iO2k6NTtzOjIwOiJhbGVydC5jb25maWcucGFkZGluZyI7aTo2O3M6MzA6ImFsZXJ0LmNvbmZpZy5zaG93Q29uZmlybUJ1dHRvbiI7aTo3O3M6Mjg6ImFsZXJ0LmNvbmZpZy5zaG93Q2xvc2VCdXR0b24iO2k6ODtzOjI5OiJhbGVydC5jb25maWcudGltZXJQcm9ncmVzc0JhciI7aTo5O3M6MjQ6ImFsZXJ0LmNvbmZpZy5jdXN0b21DbGFzcyI7aToxMDtzOjE3OiJhbGVydC5jb25maWcuaWNvbiI7aToxMTtzOjEyOiJhbGVydC5jb25maWciO2k6MTI7czoxODoiYWxlcnQuY29uZmlnLnRpdGxlIjtpOjEzO3M6MTc6ImFsZXJ0LmNvbmZpZy50ZXh0IjtpOjE0O3M6MTg6ImFsZXJ0LmNvbmZpZy50aW1lciI7aToxNTtzOjE4OiJhbGVydC5jb25maWcud2lkdGgiO2k6MTY7czoyMDoiYWxlcnQuY29uZmlnLnBhZGRpbmciO2k6MTc7czozMDoiYWxlcnQuY29uZmlnLnNob3dDb25maXJtQnV0dG9uIjtpOjE4O3M6Mjg6ImFsZXJ0LmNvbmZpZy5zaG93Q2xvc2VCdXR0b24iO2k6MTk7czoyOToiYWxlcnQuY29uZmlnLnRpbWVyUHJvZ3Jlc3NCYXIiO2k6MjA7czoyNDoiYWxlcnQuY29uZmlnLmN1c3RvbUNsYXNzIjtpOjIxO3M6MTc6ImFsZXJ0LmNvbmZpZy5pY29uIjtpOjIyO3M6MTg6ImFsZXJ0LmNvbmZpZy50b2FzdCI7aToyMztzOjIxOiJhbGVydC5jb25maWcucG9zaXRpb24iO2k6MjQ7czoxMjoiYWxlcnQuY29uZmlnIjtpOjI1O3M6MTg6ImFsZXJ0LmNvbmZpZy50aXRsZSI7aToyNjtzOjE3OiJhbGVydC5jb25maWcudGV4dCI7aToyNztzOjMwOiJhbGVydC5jb25maWcuc2hvd0NvbmZpcm1CdXR0b24iO2k6Mjg7czoyOToiYWxlcnQuY29uZmlnLnRpbWVyUHJvZ3Jlc3NCYXIiO2k6Mjk7czoyNDoiYWxlcnQuY29uZmlnLmN1c3RvbUNsYXNzIjtpOjMwO3M6MTc6ImFsZXJ0LmNvbmZpZy5pY29uIjtpOjMxO3M6MTg6ImFsZXJ0LmNvbmZpZy50b2FzdCI7aTozMjtzOjE4OiJhbGVydC5jb25maWcudGltZXIiO2k6MzM7czoyMToiYWxlcnQuY29uZmlnLnBvc2l0aW9uIjtpOjM0O3M6Mjg6ImFsZXJ0LmNvbmZpZy5zaG93Q2xvc2VCdXR0b24iO2k6MzU7czoxMjoiYWxlcnQuY29uZmlnIjt9czozOiJuZXciO2E6MDp7fX1zOjUwOiJsb2dpbl93ZWJfNTliYTM2YWRkYzJiMmY5NDAxNTgwZjAxNGM3ZjU4ZWE0ZTMwOTg5ZCI7aToxO3M6MjE6InBhc3N3b3JkX2hhc2hfc2FuY3R1bSI7czo2MDoiJDJ5JDEwJE4ybG82Q2Nlbi5OOG9veFQucGExb2VmUXhlUmd5NTRxTGhReFBDTE5hb2xGajNMcnpuZTBxIjtzOjU6ImFsZXJ0IjthOjA6e319',1677488383);
UNLOCK TABLES;
/*!40000 ALTER TABLE `sessions` ENABLE KEYS */;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
CREATE TABLE `users` (
  `id` bigint(20) unsigned auto_increment,
  `name` varchar(255),
  `email` varchar(255),
  `email_verified_at` timestamp,
  `password` varchar(255),
  `two_factor_secret` text,
  `two_factor_recovery_codes` text,
  `two_factor_confirmed_at` timestamp,
  `remember_token` varchar(100),
  `current_team_id` bigint(20) unsigned,
  `profile_photo_path` varchar(2048),
  `created_at` timestamp,
  `updated_at` timestamp,
  PRIMARY KEY (`id`),
  UNIQUE `users_email_unique` (`email`)
)/*! engine=InnoDB */;

--
-- Dumping data for table `users`
--


/*!40000 ALTER TABLE `users` DISABLE KEYS */;
LOCK TABLES `users` WRITE;
INSERT INTO `users` VALUES (1,'Samsi Wahyudi','kknuad@gmail.com',NULL,'$2y$10$N2lo6Ccen.N8ooxT.pa1oefQxeRgy54qLhQxPCLNaolFj3Lrzne0q',NULL,NULL,NULL,'JSqkdL6GXqRGqrbfWwnwcDRrowSDL0CSQ4Vx2ltn59MSM2awuA1pnOph9KeT',NULL,NULL,'2023-02-27 03:42:16','2023-02-27 03:42:16');
UNLOCK TABLES;
/*!40000 ALTER TABLE `users` ENABLE KEYS */;

--
-- Dumping routines for database 'si-dukuh-ringharjo'
--
DELIMITER ;;
DELIMITER ;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

