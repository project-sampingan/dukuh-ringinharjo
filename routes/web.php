<?php

use App\Http\Controllers\MasterDasaWismaController;
use App\Http\Controllers\PendatangController;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\FamilyController;
use App\Http\Controllers\CardFamilyController;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\InhabitantController;
use App\Http\Controllers\ActivityController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
// URL::forceScheme('https');
Route::get('/', function () {
    return view('auth.login');
});

Route::middleware([
    'auth:sanctum',
    config('jetstream.auth_session'),
    'verified'
])->group(function () {
    Route::get('/dashboard', HomeController::class)->name('dashboard');

    Route::resource('families', FamilyController::class);

    Route::get('download/families', [FamilyController::class, 'download'])->name('families.download');
    Route::get('download/inhabitant', [InhabitantController::class, 'download'])->name('inhabitant.download');
    Route::get('download/pendatang', [PendatangController::class, 'download'])->name('pendatang.download');

    Route::get('families/{family}/cardfamily/create', [CardFamilyController::class, 'create'])->name('cardfamily.create');
    Route::post('families/{family}/cardfamily', [CardFamilyController::class, 'store'])->name('cardfamily.store');
    Route::get('families/{family}/cardfamily/{cardfamily}/edit', [CardFamilyController::class, 'edit'])->name('cardfamily.edit');
    Route::put('families/{family}/cardfamily/{cardfamily}', [CardFamilyController::class, 'update'])->name('cardfamily.update');
    Route::delete('families/{family}/cardfamily/{cardfamily}', [CardFamilyController::class, 'destroy'])->name('cardfamily.destroy');
    Route::get('families/{family}/cardfamily/{cardfamily}', [CardFamilyController::class, 'show'])->name('cardfamily.show');

    Route::resource('inhabitants', InhabitantController::class);

    Route::get('inhabitants/{inhabitant}/activity/create', [ActivityController::class, 'create'])->name('activity.create');
    Route::post('inhabitants/{inhabitant}/activity', [ActivityController::class, 'store'])->name('activity.store');
    Route::get('inhabitants/{inhabitant}/activity/{activity}', [ActivityController::class, 'show'])->name('activity.show');
    Route::get('inhabitants/{inhabitant}/activity/{activity}/edit', [ActivityController::class, 'edit'])->name('activity.edit');
    Route::put('inhabitants/{inhabitant}/activity/{activity}', [ActivityController::class, 'update'])->name('activity.update');
    Route::delete('inhabitants/{inhabitant}/activity/{activity}', [ActivityController::class, 'destroy'])->name('activity.destroy');

    Route::resource('masterdasawisma', MasterDasaWismaController::class);

    Route::resource('pendatang', PendatangController::class);
    // Route::get('pendatang/{pendatang}', PendatangController::class, 'detail')->name('pendatang.detail');

    // Route::get('/pendatang', PendatangController::class)->name('pendatang');
    Route::get('pendatang/create', [PendatangController::class, 'create'])->name('pendatang.create');
    Route::post('pendatang/store', [PendatangController::class, 'store'])->name('pendatang.store');
    Route::get('pendatang/edit/{pendatang}', [PendatangController::class, 'edit'])->name('pendatang.edit');
    Route::put('pendatang/update/{pendatang}', [PendatangController::class, 'update'])->name('pendatang.update');
});
