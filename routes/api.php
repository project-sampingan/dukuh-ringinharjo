<?php

use App\Http\Controllers\Api\KartuKeluargaApi;
use App\Http\Controllers\Api\WargaApi;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});

Route::get('warga/get_one', [WargaApi::class, 'get_one']);
Route::get('warga/by_nik', [WargaApi::class, 'by_nik']);
Route::get('warga/get_province', [WargaApi::class, 'get_province']);
Route::get('warga/get_kabupaten', [WargaApi::class, 'get_kabupaten']);
Route::get('warga/get_kecamatan', [WargaApi::class, 'get_kecamatan']);
Route::get('warga/get_kelurahan', [WargaApi::class, 'get_kelurahan']);
Route::post('warga/hapus_warga_pendatang', [WargaApi::class, 'hapus_warga_pendatang']);

Route::post('kartu_keluarga/tambah_anggota', [KartuKeluargaApi::class, 'tambah_anggota']);
