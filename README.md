# Doa Minta Rezeki yang Halal
Ya Allah, sesungguhnya aku memohon kepada-Mu agar melimpahkan rezeki kepadaku berupa rezeki yang halal, luas dan tanpa susah payah, tanpa memberatkan, tanpa membahayakan dan tanpa rasa lelah dalam memperolehnya. Sesunggunya Engkau berkuasa atas segala sesuatu.

<p align="center"><a href="https://github.com/firlanaluchiana/KKN" target="_blank"><img src="https://github.com/firlanaluchiana/KKN/blob/dev/public/voler/assets/images/kknuad.png" width="400" alt="Laravel Logo"></a></p>


# Installation

Aku sarankan pake laragon, karena aku pake laragon. Jadi kalo mau pake laragon, ikutin aja langkah-langkahnya.

- [Download Laragon Full](https://laragon.org/download/index.html)
- [Jika download project ZIP](https://github.com/firlanaluchiana/KKN)

## Clone Project
```shell
git clone git@github.com:firlanaluchiana/KKN.git

cd KKN

composer install

cp .env.example .env

```
## Jika copy manual copy file .env.example ke .env / rename .env.example menjadi .env

## lanjutkan dengan perintah berikut
```shell
npm install && npm run build
```
## lanjutkan dengan perintah berikut
```shell

php artisan key:generate

php artisan migrate:fresh

php artisan db:seed --class=UserSeeder

php artisan storage:link
```
