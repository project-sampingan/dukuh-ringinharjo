@section('title', 'Tambah Data Warga')
@extends('layouts.voler')
@section('content')
    <div id="main">
        <x-navbar></x-navbar>

        <div class="main-content container-fluid">
            <div class="page-title">
                <div class="row">
                    <div class="col-12 col-md-6 order-md-1 order-last">
                        <h3>@yield('title')</h3>
                    </div>
                    <div class="col-12 col-md-6 order-md-2 order-first">
                        <nav aria-label="breadcrumb" class='breadcrumb-header'>
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="{{ route('dashboard') }}">Dashboard</a></li>
                                <li class="breadcrumb-item active" aria-current="page">@yield('title')</li>
                            </ol>
                        </nav>
                    </div>
                </div>
            </div>
            <section id="basic-input-groups">
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-content">
                                <div class="card-body">
                                    <form action="{{ route('inhabitants.store') }}" method="POST">
                                        @csrf
                                        <div class="row">
                                            <div class="col-lg-12">
                                                <div class="form-group">
                                                    <label for="desa">Desa Wisma</label>
                                                    <!-- <input type="text" name="desa" id="desa"
                                                        class="form-control round" placeholder="Desa Wisma"> -->
                                                    <div class="input-group">
                                                        <select class="form-select round" id="desa"
                                                            name="desa">
                                                            <option selected>Pilih...</option>
                                                            @if (!empty($dasawisma))
                                                            @foreach ($dasawisma as $no => $row)
                                                                <option value="{{ $row->name }}">{{ $row->name }}</option>
                                                            @endforeach
                                                            @endif
                                                        </select>
                                                    </div>

                                                    @error('desa')
                                                        <div class="text-danger">{{ $message }}</div>
                                                    @enderror
                                                </div>
                                            </div>
                                            <div class="col-lg-12">
                                                <div class="form-group">
                                                    <!-- <label for="nama_kk">Nama Kepala Rumah Tangga</label> -->
                                                    <input type="hidden" name="nama_kk" id="nama_kk"
                                                        class="form-control round" placeholder="Nama Kepala Rumah Tangga">

                                                    @error('nama_kk')
                                                        <div class="text-danger">{{ $message }}</div>
                                                    @enderror
                                                </div>
                                            </div>
                                            <div class="col-lg-12">
                                                <div class="form-group">
                                                    <label for="no_reg">No. Registrasi</label>
                                                    <input type="number" name="no_reg" id="no_reg"
                                                        class="form-control round" placeholder="No. Registrasi">

                                                    @error('no_reg')
                                                        <div class="text-danger">{{ $message }}</div>
                                                    @enderror
                                                </div>
                                            </div>
                                            <div class="col-lg-12">
                                                <div class="form-group">
                                                    <label for="nik">No. KTP/NIK</label>
                                                    <input type="number" name="nik" id="nik"
                                                        class="form-control round" placeholder="No. KTP/NIK">

                                                    @error('nik')
                                                        <div class="text-danger">{{ $message }}</div>
                                                    @enderror
                                                </div>
                                            </div>
                                            <div class="col-lg-12">
                                                <div class="form-group">
                                                    <label for="nama">Nama</label>
                                                    <input type="text" name="nama" id="nama"
                                                        class="form-control round" placeholder="Nama">

                                                    @error('nama')
                                                        <div class="text-danger">{{ $message }}</div>
                                                    @enderror
                                                </div>
                                            </div>
                                            <div class="col-lg-12">
                                                <div class="form-group">
                                                    <label for="jabatan">Jabatan</label>
                                                    <input type="text" name="jabatan" id="jabatan"
                                                        class="form-control round" placeholder="Jabatan">

                                                    @error('jabatan')
                                                        <div class="text-danger">{{ $message }}</div>
                                                    @enderror
                                                </div>
                                            </div>
                                            <div class="col-lg-12">
                                                <div class="form-group">
                                                    <label for="jenis kelamin">Jenis Kelamin</label>
                                                    <div class="form-check">
                                                        <input class="form-check-input" type="radio" name="jenis_kelamin"
                                                            value="Laki-laki" id="lk">
                                                        <label class="form-check-label" for="lk">
                                                            Laki-laki
                                                        </label>
                                                    </div>
                                                    <div class="form-check">
                                                        <input class="form-check-input" type="radio" name="jenis_kelamin"
                                                            value="Perempuan" id="pr">
                                                        <label class="form-check-label" for="pr">
                                                            Perempuan
                                                        </label>
                                                    </div>

                                                    @error('jenis_kelamin')
                                                        <div class="text-danger">{{ $message }}</div>
                                                    @enderror
                                                </div>
                                            </div>
                                            <div class="col-lg-12">
                                                <div class="form-group">
                                                    <label for="tempat_lahir">Tempat Lahir</label>
                                                    <input type="text" name="tempat_lahir" id="tempat_lahir"
                                                        class="form-control round" placeholder="Tempat Lahir">

                                                    @error('tempat_lahir')
                                                        <div class="text-danger">{{ $message }}</div>
                                                    @enderror
                                                </div>
                                            </div>
                                            <div class="col-lg-12">
                                                <div class="form-group">
                                                    <label for="tanggal_lahir">Tanggal Lahir</label>
                                                    <input type="date" name="tanggal_lahir" id="tanggal_lahir"
                                                        class="form-control round" placeholder="Tanggal Lahir">

                                                    @error('tanggal_lahir')
                                                        <div class="text-danger">{{ $message }}</div>
                                                    @enderror
                                                </div>
                                            </div>
                                            <div class="col-lg-12">
                                                <div class="form-group">
                                                    <label for="status_dlm_perkawinan">Status Dalam Perkawinan</label>
                                                    <div class="input-group">
                                                        <select class="form-select round" id="status_dlm_perkawinan"
                                                            name="status_kawin">
                                                            <option selected>Pilih...</option>
                                                            <option value="Belum Kawin">Belum Kawin</option>
                                                            <option value="Kawin">Kawin</option>
                                                            <option value="Cerai Mati">Cerai Mati</option>
                                                            <option value="Cerai Hidup">Cerai Hidup</option>
                                                        </select>
                                                    </div>

                                                    @error('status_kawin')
                                                        <div class="text-danger">{{ $message }}</div>
                                                    @enderror
                                                </div>
                                            </div>
                                            <div class="col-lg-12">
                                                <div class="form-group">
                                                    <label for="status_dlm_keluarga">Status Dalam Keluarga</label>
                                                    <div class="form-check">
                                                        <input class="form-check-input" type="radio"
                                                            name="status_dlm_keluarga" value="Kepala Rumah Tangga"
                                                            id="kepala">
                                                        <label class="form-check-label" for="kepala">
                                                            Kepala Rumah Tangga
                                                        </label>
                                                    </div>
                                                    <div class="form-check">
                                                        <input class="form-check-input" type="radio"
                                                            name="status_dlm_keluarga" value="Anggota Keluarga"
                                                            id="anggota">
                                                        <label class="form-check-label" for="anggota">
                                                            Anggota Keluarga
                                                        </label>
                                                    </div>

                                                    @error('status_dlm_keluarga')
                                                        <div class="text-danger">{{ $message }}</div>
                                                    @enderror
                                                </div>
                                            </div>
                                            <div class="col-lg-12">
                                                <div class="form-group">
                                                    <label for="agama">Agama</label>
                                                    <div class="input-group">
                                                        <select class="form-select round" id="agama" name="agama">
                                                            <option selected>Pilih...</option>
                                                            <option value="Islam">Islam</option>
                                                            <option value="Kristen">Kristen</option>
                                                            <option value="Katolik">Katolik</option>
                                                            <option value="Budha">Budha</option>
                                                            <option value="Hindu">Hindu</option>
                                                            <option value="Konghucu">Konghucu</option>
                                                            <option value="Kepada Tuhan YME">Kepada Tuhan YME
                                                            </option>
                                                        </select>
                                                    </div>

                                                    @error('agama')
                                                        <div class="text-danger">{{ $message }}</div>
                                                    @enderror
                                                </div>
                                            </div>
                                            <div class="col-lg-12">
                                                <div class="form-group">
                                                    <label for="alamat">Alamat</label>
                                                    <input type="text" name="alamat" id="alamat"
                                                        class="form-control round" placeholder="Alamat">

                                                    @error('alamat')
                                                        <div class="text-danger">{{ $message }}</div>
                                                    @enderror
                                                </div>
                                            </div>
                                            <div class="col-lg-12">
                                                <div class="form-group">
                                                    <label for="pendidikan">Pendidikan</label>
                                                    <select class="form-select round" id="pendidikan" name="pendidikan">
                                                        <option selected>Pilih...</option>
                                                        <option value="Tidak Tamat SD">Tidak Tamat SD</option>
                                                        <option value="SD/MI">SD/MI</option>
                                                        <option value="SMP/Sederajat">SMP/Sederajat</option>
                                                        <option value="SMU/SMK/Sederajat">SMU/SMK/Sederajat</option>
                                                        <option value="Diploma">Diploma</option>
                                                        <option value="S1">S1</option>
                                                        <option value="S2">S2</option>
                                                        <option value="S3">S3</option>
                                                    </select>

                                                    @error('pendidikan')
                                                        <div class="text-danger">{{ $message }}</div>
                                                    @enderror
                                                </div>
                                            </div>
                                            <div class="col-lg-12">
                                                <div class="form-group">
                                                    <label for="pekerjaan">Pekerjaan</label>
                                                    <select class="form-select round" id="pekerjaan" name="pekerjaan">
                                                        <option selected>Pilih...</option>
                                                        <option value="Belum/Tidak Bekerja">Belum/Tidak Bekerja</option>
                                                        <option value="Pelajar/Mahasiswa">Pelajar/Mahasiswa</option>
                                                        <option value="Buruh Tidak Tetap">Buruh Tidak Tetap</option>
                                                        <option value="Buruh Tani">Buruh Tani</option>
                                                        <option value="Petani">Petani</option>
                                                        <option value="Pekerja Negeri Sipil (PNS)">Pekerja Negeri Sipil
                                                            (PNS)</option>
                                                        <option value="Guru">Guru</option>
                                                        <option value="Dosen">Dosen</option>
                                                        <option value="TNI/POLRI">TNI/POLRI</option>
                                                        <option value="Wirausaha">Wirausaha</option>
                                                        <option value="Wiraswasta">Wiraswasta</option>
                                                        <option value="Lainnya">Lainnya</option>
                                                    </select>

                                                    @error('pekerjaan')
                                                        <div class="text-danger">{{ $message }}</div>
                                                    @enderror
                                                </div>
                                            </div>
                                            <div class="col-lg-12">
                                                <div class="form-group">
                                                    <label for="akseptor_kb">Akseptor KB</label>
                                                    <input type="text" name="akseptor_kb" id="akseptor_kb"
                                                        class="form-control round" placeholder="Akseptor KB">

                                                    @error('akseptor_kb')
                                                        <div class="text-danger">{{ $message }}</div>
                                                    @enderror
                                                </div>
                                            </div>
                                            <div class="col-lg-12">
                                                <div class="form-group">
                                                    <label for="posyandu">Aktif Dalam Kegiatan Posyandu</label>
                                                    <!-- <input type="text" name="posyandu" id="posyandu"
                                                        class="form-control round"
                                                        placeholder="Aktif Dalam Kegiatan Posyandu"> -->

                                                    <div class="container">
                                                        <div class="row">
                                                            <div class="form-check col-md-4 col-12">
                                                                <input class="form-check-input" type="radio"
                                                                    name="posyandu" value="Ya" id="posyandu_ya" checked>
                                                                <label class="form-check-label" for="posyandu_ya">
                                                                    Ya
                                                                </label>
                                                            </div>

                                                            <div class="form-check col-md-4 col-12">
                                                                <input class="form-check-input" type="radio"
                                                                    name="posyandu" value="Tidak" id="posyandu_tidak">
                                                                <label class="form-check-label" for="posyandu_tidak">
                                                                    Tidak
                                                                </label>
                                                            </div>
                                                        </div>
                                                    </div>

                                                    @error('posyandu')
                                                        <div class="text-danger">{{ $message }}</div>
                                                    @enderror
                                                </div>
                                            </div>
                                            <div class="col-lg-12">
                                                <div class="form-group">
                                                    <label for="bina_keluarga_balita">Aktif Dalam Kegiatan Bina
                                                        Keluarga
                                                        Balita</label>
                                                    <!-- <input type="text" name="bina_keluarga_balita"
                                                        id="bina_keluarga_balita" class="form-control round"
                                                        placeholder="Aktif Dalam Kegiatan Bina Keluarga Balita"> -->

                                                    <div class="container">
                                                        <div class="row">
                                                            <div class="form-check col-md-4 col-12">
                                                                <input class="form-check-input" type="radio"
                                                                    name="bina_keluarga_balita" value="Ya" id="bina_keluarga_balita_ya" checked>
                                                                <label class="form-check-label" for="bina_keluarga_balita_ya">
                                                                    Ya
                                                                </label>
                                                            </div>

                                                            <div class="form-check col-md-4 col-12">
                                                                <input class="form-check-input" type="radio"
                                                                    name="bina_keluarga_balita" value="Tidak" id="bina_keluarga_balita_tidak">
                                                                <label class="form-check-label" for="bina_keluarga_balita_tidak">
                                                                    Tidak
                                                                </label>
                                                            </div>
                                                        </div>
                                                    </div>

                                                    @error('bina_keluarga_balita')
                                                        <div class="text-danger">{{ $message }}</div>
                                                    @enderror
                                                </div>
                                            </div>
                                            <div class="col-lg-12">
                                                <div class="form-group">
                                                    <label for="paud">Mengikuti PAUD/Sejenis</label>
                                                    <!-- <input type="text" name="paud" id="paud"
                                                        class="form-control round" placeholder="Mengikuti PAUD/Sejenis"> -->

                                                    <div class="container">
                                                        <div class="row">
                                                            <div class="form-check col-md-4 col-12">
                                                                <input class="form-check-input" type="radio"
                                                                    name="paud" value="Ya" id="paud_ya" checked>
                                                                <label class="form-check-label" for="paud_ya">
                                                                    Ya
                                                                </label>
                                                            </div>

                                                            <div class="form-check col-md-4 col-12">
                                                                <input class="form-check-input" type="radio"
                                                                    name="paud" value="Tidak" id="paud_tidak">
                                                                <label class="form-check-label" for="paud_tidak">
                                                                    Tidak
                                                                </label>
                                                            </div>
                                                        </div>
                                                    </div>

                                                    @error('paud')
                                                        <div class="text-danger">{{ $message }}</div>
                                                    @enderror
                                                </div>
                                            </div>
                                            <div class="col-lg-12">
                                                <div class="form-group">
                                                    <label for="koperasi">Ikut Dalam Kegiatan Koperasi</label>
                                                    <!-- <input type="text" name="koperasi" id="koperasi"
                                                        class="form-control round"
                                                        placeholder="Ikut Dalam Kegiatan Koperasi"> -->

                                                    <div class="container">
                                                        <div class="row">
                                                            <div class="form-check col-md-4 col-12">
                                                                <input class="form-check-input" type="radio"
                                                                    name="koperasi" value="Ya" id="koperasi_ya" checked>
                                                                <label class="form-check-label" for="koperasi_ya">
                                                                    Ya
                                                                </label>
                                                            </div>

                                                            <div class="form-check col-md-4 col-12">
                                                                <input class="form-check-input" type="radio"
                                                                    name="koperasi" value="Tidak" id="koperasi_tidak">
                                                                <label class="form-check-label" for="koperasi_tidak">
                                                                    Tidak
                                                                </label>
                                                            </div>
                                                        </div>
                                                    </div>

                                                    @error('koperasi')
                                                        <div class="text-danger">{{ $message }}</div>
                                                    @enderror
                                                </div>
                                            </div> 
                                            <button type="submit" class="btn btn-primary btn-block">Simpan</button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </div>
        <x-footer></x-footer>
    </div>
@endsection
