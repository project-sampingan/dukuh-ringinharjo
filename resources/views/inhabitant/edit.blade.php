@section('title', 'Edit Data Warga TP-PKK')
@extends('layouts.voler')
@section('content')
    <div id="main">
        <x-navbar></x-navbar>

        <div class="main-content container-fluid">
            <div class="page-title">
                <div class="row">
                    <div class="col-12 col-md-6 order-md-1 order-last">
                        <h4>@yield('title') {{ $inhabitant->nama }}</h4>
                    </div>
                    <div class="col-12 col-md-6 order-md-2 order-first">
                        <nav aria-label="breadcrumb" class='breadcrumb-header'>
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="{{ route('inhabitants.index') }}">Data Warga</a></li>
                                <li class="breadcrumb-item active" aria-current="page">@yield('title')</li>
                            </ol>
                        </nav>
                    </div>
                </div>
            </div>
            <section id="basic-input-groups">
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-content">
                                <div class="card-body">
                                    <form action="{{ route('inhabitants.update', $inhabitant->id) }}" method="POST">
                                        @method('PUT')
                                        @csrf
                                        <div class="row">
                                            <div class="col-lg-12">
                                                <div class="form-group">
                                                    <label for="desa">Desa Wisma</label>
                                                    <!-- <input type="text" name="desa" value="{{ $inhabitant->desa }}"
                                                        id="desa" class="form-control round" placeholder="Desa Wisma"> -->
                                                    <select class="form-control round" name="desa">
                                                        <option selected value="{{ $inhabitant->desa }}">{{ $inhabitant->desa }}</option>
                                                        @if (!empty($dasa_wisma))
                                                            @foreach($dasa_wisma as $no => $row)
                                                            <option value="{{ $row->name }}">{{ $row->name }}</option>
                                                            @endforeach
                                                        @endif
                                                    </select>

                                                    @error('desa')
                                                        <div class="text-danger">{{ $message }}</div>
                                                    @enderror
                                                </div>
                                            </div>
                                            <div class="col-lg-12">
                                                <div class="form-group">
                                                    <label for="nama_kk">Nama Kepala Rumah Tangga</label>
                                                    <input type="text" name="nama_kk" value="{{ $inhabitant->nama_kk }}"
                                                        id="nama_kk" class="form-control round"
                                                        placeholder="Nama Kepala Rumah Tangga">

                                                    @error('nama_kk')
                                                        <div class="text-danger">{{ $message }}</div>
                                                    @enderror
                                                </div>
                                            </div>
                                            <div class="col-lg-12">
                                                <div class="form-group">
                                                    <label for="no_reg">No. Registrasi</label>
                                                    <input type="text" name="no_reg" value="{{ $inhabitant->no_reg }}"
                                                        id="no_reg" class="form-control round"
                                                        placeholder="No. Registrasi">

                                                    @error('no_reg')
                                                        <div class="text-danger">{{ $message }}</div>
                                                    @enderror
                                                </div>
                                            </div>
                                            <div class="col-lg-12">
                                                <div class="form-group">
                                                    <label for="nik">No. KTP/NIK</label>
                                                    <input type="text" name="nik" value="{{ $inhabitant->nik }}"
                                                        id="nik" class="form-control round" placeholder="No. KTP/NIK">

                                                    @error('nik')
                                                        <div class="text-danger">{{ $message }}</div>
                                                    @enderror
                                                </div>
                                            </div>
                                            <div class="col-lg-12">
                                                <div class="form-group">
                                                    <label for="nama">Nama</label>
                                                    <input type="text" name="nama" value="{{ $inhabitant->nama }}"
                                                        id="nama" class="form-control round" placeholder="Nama">

                                                    @error('nama')
                                                        <div class="text-danger">{{ $message }}</div>
                                                    @enderror
                                                </div>
                                            </div>
                                            <div class="col-lg-12">
                                                <div class="form-group">
                                                    <label for="jabatan">Jabatan</label>
                                                    <input type="text" name="jabatan" value="{{ $inhabitant->jabatan }}"
                                                        id="jabatan" class="form-control round" placeholder="Jabatan">

                                                    @error('jabatan')
                                                        <div class="text-danger">{{ $message }}</div>
                                                    @enderror
                                                </div>
                                            </div>
                                            <div class="col-lg-12">
                                                <div class="form-group">
                                                    <label for="jenis kelamin">Jenis Kelamin</label>
                                                    @if ($inhabitant->jenis_kelamin == 'Laki-laki')
                                                        <div class="form-check">
                                                            <input class="form-check-input" type="radio"
                                                                name="jenis_kelamin" value="Laki-laki" id="lk"
                                                                checked>
                                                            <label class="form-check-label" for="lk">
                                                                Laki-laki
                                                            </label>
                                                        </div>
                                                        <div class="form-check">
                                                            <input class="form-check-input" type="radio"
                                                                name="jenis_kelamin" value="Perempuan" id="pr">
                                                            <label class="form-check-label" for="pr">
                                                                Perempuan
                                                            </label>
                                                        </div>
                                                    @elseif ($inhabitant->jenis_kelamin == 'Perempuan')
                                                        <div class="form-check">
                                                            <input class="form-check-input" type="radio"
                                                                name="jenis_kelamin" value="Laki-laki" id="lk">
                                                            <label class="form-check-label" for="lk">
                                                                Laki-laki
                                                            </label>
                                                        </div>
                                                        <div class="form-check">
                                                            <input class="form-check-input" type="radio"
                                                                name="jenis_kelamin" value="Perempuan" id="pr"
                                                                checked>
                                                            <label class="form-check-label" for="pr">
                                                                Perempuan
                                                            </label>
                                                        </div>
                                                    @endif

                                                    @error('jenis_kelamin')
                                                        <div class="text-danger">{{ $message }}</div>
                                                    @enderror
                                                </div>
                                            </div>
                                            <div class="col-lg-12">
                                                <div class="form-group">
                                                    <label for="tempat_lahir">Tempat Lahir</label>
                                                    <input type="text" name="tempat_lahir"
                                                        value="{{ $inhabitant->tempat_lahir }}" id="tempat_lahir"
                                                        class="form-control round" placeholder="Tempat Lahir">

                                                    @error('tempat_lahir')
                                                        <div class="text-danger">{{ $message }}</div>
                                                    @enderror
                                                </div>
                                            </div>
                                            <div class="col-lg-12">
                                                <div class="form-group">
                                                    <label for="tanggal_lahir">Tanggal Lahir</label>
                                                    <input type="date" name="tanggal_lahir"
                                                        value="{{ $inhabitant->tanggal_lahir }}" id="tanggal_lahir"
                                                        class="form-control round" placeholder="Tanggal Lahir">

                                                    @error('tanggal_lahir')
                                                        <div class="text-danger">{{ $message }}</div>
                                                    @enderror
                                                </div>
                                            </div>
                                            <div class="col-lg-12">
                                                <div class="form-group">
                                                    <label for="status_dlm_perkawinan">Status Dalam Perkawinan</label>
                                                    @if ($inhabitant->status_kawin == 'Belum Kawin')
                                                        <div class="input-group">
                                                            <select class="form-select round" id="status_dlm_perkawinan"
                                                                name="status_kawin">
                                                                <option value="Belum Kawin" selected>Belum Kawin</option>
                                                                <option value="Kawin">Kawin</option>
                                                                <option value="Cerai Mati">Cerai Mati</option>
                                                                <option value="Cerai Hidup">Cerai Hidup</option>
                                                            </select>
                                                        </div>
                                                    @elseif ($inhabitant->status_kawin == 'Kawin')
                                                        <div class="input-group">
                                                            <select class="form-select round" id="status_dlm_perkawinan"
                                                                name="status_kawin">
                                                                <option value="Belum Kawin">Belum Kawin</option>
                                                                <option value="Kawin" selected>Kawin</option>
                                                                <option value="Cerai Mati">Cerai Mati</option>
                                                                <option value="Cerai Hidup">Cerai Hidup</option>
                                                            </select>
                                                        </div>
                                                    @elseif ($inhabitant->status_kawin == 'Cerai Mati')
                                                        <div class="input-group">
                                                            <select class="form-select round" id="status_dlm_perkawinan"
                                                                name="status_kawin">
                                                                <option value="Belum Kawin">Belum Kawin</option>
                                                                <option value="Kawin">Kawin</option>
                                                                <option value="Cerai Mati" selected>Cerai Mati</option>
                                                                <option value="Cerai Hidup">Cerai Hidup</option>
                                                            </select>
                                                        </div>
                                                    @elseif ($inhabitant->status_kawin == 'Cerai Hidup')
                                                        <div class="input-group">
                                                            <select class="form-select round" id="status_dlm_perkawinan"
                                                                name="status_kawin">
                                                                <option value="Belum Kawin">Belum Kawin</option>
                                                                <option value="Kawin">Kawin</option>
                                                                <option value="Cerai Mati">Cerai Mati</option>
                                                                <option value="Cerai Hidup" selected>Cerai Hidup</option>
                                                            </select>
                                                        </div>
                                                    @endif

                                                    @error('status_kawin')
                                                        <div class="text-danger">{{ $message }}</div>
                                                    @enderror
                                                </div>
                                            </div>
                                            <div class="col-lg-12">
                                                <div class="form-group">
                                                    <label for="status_dlm_keluarga">Status Dalam Keluarga</label>
                                                    @if ($inhabitant->status_dlm_keluarga == 'Kepala Rumah Tangga')
                                                        <div class="form-check">
                                                            <input class="form-check-input" type="radio"
                                                                name="status_dlm_keluarga" value="Kepala Rumah Tangga"
                                                                id="kepala" checked>
                                                            <label class="form-check-label" for="kepala">
                                                                Kepala Rumah Tangga
                                                            </label>
                                                        </div>
                                                        <div class="form-check">
                                                            <input class="form-check-input" type="radio"
                                                                name="status_dlm_keluarga" value="Anggota Keluarga"
                                                                id="anggota">
                                                            <label class="form-check-label" for="anggota">
                                                                Anggota Keluarga
                                                            </label>
                                                        </div>
                                                    @elseif ($inhabitant->status_dlm_keluarga == 'Anggota Keluarga')
                                                        <div class="form-check">
                                                            <input class="form-check-input" type="radio"
                                                                name="status_dlm_keluarga" value="Kepala Rumah Tangga"
                                                                id="kepala">
                                                            <label class="form-check-label" for="kepala">
                                                                Kepala Rumah Tangga
                                                            </label>
                                                        </div>
                                                        <div class="form-check">
                                                            <input class="form-check-input" type="radio"
                                                                name="status_dlm_keluarga" value="Anggota Keluarga"
                                                                id="anggota" checked>
                                                            <label class="form-check-label" for="anggota">
                                                                Anggota Keluarga
                                                            </label>
                                                        </div>
                                                    @endif

                                                    @error('status_dlm_keluarga')
                                                        <div class="text-danger">{{ $message }}</div>
                                                    @enderror
                                                </div>
                                            </div>
                                            <div class="col-lg-12">
                                                <div class="form-group">
                                                    <label for="agama">Agama</label>
                                                    @if ($inhabitant->agama == 'Islam')
                                                        <div class="input-group">
                                                            <select class="form-select round" id="agama"
                                                                name="agama">
                                                                <option value="Islam" selected>Islam</option>
                                                                <option value="Kristen">Kristen</option>
                                                                <option value="Katolik">Katolik</option>
                                                                <option value="Budha">Budha</option>
                                                                <option value="Hindu">Hindu</option>
                                                                <option value="Konghucu">Konghucu</option>
                                                                <option value="Kepada Tuhan YME">Kepada Tuhan YME
                                                                </option>
                                                            </select>
                                                        </div>
                                                    @elseif($inhabitant->agama == 'Kristen')
                                                        <div class="input-group">
                                                            <select class="form-select round" id="agama"
                                                                name="agama">
                                                                <option value="Islam">Islam</option>
                                                                <option value="Kristen" selected>Kristen</option>
                                                                <option value="Katolik">Katolik</option>
                                                                <option value="Budha">Budha</option>
                                                                <option value="Hindu">Hindu</option>
                                                                <option value="Konghucu">Konghucu</option>
                                                                <option value="Kepada Tuhan YME">Kepada Tuhan YME
                                                                </option>
                                                            </select>
                                                        </div>
                                                    @elseif($inhabitant->agama == 'Katolik')
                                                        <div class="input-group">
                                                            <select class="form-select round" id="agama"
                                                                name="agama">
                                                                <option value="Islam">Islam</option>
                                                                <option value="Kristen">Kristen</option>
                                                                <option value="Katolik" selected>Katolik</option>
                                                                <option value="Budha">Budha</option>
                                                                <option value="Hindu">Hindu</option>
                                                                <option value="Konghucu">Konghucu</option>
                                                                <option value="Kepada Tuhan YME">Kepada Tuhan YME
                                                                </option>
                                                            </select>
                                                        </div>
                                                    @elseif($inhabitant->agama == 'Budha')
                                                        <div class="input-group">
                                                            <select class="form-select round" id="agama"
                                                                name="agama">
                                                                <option value="Islam">Islam</option>
                                                                <option value="Kristen">Kristen</option>
                                                                <option value="Katolik">Katolik</option>
                                                                <option value="Budha" selected>Budha</option>
                                                                <option value="Hindu">Hindu</option>
                                                                <option value="Konghucu">Konghucu</option>
                                                                <option value="Kepada Tuhan YME">Kepada Tuhan YME
                                                                </option>
                                                            </select>
                                                        </div>
                                                    @elseif($inhabitant->agama == 'Hindu')
                                                        <div class="input-group">
                                                            <select class="form-select round" id="agama"
                                                                name="agama">
                                                                <option value="Islam">Islam</option>
                                                                <option value="Kristen">Kristen</option>
                                                                <option value="Katolik">Katolik</option>
                                                                <option value="Budha">Budha</option>
                                                                <option value="Hindu" selected>Hindu</option>
                                                                <option value="Konghucu">Konghucu</option>
                                                                <option value="Kepada Tuhan YME">Kepada Tuhan YME
                                                                </option>
                                                            </select>
                                                        </div>
                                                    @elseif($inhabitant->agama == 'Konghucu')
                                                        <div class="input-group">
                                                            <select class="form-select round" id="agama"
                                                                name="agama">
                                                                <option value="Islam">Islam</option>
                                                                <option value="Kristen">Kristen</option>
                                                                <option value="Katolik">Katolik</option>
                                                                <option value="Budha">Budha</option>
                                                                <option value="Hindu">Hindu</option>
                                                                <option value="Konghucu" selected>Konghucu</option>
                                                                <option value="Kepada Tuhan YME">Kepada Tuhan YME
                                                                </option>
                                                            </select>
                                                        </div>
                                                    @elseif($inhabitant->agama == 'Kepada Tuhan YME')
                                                        <div class="input-group">
                                                            <select class="form-select round" id="agama"
                                                                name="agama">
                                                                <option value="Islam">Islam</option>
                                                                <option value="Kristen">Kristen</option>
                                                                <option value="Katolik">Katolik</option>
                                                                <option value="Budha">Budha</option>
                                                                <option value="Hindu">Hindu</option>
                                                                <option value="Konghucu">Konghucu</option>
                                                                <option value="Kepada Tuhan YME" selected>Kepada Tuhan
                                                                    YME</option>
                                                            </select>
                                                        </div>
                                                    @endif

                                                    @error('agama')
                                                        <div class="text-danger">{{ $message }}</div>
                                                    @enderror
                                                </div>
                                            </div>
                                            <div class="col-lg-12">
                                                <div class="form-group">
                                                    <label for="alamat">Alamat</label>
                                                    <input type="text" name="alamat"
                                                        value="{{ $inhabitant->alamat }}" id="alamat"
                                                        class="form-control round" placeholder="Alamat">

                                                    @error('alamat')
                                                        <div class="text-danger">{{ $message }}</div>
                                                    @enderror
                                                </div>
                                            </div>
                                            <div class="col-lg-12">
                                                <div class="form-group">
                                                    <label for="pendidikan">Pendidikan</label>
                                                    @if ($inhabitant->pendidikan == 'Tidak Tamat SD')
                                                        <select class="form-select round" id="pendidikan"
                                                            name="pendidikan">
                                                            <option value="Tidak Tamat SD" selected>Tidak Tamat SD</option>
                                                            <option value="SD/MI">SD/MI</option>
                                                            <option value="SMP/Sederajat">SMP/Sederajat</option>
                                                            <option value="SMU/SMK/Sederajat">SMU/SMK/Sederajat</option>
                                                            <option value="Diploma">Diploma</option>
                                                            <option value="S1">S1</option>
                                                            <option value="S2">S2</option>
                                                            <option value="S3">S3</option>
                                                        </select>
                                                    @elseif($inhabitant->pendidikan == 'SD/MI')
                                                        <select class="form-select round" id="pendidikan"
                                                            name="pendidikan">
                                                            <option value="Tidak Tamat SD">Tidak Tamat SD</option>
                                                            <option value="SD/MI" selected>SD/MI</option>
                                                            <option value="SMP/Sederajat">SMP/Sederajat</option>
                                                            <option value="SMU/SMK/Sederajat">SMU/SMK/Sederajat</option>
                                                            <option value="Diploma">Diploma</option>
                                                            <option value="S1">S1</option>
                                                            <option value="S2">S2</option>
                                                            <option value="S3">S3</option>
                                                        </select>
                                                    @elseif($inhabitant->pendidikan == 'SMP/Sederajat')
                                                        <select class="form-select round" id="pendidikan"
                                                            name="pendidikan">
                                                            <option value="Tidak Tamat SD">Tidak Tamat SD</option>
                                                            <option value="SD/MI">SD/MI</option>
                                                            <option value="SMP/Sederajat" selected>SMP/Sederajat</option>
                                                            <option value="SMU/SMK/Sederajat">SMU/SMK/Sederajat</option>
                                                            <option value="Diploma">Diploma</option>
                                                            <option value="S1">S1</option>
                                                            <option value="S2">S2</option>
                                                            <option value="S3">S3</option>
                                                        </select>
                                                    @elseif($inhabitant->pendidikan == 'SMU/SMK/Sederajat')
                                                        <select class="form-select round" id="pendidikan"
                                                            name="pendidikan">
                                                            <option value="Tidak Tamat SD">Tidak Tamat SD</option>
                                                            <option value="SD/MI">SD/MI</option>
                                                            <option value="SMP/Sederajat">SMP/Sederajat</option>
                                                            <option value="SMU/SMK/Sederajat" selected>SMU/SMK/Sederajat
                                                            </option>
                                                            <option value="Diploma">Diploma</option>
                                                            <option value="S1">S1</option>
                                                            <option value="S2">S2</option>
                                                            <option value="S3">S3</option>
                                                        </select>
                                                    @elseif($inhabitant->pendidikan == 'Diploma')
                                                        <select class="form-select round" id="pendidikan"
                                                            name="pendidikan">
                                                            <option value="Tidak Tamat SD">Tidak Tamat SD</option>
                                                            <option value="SD/MI">SD/MI</option>
                                                            <option value="SMP/Sederajat">SMP/Sederajat</option>
                                                            <option value="SMU/SMK/Sederajat">SMU/SMK/Sederajat</option>
                                                            <option value="Diploma" selected>Diploma</option>
                                                            <option value="S1">S1</option>
                                                            <option value="S2">S2</option>
                                                            <option value="S3">S3</option>
                                                        </select>
                                                    @elseif($inhabitant->pendidikan == 'S1')
                                                        <select class="form-select round" id="pendidikan"
                                                            name="pendidikan">
                                                            <option value="Tidak Tamat SD">Tidak Tamat SD</option>
                                                            <option value="SD/MI">SD/MI</option>
                                                            <option value="SMP/Sederajat">SMP/Sederajat</option>
                                                            <option value="SMU/SMK/Sederajat">SMU/SMK/Sederajat</option>
                                                            <option value="Diploma">Diploma</option>
                                                            <option value="S1" selected>S1</option>
                                                            <option value="S2">S2</option>
                                                            <option value="S3">S3</option>
                                                        </select>
                                                    @elseif($inhabitant->pendidikan == 'S2')
                                                        <select class="form-select round" id="pendidikan"
                                                            name="pendidikan">
                                                            <option value="Tidak Tamat SD">Tidak Tamat SD</option>
                                                            <option value="SD/MI">SD/MI</option>
                                                            <option value="SMP/Sederajat">SMP/Sederajat</option>
                                                            <option value="SMU/SMK/Sederajat">SMU/SMK/Sederajat</option>
                                                            <option value="Diploma">Diploma</option>
                                                            <option value="S1">S1</option>
                                                            <option value="S2" selected>S2</option>
                                                            <option value="S3">S3</option>
                                                        </select>
                                                    @elseif($inhabitant->pendidikan == 'S3')
                                                        <select class="form-select round" id="pendidikan"
                                                            name="pendidikan">
                                                            <option value="Tidak Tamat SD">Tidak Tamat SD</option>
                                                            <option value="SD/MI">SD/MI</option>
                                                            <option value="SMP/Sederajat">SMP/Sederajat</option>
                                                            <option value="SMU/SMK/Sederajat">SMU/SMK/Sederajat</option>
                                                            <option value="Diploma">Diploma</option>
                                                            <option value="S1">S1</option>
                                                            <option value="S2">S2</option>
                                                            <option value="S3" selected>S3</option>
                                                        </select>
                                                    @endif

                                                    @error('pendidikan')
                                                        <div class="text-danger">{{ $message }}</div>
                                                    @enderror
                                                </div>
                                            </div>
                                            <div class="col-lg-12">
                                                <div class="form-group">
                                                    <label for="pekerjaan">Pekerjaan</label>
                                                    @if ($inhabitant->pekerjaan == 'Belum/Tidak Bekerja')
                                                        <select class="form-select round" id="pekerjaan"
                                                            name="pekerjaan">
                                                            <option selected value="Belum/Tidak Bekerja">Belum/Tidak
                                                                Bekerja</option>
                                                            <option value="Pelajar/Mahasiswa">Pelajar/Mahasiswa</option>
                                                            <option value="Buruh Tidak Tetap">Buruh Tidak Tetap</option>
                                                            <option value="Buruh Tani">Buruh Tani</option>
                                                            <option value="Petani">Petani</option>
                                                            <option value="Pekerja Negeri Sipil (PNS)">Pekerja Negeri Sipil
                                                                (PNS)</option>
                                                            <option value="Guru">Guru</option>
                                                            <option value="Dosen">Dosen</option>
                                                            <option value="TNI/POLRI">TNI/POLRI</option>
                                                            <option value="Wirausaha">Wirausaha</option>
                                                            <option value="Wiraswasta">Wiraswasta</option>
                                                            <option value="Lainnya">Lainnya</option>
                                                        </select>
                                                    @elseif($inhabitant->pekerjaan == 'Pelajar/Mahasiswa')
                                                        <select class="form-select round" id="pekerjaan"
                                                            name="pekerjaan">
                                                            <option value="Belum/Tidak Bekerja">Belum/Tidak Bekerja
                                                            </option>
                                                            <option selected value="Pelajar/Mahasiswa">Pelajar/Mahasiswa
                                                            </option>
                                                            <option value="Buruh Tidak Tetap">Buruh Tidak Tetap</option>
                                                            <option value="Buruh Tani">Buruh Tani</option>
                                                            <option value="Petani">Petani</option>
                                                            <option value="Pekerja Negeri Sipil (PNS)">Pekerja Negeri Sipil
                                                                (PNS)</option>
                                                            <option value="Guru">Guru</option>
                                                            <option value="Dosen">Dosen</option>
                                                            <option value="TNI/POLRI">TNI/POLRI</option>
                                                            <option value="Wirausaha">Wirausaha</option>
                                                            <option value="Wiraswasta">Wiraswasta</option>
                                                            <option value="Lainnya">Lainnya</option>
                                                        </select>
                                                    @elseif($inhabitant->pekerjaan == 'Buruh Tidak Tetap')
                                                        <select class="form-select round" id="pekerjaan"
                                                            name="pekerjaan">
                                                            <option value="Belum/Tidak Bekerja">Belum/Tidak Bekerja
                                                            </option>
                                                            <option value="Pelajar/Mahasiswa">Pelajar/Mahasiswa</option>
                                                            <option selected value="Buruh Tidak Tetap">Buruh Tidak Tetap
                                                            </option>
                                                            <option value="Buruh Tani">Buruh Tani</option>
                                                            <option value="Petani">Petani</option>
                                                            <option value="Pekerja Negeri Sipil (PNS)">Pekerja Negeri Sipil
                                                                (PNS)</option>
                                                            <option value="Guru">Guru</option>
                                                            <option value="Dosen">Dosen</option>
                                                            <option value="TNI/POLRI">TNI/POLRI</option>
                                                            <option value="Wirausaha">Wirausaha</option>
                                                            <option value="Wiraswasta">Wiraswasta</option>
                                                            <option value="Lainnya">Lainnya</option>
                                                        </select>
                                                    @elseif($inhabitant->pekerjaan == 'Buruh Tani')
                                                        <select class="form-select round" id="pekerjaan"
                                                            name="pekerjaan">
                                                            <option value="Belum/Tidak Bekerja">Belum/Tidak Bekerja
                                                            </option>
                                                            <option value="Pelajar/Mahasiswa">Pelajar/Mahasiswa</option>
                                                            <option value="Buruh Tidak Tetap">Buruh Tidak Tetap</option>
                                                            <option selected value="Buruh Tani">Buruh Tani</option>
                                                            <option value="Petani">Petani</option>
                                                            <option value="Pekerja Negeri Sipil (PNS)">Pekerja Negeri Sipil
                                                                (PNS)</option>
                                                            <option value="Guru">Guru</option>
                                                            <option value="Dosen">Dosen</option>
                                                            <option value="TNI/POLRI">TNI/POLRI</option>
                                                            <option value="Wirausaha">Wirausaha</option>
                                                            <option value="Wiraswasta">Wiraswasta</option>
                                                            <option value="Lainnya">Lainnya</option>
                                                        </select>
                                                    @elseif($inhabitant->pekerjaan == 'Petani')
                                                        <select class="form-select round" id="pekerjaan"
                                                            name="pekerjaan">
                                                            <option value="Belum/Tidak Bekerja">Belum/Tidak Bekerja
                                                            </option>
                                                            <option value="Pelajar/Mahasiswa">Pelajar/Mahasiswa</option>
                                                            <option value="Buruh Tidak Tetap">Buruh Tidak Tetap</option>
                                                            <option value="Buruh Tani">Buruh Tani</option>
                                                            <option selected value="Petani">Petani</option>
                                                            <option value="Pekerja Negeri Sipil (PNS)">Pekerja Negeri Sipil
                                                                (PNS)</option>
                                                            <option value="Guru">Guru</option>
                                                            <option value="Dosen">Dosen</option>
                                                            <option value="TNI/POLRI">TNI/POLRI</option>
                                                            <option value="Wirausaha">Wirausaha</option>
                                                            <option value="Wiraswasta">Wiraswasta</option>
                                                            <option value="Lainnya">Lainnya</option>
                                                        </select>
                                                    @elseif($inhabitant->pekerjaan == 'Pekerja Negeri Sipil (PNS)')
                                                        <select class="form-select round" id="pekerjaan"
                                                            name="pekerjaan">
                                                            <option value="Belum/Tidak Bekerja">Belum/Tidak Bekerja
                                                            </option>
                                                            <option value="Pelajar/Mahasiswa">Pelajar/Mahasiswa</option>
                                                            <option value="Buruh Tidak Tetap">Buruh Tidak Tetap</option>
                                                            <option value="Buruh Tani">Buruh Tani</option>
                                                            <option value="Petani">Petani</option>
                                                            <option selected value="Pekerja Negeri Sipil (PNS)">Pekerja
                                                                Negeri Sipil (PNS)</option>
                                                            <option value="Guru">Guru</option>
                                                            <option value="Dosen">Dosen</option>
                                                            <option value="TNI/POLRI">TNI/POLRI</option>
                                                            <option value="Wirausaha">Wirausaha</option>
                                                            <option value="Wiraswasta">Wiraswasta</option>
                                                            <option value="Lainnya">Lainnya</option>
                                                        </select>
                                                    @elseif($inhabitant->pekerjaan == 'Guru')
                                                        <select class="form-select round" id="pekerjaan"
                                                            name="pekerjaan">
                                                            <option value="Belum/Tidak Bekerja">Belum/Tidak Bekerja
                                                            </option>
                                                            <option value="Pelajar/Mahasiswa">Pelajar/Mahasiswa</option>
                                                            <option value="Buruh Tidak Tetap">Buruh Tidak Tetap</option>
                                                            <option value="Buruh Tani">Buruh Tani</option>
                                                            <option value="Petani">Petani</option>
                                                            <option value="Pekerja Negeri Sipil (PNS)">Pekerja Negeri Sipil
                                                                (PNS)</option>
                                                            <option selected value="Guru">Guru</option>
                                                            <option value="Dosen">Dosen</option>
                                                            <option value="TNI/POLRI">TNI/POLRI</option>
                                                            <option value="Wirausaha">Wirausaha</option>
                                                            <option value="Wiraswasta">Wiraswasta</option>
                                                            <option value="Lainnya">Lainnya</option>
                                                        </select>
                                                    @elseif($inhabitant->pekerjaan == 'Dosen')
                                                        <select class="form-select round" id="pekerjaan"
                                                            name="pekerjaan">
                                                            <option value="Belum/Tidak Bekerja">Belum/Tidak Bekerja
                                                            </option>
                                                            <option value="Pelajar/Mahasiswa">Pelajar/Mahasiswa</option>
                                                            <option value="Buruh Tidak Tetap">Buruh Tidak Tetap</option>
                                                            <option value="Buruh Tani">Buruh Tani</option>
                                                            <option value="Petani">Petani</option>
                                                            <option value="Pekerja Negeri Sipil (PNS)">Pekerja Negeri Sipil
                                                                (PNS)</option>
                                                            <option value="Guru">Guru</option>
                                                            <option selected value="Dosen">Dosen</option>
                                                            <option value="TNI/POLRI">TNI/POLRI</option>
                                                            <option value="Wirausaha">Wirausaha</option>
                                                            <option value="Wiraswasta">Wiraswasta</option>
                                                            <option value="Lainnya">Lainnya</option>
                                                        </select>
                                                    @elseif($inhabitant->pekerjaan == 'TNI/POLRI')
                                                        <select class="form-select round" id="pekerjaan"
                                                            name="pekerjaan">
                                                            <option value="Belum/Tidak Bekerja">Belum/Tidak Bekerja
                                                            </option>
                                                            <option value="Pelajar/Mahasiswa">Pelajar/Mahasiswa</option>
                                                            <option value="Buruh Tidak Tetap">Buruh Tidak Tetap</option>
                                                            <option value="Buruh Tani">Buruh Tani</option>
                                                            <option value="Petani">Petani</option>
                                                            <option value="Pekerja Negeri Sipil (PNS)">Pekerja Negeri Sipil
                                                                (PNS)</option>
                                                            <option value="Guru">Guru</option>
                                                            <option value="Dosen">Dosen</option>
                                                            <option selected value="TNI/POLRI">TNI/POLRI</option>
                                                            <option value="Wirausaha">Wirausaha</option>
                                                            <option value="Wiraswasta">Wiraswasta</option>
                                                            <option value="Lainnya">Lainnya</option>
                                                        </select>
                                                    @elseif($inhabitant->pekerjaan == 'Wirausaha')
                                                        <select class="form-select round" id="pekerjaan"
                                                            name="pekerjaan">
                                                            <option value="Belum/Tidak Bekerja">Belum/Tidak Bekerja
                                                            </option>
                                                            <option value="Pelajar/Mahasiswa">Pelajar/Mahasiswa</option>
                                                            <option value="Buruh Tidak Tetap">Buruh Tidak Tetap</option>
                                                            <option value="Buruh Tani">Buruh Tani</option>
                                                            <option value="Petani">Petani</option>
                                                            <option value="Pekerja Negeri Sipil (PNS)">Pekerja Negeri Sipil
                                                                (PNS)</option>
                                                            <option value="Guru">Guru</option>
                                                            <option value="Dosen">Dosen</option>
                                                            <option value="TNI/POLRI">TNI/POLRI</option>
                                                            <option selected value="Wirausaha">Wirausaha</option>
                                                            <option value="Wiraswasta">Wiraswasta</option>
                                                            <option value="Lainnya">Lainnya</option>
                                                        </select>
                                                    @elseif($inhabitant->pekerjaan == 'Wiraswasta')
                                                        <select class="form-select round" id="pekerjaan"
                                                            name="pekerjaan">
                                                            <option value="Belum/Tidak Bekerja">Belum/Tidak Bekerja
                                                            </option>
                                                            <option value="Pelajar/Mahasiswa">Pelajar/Mahasiswa</option>
                                                            <option value="Buruh Tidak Tetap">Buruh Tidak Tetap</option>
                                                            <option value="Buruh Tani">Buruh Tani</option>
                                                            <option value="Petani">Petani</option>
                                                            <option value="Pekerja Negeri Sipil (PNS)">Pekerja Negeri Sipil
                                                                (PNS)</option>
                                                            <option value="Guru">Guru</option>
                                                            <option value="Dosen">Dosen</option>
                                                            <option value="TNI/POLRI">TNI/POLRI</option>
                                                            <option value="Wirausaha">Wirausaha</option>
                                                            <option selected value="Wiraswasta">Wiraswasta</option>
                                                            <option value="Lainnya">Lainnya</option>
                                                        </select>
                                                    @elseif($inhabitant->pekerjaan == 'Lainnya')
                                                        <select class="form-select round" id="pekerjaan"
                                                            name="pekerjaan">
                                                            <option value="Belum/Tidak Bekerja">Belum/Tidak Bekerja
                                                            </option>
                                                            <option value="Pelajar/Mahasiswa">Pelajar/Mahasiswa</option>
                                                            <option value="Buruh Tidak Tetap">Buruh Tidak Tetap</option>
                                                            <option value="Buruh Tani">Buruh Tani</option>
                                                            <option value="Petani">Petani</option>
                                                            <option value="Pekerja Negeri Sipil (PNS)">Pekerja Negeri Sipil
                                                                (PNS)</option>
                                                            <option value="Guru">Guru</option>
                                                            <option value="Dosen">Dosen</option>
                                                            <option value="TNI/POLRI">TNI/POLRI</option>
                                                            <option value="Wirausaha">Wirausaha</option>
                                                            <option value="Wiraswasta">Wiraswasta</option>
                                                            <option selected value="Lainnya">Lainnya</option>
                                                        </select>
                                                    @endif

                                                    @error('pekerjaan')
                                                        <div class="text-danger">{{ $message }}</div>
                                                    @enderror
                                                </div>
                                            </div>
                                            <div class="col-lg-12">
                                                <div class="form-group">
                                                    <label for="akseptor_kb">Akseptor KB</label>
                                                    <input type="text" name="akseptor_kb"
                                                        value="{{ $inhabitant->akseptor_kb }}" id="akseptor_kb"
                                                        class="form-control round" placeholder="Akseptor KB">

                                                    @error('akseptor_kb')
                                                        <div class="text-danger">{{ $message }}</div>
                                                    @enderror
                                                </div>
                                            </div>
                                            <div class="col-lg-12">
                                                <div class="form-group">
                                                    <label for="posyandu">Aktif Dalam Kegiatan Posyandu</label>
                                                    <!-- <input type="text" name="posyandu"
                                                        value="{{ $inhabitant->posyandu }}" id="posyandu"
                                                        class="form-control round"
                                                        placeholder="Aktif Dalam Kegiatan Posyandu"> -->

                                                    <div class="container">
                                                        <div class="row">
                                                            <div class="form-check col-md-4 col-12">
                                                                <input class="form-check-input" type="radio"
                                                                    name="posyandu" value="Ya" id="posyandu_ya" {{ ($inhabitant->posyandu == 'Ya') ? 'checked' : '' }}>
                                                                <label class="form-check-label" for="posyandu_ya">
                                                                    Ya
                                                                </label>
                                                            </div>

                                                            <div class="form-check col-md-4 col-12">
                                                                <input class="form-check-input" type="radio"
                                                                    name="posyandu" value="Tidak" id="posyandu_tidak" {{ ($inhabitant->posyandu == 'Tidak') ? 'checked' : '' }}>
                                                                <label class="form-check-label" for="posyandu_tidak">
                                                                    Tidak
                                                                </label>
                                                            </div>
                                                        </div>
                                                    </div>

                                                    @error('posyandu')
                                                        <div class="text-danger">{{ $message }}</div>
                                                    @enderror
                                                </div>
                                            </div>
                                            <div class="col-lg-12">
                                                <div class="form-group">
                                                    <label for="bina_keluarga_balita">Aktif Dalam Kegiatan Bina
                                                        Keluarga
                                                        Balita</label>
                                                    <!-- <input type="text" name="bina_keluarga_balita"
                                                        value="{{ $inhabitant->bina_keluarga_balita }}"
                                                        id="bina_keluarga_balita" class="form-control round"
                                                        placeholder="Aktif Dalam Kegiatan Bina Keluarga Balita"> -->

                                                    <div class="container">
                                                        <div class="row">
                                                            <div class="form-check col-md-4 col-12">
                                                                <input class="form-check-input" type="radio"
                                                                    name="bina_keluarga_balita" value="Ya" id="bina_keluarga_balita_ya" {{ ($inhabitant->bina_keluarga_balita == 'Ya') ? 'checked' : '' }}>
                                                                <label class="form-check-label" for="bina_keluarga_balita_ya">
                                                                    Ya
                                                                </label>
                                                            </div>

                                                            <div class="form-check col-md-4 col-12">
                                                                <input class="form-check-input" type="radio"
                                                                    name="bina_keluarga_balita" value="Tidak" id="bina_keluarga_balita_tidak" {{ ($inhabitant->bina_keluarga_balita == 'Tidak') ? 'checked' : '' }}>
                                                                <label class="form-check-label" for="bina_keluarga_balita_tidak">
                                                                    Tidak
                                                                </label>
                                                            </div>
                                                        </div>
                                                    </div>

                                                    @error('bina_keluarga_balita')
                                                        <div class="text-danger">{{ $message }}</div>
                                                    @enderror
                                                </div>
                                            </div>
                                            <div class="col-lg-12">
                                                <div class="form-group">
                                                    <label for="paud">Mengikuti PAUD/Sejenis</label>
                                                    <!-- <input type="text" name="paud" value="{{ $inhabitant->paud }}"
                                                        id="paud" class="form-control round"
                                                        placeholder="Mengikuti PAUD/Sejenis"> -->

                                                    <div class="container">
                                                        <div class="row">
                                                            <div class="form-check col-md-4 col-12">
                                                                <input class="form-check-input" type="radio"
                                                                    name="paud" value="Ya" id="paud_ya" {{ ($inhabitant->paud == 'Ya') ? 'checked' : '' }}>
                                                                <label class="form-check-label" for="paud_ya">
                                                                    Ya
                                                                </label>
                                                            </div>

                                                            <div class="form-check col-md-4 col-12">
                                                                <input class="form-check-input" type="radio"
                                                                    name="paud" value="Tidak" id="paud_tidak" {{ ($inhabitant->paud == 'Tidak') ? 'checked' : '' }}>
                                                                <label class="form-check-label" for="paud_tidak">
                                                                    Tidak
                                                                </label>
                                                            </div>
                                                        </div>
                                                    </div>

                                                    @error('paud')
                                                        <div class="text-danger">{{ $message }}</div>
                                                    @enderror
                                                </div>
                                            </div>
                                            <div class="col-lg-12">
                                                <div class="form-group">
                                                    <label for="koperasi">Ikut Dalam Kegiatan Koperasi</label>
                                                    <!-- <input type="text" name="koperasi"
                                                        value="{{ $inhabitant->koperasi }}" id="koperasi"
                                                        class="form-control round"
                                                        placeholder="Ikut Dalam Kegiatan Koperasi"> -->
                                                    
                                                    <div class="container">
                                                        <div class="row">
                                                            <div class="form-check col-md-4 col-12">
                                                                <input class="form-check-input" type="radio"
                                                                    name="koperasi" value="Ya" id="koperasi_ya" {{ ($inhabitant->koperasi == 'Ya') ? 'checked' : '' }}>
                                                                <label class="form-check-label" for="koperasi_ya">
                                                                    Ya
                                                                </label>
                                                            </div>

                                                            <div class="form-check col-md-4 col-12">
                                                                <input class="form-check-input" type="radio"
                                                                    name="koperasi" value="Tidak" id="koperasi_tidak" {{ ($inhabitant->koperasi == 'Tidak') ? 'checked' : '' }}>
                                                                <label class="form-check-label" for="koperasi_tidak">
                                                                    Tidak
                                                                </label>
                                                            </div>
                                                        </div>
                                                    </div>

                                                    @error('koperasi')
                                                        <div class="text-danger">{{ $message }}</div>
                                                    @enderror
                                                </div>
                                            </div>
                                            <button type="submit" class="btn btn-primary btn-block">Simpan</button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </div>
        <x-footer></x-footer>
    </div>
@endsection
