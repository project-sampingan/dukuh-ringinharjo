@section('title', 'Data Warga')
@extends('layouts.voler')
@section('content')
    <div id="main">
        <x-navbar></x-navbar>

        <div class="main-content container-fluid">
            <div class="page-title">
                <div class="row">
                    <div class="col-12 col-md-6 order-md-1 order-last">
                        <h3>@yield('title')</h3>
                    </div>
                    <div class="col-12 col-md-6 order-md-2 order-first">
                        <nav aria-label="breadcrumb" class='breadcrumb-header'>
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="{{ route('dashboard') }}">Dashboard</a></li>
                                <li class="breadcrumb-item active" aria-current="page">@yield('title')</li>
                            </ol>
                        </nav>
                    </div>
                </div>
            </div>
            <section class="section">
                <div class="card">
                    <div class="card-header row">
                        <div class="col-lg-2">
                            <a href="{{ route('inhabitants.create') }}" class="btn icon icon-left btn-primary">
                                <i data-feather="plus"></i>
                                <span>Tambah Data</span>
                            </a>
                        </div>
                        <div class="col-lg-3">
                            <a href="{{ url('download/inhabitant') }}" class="btn icon btn-success">
                                <i data-feather="download"></i>
                            </a>
                        </div>
                    </div>
                    <div class="card-body">
                        <table class='table' id="table1">
                            <thead>
                                <tr>
                                    <th>No.</th>
                                    <th>Dasawisma</th>
                                    <th>No. Registrasi</th>
                                    <th>Nama</th>
                                    <th>Jabatan</th>
                                    <th>Aksi</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($inhabitants as $inhabitant)
                                    <tr>
                                        <td>{{ $loop->iteration }}</td>
                                        <td>{{ $inhabitant->desa }}</td>
                                        <td>{{ $inhabitant->no_reg }}</td>
                                        <td>{{ $inhabitant->nama }}</td>
                                        <td>{{ $inhabitant->jabatan }}</td>
                                        <td>
                                            <a href="{{ route('activity.create', $inhabitant->id) }}"
                                                class="btn icon btn-success btn-sm">
                                                <i data-feather="plus"></i>
                                                <span>Ac</span>
                                            </a>
                                            <a href="{{ route('activity.show', [$inhabitant->id, $inhabitant->id]) }}"
                                                class="btn icon btn-primary btn-sm">
                                                <i data-feather="eye"></i>
                                                <span>Ac</span>
                                            </a>
                                            <a href="{{ route('inhabitants.show', $inhabitant->id) }}"
                                                class="btn icon btn-info btn-sm">
                                                <i data-feather="eye"></i>
                                            </a>
                                            <a href="{{ route('inhabitants.edit', $inhabitant->id) }}"
                                                class="btn icon icon-left btn-warning btn-sm">
                                                <i data-feather="edit"></i>
                                            </a>
                                            <form action="{{ route('inhabitants.destroy', $inhabitant->id) }}"
                                                method="POST" class="d-inline">
                                                @csrf
                                                @method('delete')
                                                <button type="submit" class="btn icon icon-left btn-danger btn-sm"
                                                    onclick="return confirm('Yakin ingin menghapus data?')">
                                                    <i data-feather="trash"></i>
                                                </button>
                                            </form>
                                        </td>
                                    </tr>
                                @endforeach

                                @if ($inhabitants->count() == 0)
                                    <tr>
                                        <td colspan="4" class="text-center">Data Kosong</td>
                                    </tr>
                                @endif
                            </tbody>
                        </table>
                    </div>
                </div>

            </section>
        </div>
        <x-footer></x-footer>
    </div>
@endsection
