@section('title', 'Data Warga TP-PKK')
@extends('layouts.bt5')
@section('content')
    <div class="container mt-3">
        <center>
            <h5>Data Warga TP-PKK</h5>
        </center>

        <p class="mt-2 mb-0">Dasawisma : {{ $inhabitants->desa }}</p>
        <p class="mb-0">Nama Kepala Rumah Tangga : {{ $inhabitants->nama_kk }}</p>
        <ol>
            <li>
                <p class="mb-0">No. Registrasi : {{ $inhabitants->no_reg }}</p>
            </li>
            <li>
                <p class="mb-0">No. KTP/NIK : {{ $inhabitants->nik }}</p>
            </li>
            <li>
                <p class="mb-0">Nama : {{ $inhabitants->nama }}</p>
            </li>
            <li>
                <p class="mb-0">Jabatan : {{ $inhabitants->jabatan }}</p>
            </li>
            <li>
                <p class="mb-0">Jenis Kelamin : {{ $inhabitants->jenis_kelamin }}</p>
            </li>
            <li>
                <p class="mb-0">Tempat Lahir : {{ $inhabitants->tempat_lahir }}</p>
            </li>
            <li>
                <p class="mb-0">Tanggal Lahir : {{ $inhabitants->tanggal_lahir }} Umur : {{ $age }} Tahun</p>
            </li>
            <li>
                <p class="mb-0">Status Perkawinan : {{ $inhabitants->status_kawin }}</p>
            </li>
            <li>
                <p class="mb-0">Status Dalam Keluarga : {{ $inhabitants->status_dlm_keluarga }}</p>
            </li>
            <li>
                <p class="mb-0">Agama : {{ $inhabitants->agama }}</p>
            </li>
            <li>
                <p class="mb-0">Alamat : {{ $inhabitants->alamat }}</p>
            </li>
            <li>
                <p class="mb-0">Pendidikan : {{ $inhabitants->pendidikan }}</p>
            </li>
            <li>
                <p class="mb-0">Pekerjaan : {{ $inhabitants->pekerjaan }}</p>
            </li>
            <li>
                <p class="mb-0">Akseptor KB : {{ $inhabitants->akseptor_kb }}</p>
            </li>
            <li>
                <p class="mb-0">Aktif dalam kegiatan Posyandu : {{ $inhabitants->posyandu }}</p>
            </li>
            <li>
                <p class="mb-0">Mengikuti Program Bina Keluarga Balita : {{ $inhabitants->bina_keluarga_balita }}</p>
            </li>
            <li>
                <p class="mb-0">Mengikuti PAUD/Sejenis : {{ $inhabitants->paud }}</p>
            </li>
            <li>
                <p class="mb-0">Ikut dalam Kegiatan Koperasi : {{ $inhabitants->koperasi }}</p>
            </li>
        </ol>
        <center>
            <h5>Kegiatan Warga</h5>
        </center>
        <table class="table text-center table-bordered">
            <thead>
                <tr>
                    <th scope="col">No</th>
                    <th scope="col">Kegiatan</th>
                    <th scope="col">Aktivitas Y/T</th>
                    <th scope="col">Keterangan (Jenis Kegiatan Yang Diikuti)</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($activities as $activity)
                    <tr>
                        <td scope="row">{{ $loop->iteration }}</td>
                        <td>{{ $activity->nama_kegiatan }}</td>
                        <td>{{ $activity->status }}</td>
                        <td>{{ $activity->deskripsi }}</td>
                    </tr>
                @endforeach
            </tbody>
    </div>
@endsection
