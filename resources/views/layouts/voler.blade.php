<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>@yield('title')</title>

    <link rel="stylesheet" href="{{ asset('voler/assets/css/bootstrap.css') }}">

    <link rel="stylesheet" href="{{ asset('voler/assets/vendors/simple-datatables/style.css') }}">

    <link rel="stylesheet" href="{{ asset('voler/assets/vendors/chartjs/Chart.min.css') }}">

    <link rel="stylesheet" href="{{ asset('voler/assets/vendors/perfect-scrollbar/perfect-scrollbar.css') }}">
    <link rel="stylesheet" href="{{ asset('voler/assets/css/app.css') }}">
    <link rel="shortcut icon" href="{{ asset('voler/assets/images/favicon.svg') }}" type="image/x-icon">
    <link rel="stylesheet" href="{{ asset('voler/assets/css/sweetalert2.min.css') }}">
    <script src="{{ asset('voler/assets/js/jquery.min.js') }}"></script>
    <!-- <link href="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/5.0.7/sweetalert2.min.css" rel="stylesheet"> -->
    <!-- <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.6.3/jquery.min.js" integrity="sha512-STof4xm1wgkfm7heWqFJVn58Hm3EtS31XFaagaa8VMReCXAkQnJZ+jEy8PCC/iT18dFy95WcExNHFTqLyp72eQ==" crossorigin="anonymous" referrerpolicy="no-referrer"></script> -->
</head>

<body>
    <div id="app">
        @include('sweetalert::alert', ['cdn' => 'https://cdn.jsdelivr.net/npm/sweetalert2@9'])
        <x-sidebar></x-sidebar>
        
        @yield('content')
    </div>
    <!-- <script src="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/2.1.2/sweetalert.min.js"></script> -->
    <script src="{{ asset('voler/assets/js/sweetalert.min.js') }}"></script>
    <script src="{{ asset('voler/assets/js/feather-icons/feather.min.js') }}"></script>
    <script src="{{ asset('voler/assets/vendors/simple-datatables/simple-datatables.js') }}"></script>
    <script src="{{ asset('voler/assets/vendors/perfect-scrollbar/perfect-scrollbar.min.js') }}"></script>
    <script src="{{ asset('voler/assets/js/app.js') }}"></script>
    <script src="{{ asset('voler/assets/js/vendors.js') }}"></script>

    @if (!empty($page) && $page == 'dashboard')
        <script src="voler/assets/vendors/chartjs/Chart.min.js"></script>
        <script src="{{ asset('voler/assets/vendors/apexcharts/apexcharts.min.js') }}"></script>
        <script src="{{ asset('voler/assets/js/pages/dashboard.js') }}"></script>
    @endif

    <script src="{{ asset('voler/assets/js/main.js') }}"></script>

</body>

</html>
