@section('title', 'Master Dasa Wisma')
@extends('layouts.voler')
@section('content')
    <div id="main">
        <x-navbar></x-navbar>

        <div class="main-content container-fluid">
            <div class="page-title">
                <div class="row">
                    <div class="col-12 col-md-6 order-md-1 order-last">
                        <h3>@yield('title')</h3>
                    </div>
                    <div class="col-12 col-md-6 order-md-2 order-first">
                        <nav aria-label="breadcrumb" class='breadcrumb-header'>
                            <ol class="breadcrumb">
                                <!-- <li class="breadcrumb-item"><a href="{{ route('dashboard') }}">Dashboard</a></li> -->
                                <!-- <li class="breadcrumb-item active" aria-current="page">@yield('title')</li> -->
                            </ol>
                        </nav>
                    </div>
                </div>
            </div>
            <section class="section">
                <div class="card">
                    <div class="card-header">
                        <button type="button" class="btn icon btn-primary btn-sm" data-bs-toggle="modal" data-bs-target="#staticBackdrop">
                            <i data-feather="plus"></i>
                        </button>
                    </div>
                    <div class="card-body">
                        <table class='table table-hover table-striped' id="table1">
                            <thead>
                                <tr>
                                    <th>No.</th>
                                    <th>Nama</th>
                                    <th>Tanggal Buat</th>
                                    <th>Aksi</th>
                                </tr>
                            </thead>
                            <tbody>
                                @if (!empty($dasawisma))
                                    @foreach ($dasawisma as $no => $row)
                                        <tr>
                                            <td>{{ $no+1 }}</td>
                                            <td>{{ $row->name }}</td>
                                            <td>{{ $row->created_at }}</td>
                                            <td>
                                                <button type="button" class="btn icon btn-warning btn-sm" data-bs-toggle="modal" data-bs-target="#editData{{ $row->id }}">
                                                    <i data-feather="edit"></i>
                                                </button>
                                                <button type="button" class="btn icon btn-danger btn-sm" data-bs-toggle="modal" data-bs-target="#hapusData{{ $row->id }}">
                                                    <i data-feather="trash"></i>
                                                </button>
                                            </td>
                                        </tr>
                                    @endforeach

                                    @if ($dasawisma->count() == 0)
                                        <tr>
                                            <td colspan="4" class="text-center">Data Kosong</td>
                                        </tr>
                                    @endif
                                @endif
                            </tbody>
                        </table>
                    </div>
                </div>

            </section>
        @include('masterdasawisma.modal')
        @include('masterdasawisma.modal-edit')
        </div>
        <script>
            // $('#tableDasaWisma').dataTable( {
            //         "columnDefs": [ {
            //         "targets": 'no-sort',
            //         "orderable": false,
            //     } ]
            // } );
        </script>
        <x-footer></x-footer>
    </div>
@endsection
