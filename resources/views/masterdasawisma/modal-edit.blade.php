@if (!empty($dasawisma))
@foreach ($dasawisma as $no => $row)

<div class="modal fade" id="editData{{ $row->id }}" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1" aria-labelledby="staticBackdropLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="staticBackdropLabel">Edit data dasa wisma</h5>
        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
      </div>
      <div class="modal-body">
        <form action="{{ route('masterdasawisma.update', $row->id) }}" method="POST">
            @method('PUT')
            @csrf
            <div class="row">
                <div class="col-lg-12">
                    <div class="form-group">
                        <label for="desa">Nama Dasa Wisma</label>
                        <input type="text" name="desa" id="editDasaWisma{{ $row->id }}" onChange="checkInputEdit({{ $row->id }})" value="{{ $row->name }}" class="form-control round" placeholder="Desa Wisma">
                        <div id="textErrorEdit{{ $row->id }}" class="text-danger"></div>
                    </div>
                </div>
            </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
        <button type="submit" id="tombolEdit{{ $row->id }}" class="btn btn-primary">Edit</button>
        </form>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="hapusData{{ $row->id }}" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1" aria-labelledby="staticBackdropLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <!-- <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button> -->
      </div>
      <div class="modal-body">
        <form action="{{ route('masterdasawisma.destroy', $row->id) }}" method="POST">
            @method('delete')
            @csrf
            <div class="row">
                <div class="col-lg-12 text-center">
                   <h4>Yakin menghapus data ini ?</h4>
                </div>
            </div>
      </div>
      <div class="modal-footer">
        <button type="submit" class="btn btn-info">Ya</button>
        <button type="button" class="btn btn-default" data-bs-dismiss="modal">Tidak</button>
        </form>
      </div>
    </div>
  </div>
</div>

@endforeach

<script>
    function checkInputEdit(id) {
        var name = $("#editDasaWisma"+id).val()
        if (name == "") {
            $("#textErrorEdit"+id).html("Nama dasa wisma wajib di isi")
            $("#tombolEdit"+id).prop("disabled", true)
        } else {
            $("#textErrorEdit"+id).html("")
            $("#tombolEdit"+id).prop("disabled", false)
        }
    }
</script>
@endif