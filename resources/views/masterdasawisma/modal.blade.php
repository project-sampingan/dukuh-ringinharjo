<!-- Modal -->
<div class="modal fade" id="staticBackdrop" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1" aria-labelledby="staticBackdropLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="staticBackdropLabel">Tambah data dasa wisma</h5>
        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
      </div>
      <div class="modal-body">
        <form action="{{ route('masterdasawisma.store') }}" method="POST">
            @csrf
            <div class="row">
                <div class="col-lg-12">
                    <div class="form-group">
                        <label for="desa">Desa Wisma</label>
                        <input type="text" name="desa" id="desa" class="form-control round" onChange="checkInput()" placeholder="Desa Wisma">
                        @error('desa')
                            <div class="text-danger">{{ $message }}</div>
                        @enderror
                    </div>
                </div>
            </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
        <button type="submit" id="tombolSimpan" class="btn btn-primary" disabled>Simpan</button>
        </form>
      </div>
    </div>
  </div>
</div>

<script>
    function checkInput() {
        var desa = $("#desa").val();
        if (desa) {
            $("#tombolSimpan").prop("disabled", false);
        }
    }
</script>