<div class="card">
    <button type="button" class="card-header" data-bs-toggle="collapse" href="#collapseLokasiDetail" role="button" aria-expanded="false" aria-controls="collapseLokasiDetail">
        <h4 class="fw-bolder">Lokasi Detail</h4>
    </button>
    <div class="collapse card-body show" id="collapseLokasiDetail">
        
        <div class="row">
            <div class="col-lg-12">
                <div class="form-group">
                    <label for="desa">Dasa Wisma</label>

                    <div class="input-group">
                        <select class="form-select round" id="desa"
                            name="desa">
                            <option selected disabled value="">Pilih...</option>
                            @if (!empty($dasawisma))
                            @foreach ($dasawisma as $no => $row)
                                <option value="{{ $row->name }}">{{ $row->name }}</option>
                            @endforeach
                            @endif
                        </select>
                    </div>

                    @error('desa')
                        <div class="text-danger">{{ $message }}</div>
                    @enderror
                </div>
            </div>
            <div class="col-lg-12">
                <div class="form-group">
                    <label for="rt">RT</label>
                    <div class="input-group">
                        <select class="form-select round" id="rt"
                            name="rt">
                            <option selected>Pilih...</option>
                            @for ($i = 0; $i < 7; $i++)
                            <option value="{{ $i + 1 }}">0{{ $i + 1 }}</option>
                            @endfor
                        </select>
                    </div>

                    @error('rt')
                        <div class="text-danger">{{ $message }}</div>
                    @enderror
                </div>
            </div>
            <div class="col-lg-12">
                <div class="form-group">
                    <input type="hidden" name="rw" id="rw" class="form-control round" value="00">

                    @error('rw')
                        <div class="text-danger">{{ $message }}</div>
                    @enderror
                </div>
            </div>
            <div class="col-lg-12">
                <div class="form-group">
                    <label for="prov">Provinsi</label>
                    <input type="text" name="prov" value="Di Yogyakarta" id="prov" class="form-control round" disabled>

                    @error('prov')
                        <div class="text-danger">{{ $message }}</div>
                    @enderror
                </div>
            </div>
            <div class="col-lg-12">
                <div class="form-group">
                    <label for="kab">Kabupaten</label>
                    <input type="text" name="kab" value="Bantul" id="kab" class="form-control round" disabled>

                    @error('kab')
                        <div class="text-danger">{{ $message }}</div>
                    @enderror
                </div>
            </div>
            <div class="col-lg-12">
                <div class="form-group">
                    <label for="kep">Kapanewon</label>
                    <input type="text" name="kep" id="kep" value="Ringinharjo" class="form-control round" placeholder="Kapanewon" disabled>

                    @error('kep')
                        <div class="text-danger">{{ $message }}</div>
                    @enderror
                </div>
            </div>
            <div class="col-lg-12">
                <div class="form-group">
                    <label for="kel">Kalurahan</label>
                    <input type="text" name="kel" id="kel" class="form-control round" placeholder="Kalurahan">

                    @error('kel')
                        <div class="text-danger">{{ $message }}</div>
                    @enderror
                </div>
            </div>
            <div class="col-lg-12">
                <div class="form-group">
                    <label for="dusun">Dusun</label>
                    <div class="container">
                        <div class="row">
                            <div class="form-check col-md-4 col-12">
                                <input class="form-check-input" type="radio"
                                    name="dusun" value="Mandingan Padukuhan Mandingan" id="mandingan" checked>
                                <label class="form-check-label" for="mandingan">
                                    Mandingan Padukuhan Mandingan
                                </label>
                            </div>

                            <div class="form-check col-md-4 col-12">
                                <input class="form-check-input" type="radio"
                                    name="dusun" value="Ngabean Padukuhan Mandingan" id="ngabean">
                                <label class="form-check-label" for="ngabean">
                                    Ngabean Padukuhan Mandingan
                                </label>
                            </div>

                            <div class="form-check col-md-4 col-12">
                                <input class="form-check-input" type="radio"
                                    name="dusun" value="Karang Padukuhan mandingan" id="karang">
                                <label class="form-check-label" for="karang">
                                    Karang Padukuhan mandingan
                                </label>
                            </div>
                        </div>
                    </div>

                    @error('dusun')
                        <div class="text-danger">{{ $message }}</div>
                    @enderror
                </div>
            </div>
        </div>

    </div>
</div>