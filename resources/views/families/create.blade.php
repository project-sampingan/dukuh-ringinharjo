@section('title', 'Tambah Data Keluarga')
@extends('layouts.voler')
@section('content')
    <div id="main">
        <x-navbar></x-navbar>

        <div class="main-content container-fluid">
            <div class="page-title">
                <div class="row">
                    <div class="col-12 col-md-6 order-md-1 order-last">
                        <h4>@yield('title')</h4>
                        <!-- <p class="text-subtitle text-muted">We use 'simple-datatables' made by @fiduswriter. You can check
                            the full documentation <a href="https://github.com/fiduswriter/Simple-DataTables/wiki">here</a>.
                        </p> -->
                    </div>
                    <div class="col-12 col-md-6 order-md-2 order-first">
                        <nav aria-label="breadcrumb" class='breadcrumb-header'>
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="{{ route('families.index') }}">Data Keluarga</a></li>
                                <li class="breadcrumb-item active" aria-current="page">@yield('title')</li>
                            </ol>
                        </nav>
                    </div>
                </div>
            </div>
            <section id="basic-input-groups">
                <div class="row">
                    <div class="col-12">
                        <form action="{{ route('families.store') }}" method="POST">
                            @csrf
                            <div class="card">
                                <div class="card-content">
                                    <div class="card-body">
                                        @include('families.create-lokasi')
                                        @include('families.create-anggota-keluarga')
                                        @include('families.create-keadaan-rumah')
                                        <button type="submit" class="btn btn-primary">Simpan</button>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </section>
        </div>
        <x-footer></x-footer>
    </div>

<script>
    // $('#show_nama_kk').css('display', 'none');
    $('#search_by_nik').click(() => {
        $('#search_by_nik').prop('disabled', true)
        $('#search_by_nik').html('')
        html = `<div class="spinner-grow spinner-grow-sm" role="status">
                    <span class="visually-hidden">Loading...</span>
                </div>`
        $('#search_by_nik').html(html)

        $.ajax({
            method: 'GET',
            url: '{{ url("api/warga/by_nik") }}',
            data: { nik: $('#search_nik').val() }
        })
        .done(function(data) {
            $('#id_kk').val(data.data.id)
            $('#nama_kk').val(data.data.nama)
            $('#nama_kk_show').val(data.data.nama)
            $('#search_by_nik').prop('disabled', '')
            $('#search_by_nik').html('Cari')
            // $('#show_nama_kk').css('display', 'block');
        })
    })
</script>
@endsection
