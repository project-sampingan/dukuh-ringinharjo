<div class="card">
    <button type="button" class="card-header" data-bs-toggle="collapse" href="#collapseKeadaanRumah" role="button" aria-expanded="false" aria-controls="collapseKeadaanRumah">
        <h4 class="fw-bolder">Keadaan Rumah</h4>
    </button>
    <div class="collapse card-body show" id="collapseKeadaanRumah">
        
        <div class="row">
            <div class="col-lg-12">
                <div class="form-group">
                    <label for="makanan_pokok">Makanan Pokok Sehari-hari</label>
                    <input type="text" name="makanan_pokok" id="makanan_pokok"
                        value="Beras" class="form-control round"
                        placeholder="Makanan Pokok Sehari-hari">

                    @error('makanan_pokok')
                        <div class="text-danger">{{ $message }}</div>
                    @enderror
                </div>
            </div>
            <div class="col-lg-12">
                <div class="form-group">
                    <label for="jamban">Mempunyai Jamban keluarga</label>
                    <select class="form-select round" id="jamban" name="jamban">
                        <option selected>Pilih...</option>
                        <option value="Iya">Iya</option>
                        <option value="Tidak">Tidak</option>
                    </select>

                    @error('jamban')
                        <div class="text-danger">{{ $message }}</div>
                    @enderror
                </div>
            </div>
            <div class="col-lg-12">
                <div class="form-group">
                    <label for="sumber_air">Sumber Air Keluarga</label>
                    <select class="form-select round" id="sumber_air" name="sumber_air">
                        <option selected>Pilih...</option>
                        <option value="PDAM">PDAM</option>
                        <option value="Sumur">Sumur</option>
                        <option value="Sungai">Sungai</option>
                    </select>

                    @error('sumber_air')
                        <div class="text-danger">{{ $message }}</div>
                    @enderror
                </div>
            </div>
            <div class="col-lg-12">
                <div class="form-group">
                    <label for="sampah">Memiliki Tempat Pembuangan Sampah</label>
                    <select class="form-select round" id="sampah"
                        name="pembuangan_sampah">
                        <option selected>Pilih...</option>
                        <option value="Iya">Iya</option>
                        <option value="Tidak">Tidak</option>
                    </select>

                    @error('pembuangan_sampah')
                        <div class="text-danger">{{ $message }}</div>
                    @enderror
                </div>
            </div>
            <div class="col-lg-12">
                <div class="form-group">
                    <label for="air_limbah">Memiliki Saluran Pembuagan Air Limbah</label>
                    <select class="form-select round" id="air_limbah"
                        name="pembuangan_air_limbah">
                        <option selected>Pilih...</option>
                        <option value="Iya">Iya</option>
                        <option value="Tidak">Tidak</option>
                    </select>

                    @error('pembuangan_air_limbah')
                        <div class="text-danger">{{ $message }}</div>
                    @enderror
                </div>
            </div>
            <div class="col-lg-12">
                <div class="form-group">
                    <label for="p4k">Menempelkan Stiker P4k</label>
                    <select class="form-select round" id="p4k" name="stiker_p4k">
                        <option selected>Pilih...</option>
                        <option value="Iya">Iya</option>
                        <option value="Tidak">Tidak</option>
                    </select>

                    @error('stiker_p4k')
                        <div class="text-danger">{{ $message }}</div>
                    @enderror
                </div>
            </div>
            <div class="col-lg-12">
                <div class="form-group">
                    <label for="kriteria_rumah">Kriteria Rumah</label>
                    
                    <div class="row">
                        <div class="col-md-2">
                            <div class="form-check">
                                <input class="form-check-input" type="radio"
                                    name="kriteria_rumah" value="Sehat" id="sehat">
                                <label class="form-check-label" for="sehat">
                                    Sehat
                                </label>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-check">
                                <input class="form-check-input" type="radio"
                                    name="kriteria_rumah" value="Tidak Sehat" id="tidak sehat">
                                <label class="form-check-label" for="tidak sehat">
                                    Tidak Sehat
                                </label>
                            </div>
                        </div>
                    </div>

                    @error('kriteria_rumah')
                        <div class="text-danger">{{ $message }}</div>
                    @enderror
                </div>
            </div>
            <div class="col-lg-12">
                <div class="form-group">
                    <label for="up2k">Aktivitas UP2K</label>
                    <select class="form-select round" id="up2k" name="up2k">
                        <option selected>Pilih...</option>
                        <option value="Iya">Iya</option>
                        <option value="Tidak">Tidak</option>
                    </select>

                    @error('up2k')
                        <div class="text-danger">{{ $message }}</div>
                    @enderror
                </div>
            </div>
            <div class="col-lg-12">
                <div class="form-group">
                    <label for="kukl">Aktivitas Kegiatan Usaha Lingkungan</label>
                    <select class="form-select round" id="kukl" name="kukl">
                        <option selected>Pilih...</option>
                        <option value="Iya">Iya</option>
                        <option value="Tidak">Tidak</option>
                    </select>

                    @error('kukl')
                        <div class="text-danger">{{ $message }}</div>
                    @enderror
                </div>
            </div>
        </div>

    </div>
</div>