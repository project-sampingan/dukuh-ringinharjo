@section('title', 'Edit Data Keluarga')
@extends('layouts.voler')
@section('content')
    <div id="main">
        <x-navbar></x-navbar>

        <div class="main-content container-fluid">
            <div class="page-title">
                <div class="row">
                    <div class="col-12 col-md-6 order-md-1 order-last">
                        <h4>@yield('title') {{ $family->nama_kk }}</h4>
                    </div>
                    <div class="col-12 col-md-6 order-md-2 order-first">
                        <nav aria-label="breadcrumb" class='breadcrumb-header'>
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="{{ route('dashboard') }}">Dashboard</a></li>
                                <li class="breadcrumb-item active" aria-current="page">@yield('title')</li>
                            </ol>
                        </nav>
                    </div>
                </div>
            </div>
            <section id="basic-input-groups">
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-content">
                                <div class="card-body">
                                    <form action="{{ route('families.update', $family->id) }}" method="POST">
                                        @csrf
                                        @method('put')
                                        <div class="row">
                                            <div class="col-lg-12">
                                                <div class="form-group">
                                                    <label for="desa">Dasa Wisma</label>
                                                    <input type="text" name="desa" value="{{ $family->desa }}"
                                                        id="desa" class="form-control round" placeholder="Dasa Wisma">

                                                    @error('desa')
                                                        <div class="text-danger">{{ $message }}</div>
                                                    @enderror
                                                </div>
                                            </div>
                                            <div class="col-lg-12">
                                                <div class="form-group">
                                                    <label for="rt">RT</label>
                                                    <input type="number" name="rt" value="{{ $family->rt }}"
                                                        id="rt" class="form-control round" placeholder="RT">

                                                    @error('rt')
                                                        <div class="text-danger">{{ $message }}</div>
                                                    @enderror
                                                </div>
                                            </div>
                                            <div class="col-lg-12">
                                                <div class="form-group">
                                                    <label for="rw">RW</label>
                                                    <input type="number" name="rw" value="{{ $family->rw }}"
                                                        id="rw" class="form-control round" placeholder="RW">

                                                    @error('rw')
                                                        <div class="text-danger">{{ $message }}</div>
                                                    @enderror
                                                </div>
                                            </div>
                                            <div class="col-lg-12">
                                                <div class="form-group">
                                                    <label for="dusun">Dusun</label>
                                                    <input type="text" name="dusun" value="{{ $family->dusun }}"
                                                        id="dusun" class="form-control round" placeholder="Dusun">

                                                    @error('dusun')
                                                        <div class="text-danger">{{ $message }}</div>
                                                    @enderror
                                                </div>
                                            </div>
                                            <div class="col-lg-12">
                                                <div class="form-group">
                                                    <label for="kel">Kalurahan</label>
                                                    <input type="text" name="kel" value="{{ $family->kel }}"
                                                        id="kel" class="form-control round" placeholder="Kalurahan">

                                                    @error('kel')
                                                        <div class="text-danger">{{ $message }}</div>
                                                    @enderror
                                                </div>
                                            </div>
                                            <div class="col-lg-12">
                                                <div class="form-group">
                                                    <label for="kep">Kapanewon</label>
                                                    <input type="text" name="kep" value="{{ $family->kep }}"
                                                        id="kep" class="form-control round" placeholder="Kapanewon">

                                                    @error('kep')
                                                        <div class="text-danger">{{ $message }}</div>
                                                    @enderror
                                                </div>
                                            </div>
                                            <div class="col-lg-12">
                                                <div class="form-group">
                                                    <label for="kab">Kabupaten</label>
                                                    <input type="text" name="kab" value="{{ $family->kab }}"
                                                        id="kab" class="form-control round" placeholder="Kabupaten">

                                                    @error('kab')
                                                        <div class="text-danger">{{ $message }}</div>
                                                    @enderror
                                                </div>
                                            </div>
                                            <div class="col-lg-12">
                                                <div class="form-group">
                                                    <label for="prov">Provinsi</label>
                                                    <input type="text" name="prov" value="{{ $family->prov }}"
                                                        id="prov" class="form-control round" placeholder="Provinsi">

                                                    @error('prov')
                                                        <div class="text-danger">{{ $message }}</div>
                                                    @enderror
                                                </div>
                                            </div>
                                            <div class="col-lg-12">
                                                <div class="form-group">
                                                    <label for="nama_kk">Nama Kepala Rumah Tanggga</label>
                                                    <input type="text" name="nama_kk" value="{{ $family->nama_kk }}"
                                                        id="nama_kk" class="form-control round"
                                                        placeholder="Nama Kepala Rumah Tanggga">

                                                    @error('nama_kk')
                                                        <div class="text-danger">{{ $message }}</div>
                                                    @enderror
                                                </div>
                                            </div>
                                            <div class="col-lg-12">
                                                <div class="form-group">
                                                    <label for="ttl_anggota">Jumlah Anggota Keluarga</label>
                                                    <input type="number" name="ttl_anggota"
                                                        value="{{ $family->ttl_anggota }}" id="ttl_anggota"
                                                        class="form-control round" placeholder="Jumlah Anggota Keluarga">

                                                    @error('ttl_anggota')
                                                        <div class="text-danger">{{ $message }}</div>
                                                    @enderror
                                                </div>
                                            </div>
                                            <div class="col-lg-12">
                                                <div class="form-group">
                                                    <label for="jml_lk">Laki-laki</label>
                                                    <input type="number" name="jml_lk" value="{{ $family->jml_lk }}"
                                                        id="jml_lk" class="form-control round"
                                                        placeholder="Laki-laki">

                                                    @error('jml_lk')
                                                        <div class="text-danger">{{ $message }}</div>
                                                    @enderror
                                                </div>
                                            </div>
                                            <div class="col-lg-12">
                                                <div class="form-group">
                                                    <label for="jml_pr">Perempuan</label>
                                                    <input type="number" name="jml_pr" value="{{ $family->jml_pr }}"
                                                        id="jml_pr" class="form-control round"
                                                        placeholder="Perempuan">

                                                    @error('jml_pr')
                                                        <div class="text-danger">{{ $message }}</div>
                                                    @enderror
                                                </div>
                                            </div>
                                            <div class="col-lg-12">
                                                <div class="form-group">
                                                    <label for="jml_kk">Jumlah KK</label>
                                                    <input type="number" name="jml_kk" value="{{ $family->jml_kk }}"
                                                        id="jml_kk" class="form-control round"
                                                        placeholder="Jumlah KK">

                                                    @error('jml_kk')
                                                        <div class="text-danger">{{ $message }}</div>
                                                    @enderror
                                                </div>
                                            </div>
                                            <div class="col-lg-12">
                                                <div class="form-group">
                                                    <label for="balita">Balita</label>
                                                    <input type="number" name="jml_balita"
                                                        value="{{ $family->jml_balita }}" id="balita"
                                                        class="form-control round" placeholder="Balita">

                                                    @error('jml_balita')
                                                        <div class="text-danger">{{ $message }}</div>
                                                    @enderror
                                                </div>
                                            </div>
                                            <div class="col-lg-12">
                                                <div class="form-group">
                                                    <label for="anak">Anak</label>
                                                    <input type="number" name="jml_anak"
                                                        value="{{ $family->jml_anak }}" id="anak"
                                                        class="form-control round" placeholder="Anak">

                                                    @error('jml_anak')
                                                        <div class="text-danger">{{ $message }}</div>
                                                    @enderror
                                                </div>
                                            </div>
                                            <div class="col-lg-12">
                                                <div class="form-group">
                                                    <label for="pus">PUS</label>
                                                    <input type="number" name="jml_pus" value="{{ $family->jml_pus }}"
                                                        id="pus" class="form-control round" placeholder="PUS">

                                                    @error('jml_pus')
                                                        <div class="text-danger">{{ $message }}</div>
                                                    @enderror
                                                </div>
                                            </div>
                                            <div class="col-lg-12">
                                                <div class="form-group">
                                                    <label for="wus">WUS</label>
                                                    <input type="number" name="jml_wus" value="{{ $family->jml_wus }}"
                                                        id="wus" class="form-control round" placeholder="WUS">

                                                    @error('jml_wus')
                                                        <div class="text-danger">{{ $message }}</div>
                                                    @enderror
                                                </div>
                                            </div>
                                            <div class="col-lg-12">
                                                <div class="form-group">
                                                    <label for="buta">Buta</label>
                                                    <input type="number" name="jml_buta"
                                                        value="{{ $family->jml_buta }}" id="buta"
                                                        class="form-control round" placeholder="Buta">

                                                    @error('jml_buta')
                                                        <div class="text-danger">{{ $message }}</div>
                                                    @enderror
                                                </div>
                                            </div>
                                            <div class="col-lg-12">
                                                <div class="form-group">
                                                    <label for="jml_bumil">Ibu Hamil</label>
                                                    <input type="number" name="jml_bumil"
                                                        value="{{ $family->jml_bumil }}" id="jml_bumil"
                                                        class="form-control round" placeholder="Ibu Hamil">

                                                    @error('jml_bumil')
                                                        <div class="text-danger">{{ $message }}</div>
                                                    @enderror
                                                </div>
                                            </div>
                                            <div class="col-lg-12">
                                                <div class="form-group">
                                                    <label for="jml_busu">Ibu Menyusui</label>
                                                    <input type="number" name="jml_busu"
                                                        value="{{ $family->jml_busu }}" id="jml_busu"
                                                        class="form-control round" placeholder="Ibu Menyusui">

                                                    @error('jml_busu')
                                                        <div class="text-danger">{{ $message }}</div>
                                                    @enderror
                                                </div>
                                            </div>
                                            <div class="col-lg-12">
                                                <div class="form-group">
                                                    <label for="jml_lansia">Lansia</label>
                                                    <input type="number" name="jml_lansia"
                                                        value="{{ $family->jml_lansia }}" id="jml_lansia"
                                                        class="form-control round" placeholder="Lansia">

                                                    @error('jml_lansia')
                                                        <div class="text-danger">{{ $message }}</div>
                                                    @enderror
                                                </div>
                                            </div>
                                            <div class="col-lg-12">
                                                <div class="form-group">
                                                    <label for="makanan_pokok">Makanan Pokok Sehari-hari</label>
                                                    <input type="text" name="makanan_pokok" id="makanan_pokok"
                                                        value="{{ $family->makanan_pokok }}" class="form-control round"
                                                        placeholder="Makanan Pokok Sehari-hari">

                                                    @error('makanan_pokok')
                                                        <div class="text-danger">{{ $message }}</div>
                                                    @enderror
                                                </div>
                                            </div>
                                            <div class="col-lg-12">
                                                <div class="form-group">
                                                    <label for="jamban">Mempunyai Jamban keluarga</label>
                                                    @if ($family->jamban == 'Iya')
                                                        <select class="form-select round" id="jamban" name="jamban">
                                                            <option value="Iya" selected>Iya</option>
                                                            <option value="Tidak">Tidak</option>
                                                        </select>
                                                    @else
                                                        <select class="form-select round" id="jamban" name="jamban">
                                                            <option value="Iya">Iya</option>
                                                            <option value="Tidak" selected>Tidak</option>
                                                        </select>
                                                    @endif

                                                    @error('jamban')
                                                        <div class="text-danger">{{ $message }}</div>
                                                    @enderror
                                                </div>
                                            </div>
                                            <div class="col-lg-12">
                                                <div class="form-group">
                                                    <label for="sumber_air">Sumber Air Keluarga</label>
                                                    @if ($family->sumber_air == 'PDAM')
                                                        <select class="form-select round" id="sumber_air"
                                                            name="sumber_air">
                                                            <option value="PDAM" selected>PDAM</option>
                                                            <option value="Sumur">Sumur</option>
                                                            <option value="Sungai">Sungai</option>
                                                        </select>
                                                    @elseif($family->sumber_air == 'Sumur')
                                                        <select class="form-select round" id="sumber_air"
                                                            name="sumber_air">
                                                            <option value="PDAM">PDAM</option>
                                                            <option value="Sumur" selected>Sumur</option>
                                                            <option value="Sungai">Sungai</option>
                                                        </select>
                                                    @else
                                                        <select class="form-select round" id="sumber_air"
                                                            name="sumber_air">
                                                            <option value="PDAM">PDAM</option>
                                                            <option value="Sumur">Sumur</option>
                                                            <option value="Sungai" selected>Sungai</option>
                                                    @endif

                                                    @error('sumber_air')
                                                        <div class="text-danger">{{ $message }}</div>
                                                    @enderror
                                                </div>
                                            </div>
                                            <div class="col-lg-12">
                                                <div class="form-group">
                                                    <label for="sampah">Memiliki Tempat Pembuangan Sampah</label>
                                                    @if ($family->pembuangan_sampah == 'Iya')
                                                        <select class="form-select round" id="sampah"
                                                            name="pembuangan_sampah">
                                                            <option value="Iya" selected>Iya</option>
                                                            <option value="Tidak">Tidak</option>
                                                        </select>
                                                    @else
                                                        <select class="form-select round" id="sampah"
                                                            name="pembuangan_sampah">
                                                            <option value="Iya">Iya</option>
                                                            <option value="Tidak" selected>Tidak</option>
                                                        </select>
                                                    @endif

                                                    @error('pembuangan_sampah')
                                                        <div class="text-danger">{{ $message }}</div>
                                                    @enderror
                                                </div>
                                            </div>
                                            <div class="col-lg-12">
                                                <div class="form-group">
                                                    <label for="air_limbah">Memiliki Saluran Pembuagan Air Limbah</label>
                                                    @if ($family->pembuangan_air_limbah == 'Iya')
                                                        <select class="form-select round" id="air_limbah"
                                                            name="pembuangan_air_limbah">
                                                            <option value="Iya" selected>Iya</option>
                                                            <option value="Tidak">Tidak</option>
                                                        </select>
                                                    @else
                                                        <select class="form-select round" id="air_limbah"
                                                            name="pembuangan_air_limbah">
                                                            <option value="Iya">Iya</option>
                                                            <option value="Tidak" selected>Tidak</option>
                                                        </select>
                                                    @endif

                                                    @error('pembuangan_air_limbah')
                                                        <div class="text-danger">{{ $message }}</div>
                                                    @enderror
                                                </div>
                                            </div>
                                            <div class="col-lg-12">
                                                <div class="form-group">
                                                    <label for="p4k">Menempelkan Stiker P4k</label>
                                                    @if ($family->stiker_p4k == 'Iya')
                                                        <select class="form-select round" id="p4k"
                                                            name="stiker_p4k">
                                                            <option value="Iya" selected>Iya</option>
                                                            <option value="Tidak">Tidak</option>
                                                        </select>
                                                    @else
                                                        <select class="form-select round" id="p4k"
                                                            name="stiker_p4k">
                                                            <option value="Iya">Iya</option>
                                                            <option value="Tidak" selected>Tidak</option>
                                                        </select>
                                                    @endif

                                                    @error('stiker_p4k')
                                                        <div class="text-danger">{{ $message }}</div>
                                                    @enderror
                                                </div>
                                            </div>
                                            <div class="col-lg-12">
                                                <div class="form-group">
                                                    <label for="kriteria_rumah">Kriteria Rumah</label>
                                                    @if ($family->kriteria_rumah == 'Sehat')
                                                        <div class="form-check">
                                                            <input class="form-check-input" type="radio" checked
                                                                name="kriteria_rumah" value="Sehat" id="sehat">
                                                            <label class="form-check-label" for="sehat">
                                                                Sehat
                                                            </label>
                                                        </div>
                                                        <div class="form-check">
                                                            <input class="form-check-input" type="radio"
                                                                name="kriteria_rumah" value="Tidak Sehat"
                                                                id="tidak sehat">
                                                            <label class="form-check-label" for="tidak sehat">
                                                                Tidak Sehat
                                                            </label>
                                                        </div>
                                                    @else
                                                        <div class="form-check">
                                                            <input class="form-check-input" type="radio"
                                                                name="kriteria_rumah" value="Sehat" id="sehat">
                                                            <label class="form-check-label" for="sehat">
                                                                Sehat
                                                            </label>
                                                        </div>
                                                        <div class="form-check">
                                                            <input class="form-check-input" type="radio" checked
                                                                name="kriteria_rumah" value="Tidak Sehat"
                                                                id="tidak sehat">
                                                            <label class="form-check-label" for="tidak sehat">
                                                                Tidak Sehat
                                                            </label>
                                                        </div>
                                                    @endif

                                                    @error('kriteria_rumah')
                                                        <div class="text-danger">{{ $message }}</div>
                                                    @enderror
                                                </div>
                                            </div>
                                            <div class="col-lg-12">
                                                <div class="form-group">
                                                    <label for="up2k">Aktivitas UP2K</label>
                                                    @if ($family->up2k == 'Iya')
                                                        <select class="form-select round" id="up2k" name="up2k">
                                                            <option value="Iya" selected>Iya</option>
                                                            <option value="Tidak">Tidak</option>
                                                        </select>
                                                    @else
                                                        <select class="form-select round" id="up2k" name="up2k">
                                                            <option value="Iya">Iya</option>
                                                            <option value="Tidak" selected>Tidak</option>
                                                        </select>
                                                    @endif

                                                    @error('up2k')
                                                        <div class="text-danger">{{ $message }}</div>
                                                    @enderror
                                                </div>
                                            </div>
                                            <div class="col-lg-12">
                                                <div class="form-group">
                                                    <label for="kukl">Aktivitas Kegiatan Usaha Lingkungan</label>
                                                    @if ($family->kukl == 'Iya')
                                                        <select class="form-select round" id="kukl" name="kukl">
                                                            <option value="Iya" selected>Iya</option>
                                                            <option value="Tidak">Tidak</option>
                                                        </select>
                                                    @else
                                                        <select class="form-select round" id="kukl" name="kukl">
                                                            <option value="Iya">Iya</option>
                                                            <option value="Tidak" selected>Tidak</option>
                                                        </select>
                                                    @endif

                                                    @error('kukl')
                                                        <div class="text-danger">{{ $message }}</div>
                                                    @enderror
                                                </div>
                                            </div>
                                            <button type="submit" class="btn btn-primary">Simpan</button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    @endsection
