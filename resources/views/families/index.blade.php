@section('title', 'Data Keluarga')
@extends('layouts.voler')
@section('content')
    <div id="main">
        <x-navbar></x-navbar>

        <div class="main-content container-fluid">
            <div class="page-title">
                <div class="row">
                    <div class="col-12 col-md-6 order-md-1 order-last">
                        <h3>@yield('title')</h3>
                    </div>
                    <div class="col-12 col-md-6 order-md-2 order-first">
                        <nav aria-label="breadcrumb" class='breadcrumb-header'>
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="{{ route('dashboard') }}">Dashboard</a></li>
                                <li class="breadcrumb-item active" aria-current="page">@yield('title')</li>
                            </ol>
                        </nav>
                    </div>
                </div>
                <section class="section">
                    <div class="row mb-2">
                        <div class="col-12 col-md-6">
                            <div class="card card-statistic">
                                <div class="card-body p-0">
                                    <div class="d-flex flex-column">
                                        <div class='px-3 py-3 d-flex justify-content-between'>
                                            <h4 class='card-title'>Total Laki-laki</h4>
                                            <div class="card-right d-flex align-items-center">
                                                <p>{{ $lk }}</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-12 col-md-6">
                            <div class="card card-statistic">
                                <div class="card-body p-0">
                                    <div class="d-flex flex-column">
                                        <div class='px-3 py-3 d-flex justify-content-between'>
                                            <h4 class='card-title'>Total Perempuan</h4>
                                            <div class="card-right d-flex align-items-center">
                                                <p>{{ $pr }}</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
            </div>
            </section>
        </div>
        <section class="section">
            <div class="card">
                <div class="card-header row">
                    <div class="col-lg-2">
                        <a href="{{ route('families.create') }}" class="btn icon icon-left btn-primary">
                            <i data-feather="plus"></i>
                            <span>Tambah Data</span>
                        </a>
                    </div>
                    <div class="col-lg-3">
                        <a href="{{ route('families.download') }}" class="btn icon btn-success">
                            <i data-feather="download"></i>
                        </a>
                    </div>
                </div>
                <div class="card-body">
                    <table class='table' id="table1">
                        <thead>
                            <tr>
                                <th>No</th>
                                <th>Dasa Wisma</th>
                                <th>RT</th>
                                <th>RW</th>
                                <th>Nama Kepala Rumah Tangga</th>
                                <th>Aksi</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($families as $family)
                                <tr>
                                    <td>{{ $loop->iteration }}</td>
                                    <td>{{ $family->desa }}</td>
                                    <td>{{ $family->rt }}</td>
                                    <td>{{ $family->rw }}</td>
                                    <td>{{ $family->nama_kk }}</td>
                                    <td>
                                        <!-- <a href="{{ route('cardfamily.create', $family->id) }}"
                                            class="btn icon icon-left btn-success btn-sm">
                                            <i data-feather="plus"></i>
                                            <span>KK</span>
                                        </a> -->
                                        <div class="row">
                                            <div class="col-lg-3">
                                                <a href="{{ route('cardfamily.show', [$family->id, $family->id]) }}"
                                                    class="btn icon icon-left btn-primary btn-sm">
                                                    <i data-feather="eye"></i>
                                                    <!-- <span>KK</span> -->
                                                </a>
                                            </div>
                                            <div class="col-lg-3">
                                                <a href="{{ route('families.show', $family->id) }}"
                                                    class="btn icon btn-info btn-sm">
                                                    <i data-feather="printer"></i>
                                                </a>
                                            </div>
                                            <div class="col-lg-3">
                                                <a href="{{ route('families.edit', $family->id) }}"
                                                    class="btn icon icon-left btn-warning btn-sm">
                                                    <i data-feather="edit"></i>
                                                </a>
                                            </div>
                                            <div class="col-lg-3">
                                                <form action="{{ route('families.destroy', $family->id) }}" method="POST"
                                                class="d-inline">
                                                    @csrf
                                                    @method('delete')
                                                    <button type="submit" class="btn icon icon-left btn-danger btn-sm"
                                                        onclick="return confirm('Yakin ingin menghapus data?')">
                                                        <i data-feather="trash"></i>
                                                    </button>
                                                </form>
                                            </div>
                                            
                                        </div>
                                    </td>
                                </tr>
                            @endforeach

                            @if ($families->count() == 0)
                                <tr>
                                    <td colspan="4" class="text-center">Data Kosong</td>
                                </tr>
                            @endif
                        </tbody>
                    </table>
                </div>
            </div>

        </section>
    </div>
    <x-footer></x-footer>
    </div>
@endsection
