<div class="card">
    <button type="button" class="card-header" data-bs-toggle="collapse" href="#collapseAnggotaKeluarga" role="button" aria-expanded="false" aria-controls="collapseAnggotaKeluarga">
        <h4 class="fw-bolder">Anggota Keluarga</h4>
    </button>
    <div class="collapse card-body show" id="collapseAnggotaKeluarga">
        
        <div class="row">
        
            <div class="col-lg-12">
                <div class="form-group">
                    <label for="nama_kk">Nama Kepala Rumah Tangga</label>
                    <div class="row">
                        <div class="col-lg-6">
                            <input type="text" name="search_nik" id="search_nik"
                            class="form-control round"
                            placeholder="Masukkan Nik Nama Kepala Rumah Tanggga">
                        </div>
                        <div class="col-lg-5">
                            <button type="button" class="btn btn-info btn-sm" id="search_by_nik">Cari</button>
                        </div>
                    </div>

                    @error('nama_kk')
                        <div class="text-danger">{{ $message }}</div>
                    @enderror
                </div>
            </div>
            <div id="show_nama_kk" class="col-lg-12">
                <div class="form-group">
                    <input type="text" name="nama_kk_show" id="nama_kk_show" class="form-control round" value="" disabled>
                    <input type="hidden" name="nama_kk" id="nama_kk" class="form-control round" value="">
                    <input type="hidden" name="id_kk" id="id_kk" class="form-control round" value="">
                </div>
            </div>
            <div class="col-lg-12">
                <div class="form-group">
                    <label for="ttl_anggota">Jumlah Anggota Keluarga</label>
                    <input type="number" name="ttl_anggota" id="ttl_anggota"
                        class="form-control round" placeholder="Jumlah Anggota Keluarga">

                    @error('ttl_anggota')
                        <div class="text-danger">{{ $message }}</div>
                    @enderror
                </div>
            </div>
            <div class="col-lg-12">
                <div class="form-group">
                    <label for="jml_lk">Laki-laki</label>
                    <input type="number" name="jml_lk" id="jml_lk"
                        class="form-control round" placeholder="Laki-laki">

                    @error('jml_lk')
                        <div class="text-danger">{{ $message }}</div>
                    @enderror
                </div>
            </div>
            <div class="col-lg-12">
                <div class="form-group">
                    <label for="jml_pr">Perempuan</label>
                    <input type="number" name="jml_pr" id="jml_pr"
                        class="form-control round" placeholder="Perempuan">

                    @error('jml_pr')
                        <div class="text-danger">{{ $message }}</div>
                    @enderror
                </div>
            </div>
            <div class="col-lg-12">
                <div class="form-group">
                    <label for="jml_kk">Jumlah KK</label>
                    <input type="number" name="jml_kk" id="jml_kk"
                        class="form-control round" placeholder="Jumlah KK">

                    @error('jml_kk')
                        <div class="text-danger">{{ $message }}</div>
                    @enderror
                </div>
            </div>
            <div class="col-lg-12">
                <div class="form-group">
                    <label for="balita">Balita</label>
                    <input type="number" name="jml_balita" id="balita"
                        class="form-control round" placeholder="Balita">

                    @error('jml_balita')
                        <div class="text-danger">{{ $message }}</div>
                    @enderror
                </div>
            </div>
            <div class="col-lg-12">
                <div class="form-group">
                    <label for="anak">Anak</label>
                    <input type="number" name="jml_anak" id="anak"
                        class="form-control round" placeholder="Anak">

                    @error('jml_anak')
                        <div class="text-danger">{{ $message }}</div>
                    @enderror
                </div>
            </div>
            <div class="col-lg-12">
                <div class="form-group">
                    <label for="pus">PUS</label>
                    <input type="number" name="jml_pus" id="pus"
                        class="form-control round" placeholder="PUS">

                    @error('jml_pus')
                        <div class="text-danger">{{ $message }}</div>
                    @enderror
                </div>
            </div>
            <div class="col-lg-12">
                <div class="form-group">
                    <label for="wus">WUS</label>
                    <input type="number" name="jml_wus" id="wus"
                        class="form-control round" placeholder="WUS">

                    @error('jml_wus')
                        <div class="text-danger">{{ $message }}</div>
                    @enderror
                </div>
            </div>
            <div class="col-lg-12">
                <div class="form-group">
                    <label for="buta">Buta</label>
                    <input type="number" name="jml_buta" id="buta"
                        class="form-control round" placeholder="Buta">

                    @error('jml_buta')
                        <div class="text-danger">{{ $message }}</div>
                    @enderror
                </div>
            </div>
            <div class="col-lg-12">
                <div class="form-group">
                    <label for="jml_bumil">Ibu Hamil</label>
                    <input type="number" name="jml_bumil" id="jml_bumil"
                        class="form-control round" placeholder="Ibu Hamil">

                    @error('jml_bumil')
                        <div class="text-danger">{{ $message }}</div>
                    @enderror
                </div>
            </div>
            <div class="col-lg-12">
                <div class="form-group">
                    <label for="jml_busu">Ibu Menyusui</label>
                    <input type="number" name="jml_busu" id="jml_busu"
                        class="form-control round" placeholder="Ibu Menyusui">

                    @error('jml_busu')
                        <div class="text-danger">{{ $message }}</div>
                    @enderror
                </div>
            </div>
            <div class="col-lg-12">
                <div class="form-group">
                    <label for="jml_lansia">Lansia</label>
                    <input type="number" name="jml_lansia" id="jml_lansia"
                        class="form-control round" placeholder="Lansia">

                    @error('jml_lansia')
                        <div class="text-danger">{{ $message }}</div>
                    @enderror
                </div>
            </div>

        </div>

    </div>
</div>