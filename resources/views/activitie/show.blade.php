@section('title', 'Show Aktivitas Warga TP-PKK')
@extends('layouts.voler')
@section('content')
    <div id="main">
        <x-navbar></x-navbar>

        <div class="main-content container-fluid">
            <div class="page-title">
                <div class="row">
                    <div class="col-12 col-md-6 order-md-1 order-last">
                        <h3>@yield('title') {{ $inhabitant->nama }}</h3>
                        <p class="text-subtitle text-muted">We use 'simple-datatables' made by @fiduswriter. You can check
                            the full documentation <a href="https://github.com/fiduswriter/Simple-DataTables/wiki">here</a>.
                        </p>
                    </div>
                    <div class="col-12 col-md-6 order-md-2 order-first">
                        <nav aria-label="breadcrumb" class='breadcrumb-header'>
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="{{ route('dashboard') }}">Dashboard</a></li>
                                <li class="breadcrumb-item active" aria-current="page">@yield('title')</li>
                            </ol>
                        </nav>
                    </div>
                </div>
            </div>
            <section class="section">
                <div class="card">
                    <div class="card-header">
                        <a hreg="{{ route('activity.create', $inhabitant->id) }}" class="btn btn-primary"
                            data-bs-toggle="modal" data-bs-target="#tambah">
                            <i data-feather="plus"></i>
                            <span>Aktivitas</span>
                        </a>
                    </div>
                    <div class="card-body">
                        <table class='table' id="table1">
                            <thead>
                                <tr>
                                    <th>NO</th>
                                    <th>Kegiatan</th>
                                    <th>Aktivitas</th>
                                    <th>Keterangan</th>
                                    <th>AKSI</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($activities as $activity)
                                    <tr>
                                        <td>{{ $loop->iteration }}</td>
                                        <td>{{ $activity->nama_kegiatan }}</td>
                                        <td>{{ $activity->status }}</td>
                                        <td>{{ $activity->deskripsi }}</td>
                                        <td>
                                            <a href="{{ route('activity.edit', [$inhabitant->id, $activity->id]) }}"
                                                class="btn icon icon-left btn-warning btn-sm">
                                                <i data-feather="edit"></i>
                                            </a>

                                            <form action="{{ route('activity.destroy', [$inhabitant->id, $activity->id]) }}"
                                                method="POST" class="d-inline">
                                                @csrf
                                                @method('delete')
                                                <button type="submit" class="btn icon icon-left btn-danger btn-sm"
                                                    onclick="return confirm('Yakin ingin menghapus data?')">
                                                    <i data-feather="trash"></i>
                                                </button>
                                            </form>
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>

            </section>

            {{-- Modal Tambah --}}
            <div class="modal fade text-left" id="tambah" tabindex="-1" role="dialog" aria-labelledby="myModalTambah"
                aria-hidden="true">
                <div class="modal-dialog modal-dialog-centered modal-dialog-scrollable" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h4 class="modal-title" id="myModalTambah">Tambah Aktivitas</h4>
                            <button type="button" class="close" data-bs-dismiss="modal" aria-label="Close">
                                <i data-feather="x"></i>
                            </button>
                        </div>
                        <form action="{{ route('activity.store', $inhabitant->id) }}" method="POST">
                            @csrf
                            <div class="modal-body">
                                <div class="form-group">
                                    <label for="kegiatan">Kegiatan :</label>
                                    <div class="input-group">
                                        <select class="form-select round" id="kegiatan" name="nama_kegiatan">
                                            <option selected>Pilih...</option>
                                            <option value="Penghayatan dan Pengamalan Pancasila">Penghayatan
                                                dan
                                                Pengamalan Pancasila</option>
                                            <option value="Kerja Bakti">Kerja Bakti</option>
                                            <option value="Rukun Kematian">Rukun Kematian</option>
                                            <option value="Kegiatan Keagamaan">Kegiatan Keagamaan</option>
                                            <option value="Jimpitan">Jimpitan</option>
                                            <option value="Arisan">Arisan</option>
                                            <option value="Lain-lain">Lain-lain</option>
                                        </select>
                                    </div>

                                    @error('kegiatan')
                                        <div class="text-danger">{{ $message }}</div>
                                    @enderror
                                </div>
                                <div class="form-group">
                                    <label for="status">Aktivitas</label>
                                    <div class="form-check">
                                        <input class="form-check-input" type="radio" name="status" value="YA"
                                            id="y">
                                        <label class="form-check-label" for="y">
                                            YA
                                        </label>
                                    </div>
                                    <div class="form-check">
                                        <input class="form-check-input" type="radio" name="status" value="TIDAK"
                                            id="t">
                                        <label class="form-check-label" for="t">
                                            TIDAK
                                        </label>
                                    </div>

                                    @error('status')
                                        <div class="text-danger">{{ $message }}</div>
                                    @enderror
                                </div>
                                <div class="form-group">
                                    <label for="deskripsi">Keterangan</label>
                                    <input type="text" name="deskripsi" id="deskripsi" class="form-control round"
                                        placeholder="Desa Wisma">

                                    @error('deskripsi')
                                        <div class="text-danger">{{ $message }}</div>
                                    @enderror
                                </div>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-light-secondary" data-bs-dismiss="modal">
                                    <i class="bx bx-x d-block d-sm-none"></i>
                                    <span class="d-none d-sm-block">Close</span>
                                </button>
                                <button type="submit" class="btn btn-primary ml-1" data-bs-dismiss="modal">
                                    <i class="bx bx-check d-block d-sm-none"></i>
                                    <span class="d-none d-sm-block">Simpan</span>
                                </button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>

        </div>
        <x-footer></x-footer>
    </div>
@endsection
