@section('title', 'Edit Aktivitas Warga TP-PKK')
@extends('layouts.voler')
@section('content')
    <div id="main">
        <x-navbar></x-navbar>

        <div class="main-content container-fluid">
            <div class="page-title">
                <div class="row">
                    <div class="col-12 col-md-6 order-md-1 order-last">
                        <h4>@yield('title') {{ $inhabitant->nama }}</h4>
                        <p class="text-subtitle text-muted">We use 'simple-datatables' made by @fiduswriter. You can check
                            the full documentation <a href="https://github.com/fiduswriter/Simple-DataTables/wiki">here</a>.
                        </p>
                    </div>
                    <div class="col-12 col-md-6 order-md-2 order-first">
                        <nav aria-label="breadcrumb" class='breadcrumb-header'>
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="{{ route('dashboard') }}">Dashboard</a></li>
                                <li class="breadcrumb-item active" aria-current="page">@yield('title')</li>
                            </ol>
                        </nav>
                    </div>
                </div>
            </div>
            <section id="basic-input-groups">
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-content">
                                <div class="card-body">
                                    <form action="{{ route('activity.update', [$inhabitant->id, $activity->id]) }}"
                                        method="POST">
                                        @csrf
                                        @method('PUT')
                                        <div class="row">
                                            <div class="col-lg-12">
                                                <div class="form-group">
                                                    <label for="kegiatan">Kegiatan</label>
                                                    <div class="input-group">
                                                        <select class="form-select round" id="kegiatan"
                                                            name="nama_kegiatan" value="{{ $activity->nama_kegiatan }}">
                                                            @if ($activity->nama_kegiatan == 'Penghayatan dan Pengamalan Pancasila')
                                                                <option value="Penghayatan dan Pengamalan Pancasila"
                                                                    selected>
                                                                    Penghayatan
                                                                    dan
                                                                    Pengamalan Pancasila</option>
                                                                <option value="Kerja Bakti">Kerja Bakti</option>
                                                                <option value="Rukun Kematian">Rukun Kematian</option>
                                                                <option value="Kegiatan Keagamaan">Kegiatan Keagamaan
                                                                </option>
                                                                <option value="Jimpitan">Jimpitan</option>
                                                                <option value="Arisan">Arisan</option>
                                                                <option value="Lain-lain">Lain-lain</option>
                                                            @elseif ($activity->nama_kegiatan == 'Kerja Bakti')
                                                                <option value="Penghayatan dan Pengamalan Pancasila">
                                                                    Penghayatan
                                                                    dan
                                                                    Pengamalan Pancasila</option>
                                                                <option value="Kerja Bakti" selected>Kerja Bakti</option>
                                                                <option value="Rukun Kematian">Rukun Kematian</option>
                                                                <option value="Kegiatan Keagamaan">Kegiatan Keagamaan
                                                                </option>
                                                                <option value="Jimpitan">Jimpitan</option>
                                                                <option value="Arisan">Arisan</option>
                                                                <option value="Lain-lain">Lain-lain</option>
                                                            @elseif ($activity->nama_kegiatan == 'Rukun Kematian')
                                                                <option value="Penghayatan dan Pengamalan Pancasila">
                                                                    Penghayatan
                                                                    dan
                                                                    Pengamalan Pancasila</option>
                                                                <option value="Kerja Bakti">Kerja Bakti</option>
                                                                <option value="Rukun Kematian" selected>Rukun Kematian
                                                                </option>
                                                                <option value="Kegiatan Keagamaan">Kegiatan Keagamaan
                                                                </option>
                                                                <option value="Jimpitan">Jimpitan</option>
                                                                <option value="Arisan">Arisan</option>
                                                                <option value="Lain-lain">Lain-lain</option>
                                                            @elseif ($activity->nama_kegiatan == 'Kegiatan Keagamaan')
                                                                <option value="Penghayatan dan Pengamalan Pancasila">
                                                                    Penghayatan
                                                                    dan
                                                                    Pengamalan Pancasila</option>
                                                                <option value="Kerja Bakti">Kerja Bakti</option>
                                                                <option value="Rukun Kematian">Rukun Kematian</option>
                                                                <option value="Kegiatan Keagamaan" selected>Kegiatan
                                                                    Keagamaan
                                                                </option>
                                                                <option value="Jimpitan">Jimpitan</option>
                                                                <option value="Arisan">Arisan</option>
                                                                <option value="Lain-lain">Lain-lain</option>
                                                            @elseif ($activity->nama_kegiatan == 'Jimpitan')
                                                                <option value="Penghayatan dan Pengamalan Pancasila">
                                                                    Penghayatan
                                                                    dan
                                                                    Pengamalan Pancasila</option>
                                                                <option value="Kerja Bakti">Kerja Bakti</option>
                                                                <option value="Rukun Kematian">Rukun Kematian</option>
                                                                <option value="Kegiatan Keagamaan">Kegiatan Keagamaan
                                                                </option>
                                                                <option value="Jimpitan" selected>Jimpitan</option>
                                                                <option value="Arisan">Arisan</option>
                                                                <option value="Lain-lain">Lain-lain</option>
                                                            @elseif ($activity->nama_kegiatan == 'Arisan')
                                                                <option value="Penghayatan dan Pengamalan Pancasila">
                                                                    Penghayatan
                                                                    dan
                                                                    Pengamalan Pancasila</option>
                                                                <option value="Kerja Bakti">Kerja Bakti</option>
                                                                <option value="Rukun Kematian">Rukun Kematian</option>
                                                                <option value="Kegiatan Keagamaan">Kegiatan Keagamaan
                                                                </option>
                                                                <option value="Jimpitan">Jimpitan</option>
                                                                <option value="Arisan" selected>Arisan</option>
                                                                <option value="Lain-lain">Lain-lain</option>
                                                            @elseif ($activity->nama_kegiatan == 'Lain-lain')
                                                                <option value="Penghayatan dan Pengamalan Pancasila">
                                                                    Penghayatan
                                                                    dan
                                                                    Pengamalan Pancasila</option>
                                                                <option value="Kerja Bakti">Kerja Bakti</option>
                                                                <option value="Rukun Kematian">Rukun Kematian</option>
                                                                <option value="Kegiatan Keagamaan">Kegiatan Keagamaan
                                                                </option>
                                                                <option value="Jimpitan">Jimpitan</option>
                                                                <option value="Arisan">Arisan</option>
                                                                <option value="Lain-lain" selected>Lain-lain</option>
                                                            @endif
                                                        </select>
                                                    </div>

                                                    @error('kegiatan')
                                                        <div class="text-danger">{{ $message }}</div>
                                                    @enderror
                                                </div>
                                            </div>
                                            <div class="col-lg-12">
                                                <div class="form-group">
                                                    <label for="status">Aktivitas</label>
                                                    @if ($activity->status == 'YA')
                                                        <div class="form-check">
                                                            <input class="form-check-input" type="radio" name="status"
                                                                value="YA" id="y" checked>
                                                            <label class="form-check-label" for="y">
                                                                YA
                                                            </label>
                                                        </div>
                                                        <div class="form-check">
                                                            <input class="form-check-input" type="radio" name="status"
                                                                value="TIDAK" id="t">
                                                            <label class="form-check-label" for="t">
                                                                TIDAK
                                                            </label>
                                                        </div>
                                                    @elseif ($activity->status == 'TIDAK')
                                                        <div class="form-check">
                                                            <input class="form-check-input" type="radio" name="status"
                                                                value="YA" id="y">
                                                            <label class="form-check-label" for="y">
                                                                YA
                                                            </label>
                                                        </div>
                                                        <div class="form-check">
                                                            <input class="form-check-input" type="radio" name="status"
                                                                value="TIDAK" id="t" selected>
                                                            <label class="form-check-label" for="t">
                                                                TIDAK
                                                            </label>
                                                        </div>
                                                    @endif

                                                    @error('status')
                                                        <div class="text-danger">{{ $message }}</div>
                                                    @enderror
                                                </div>
                                            </div>
                                            <div class="col-lg-12">
                                                <div class="form-group">
                                                    <label for="deskripsi">Keterangan</label>
                                                    <input type="text" name="deskripsi"
                                                        value="{{ $activity->deskripsi }}" id="deskripsi"
                                                        class="form-control round" placeholder="Desa Wisma">

                                                    @error('deskripsi')
                                                        <div class="text-danger">{{ $message }}</div>
                                                    @enderror
                                                </div>
                                            </div>
                                            <button type="submit" class="btn btn-primary btn-block">Simpan</button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </div>
        <x-footer></x-footer>
    </div>
@endsection
