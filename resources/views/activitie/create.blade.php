@section('title', 'Tambah Aktivitas Warga TP-PKK')
@extends('layouts.voler')
@section('content')
    <div id="main">
        <x-navbar></x-navbar>

        <div class="main-content container-fluid">
            <div class="page-title">
                <div class="row">
                    <div class="col-12 col-md-6 order-md-1 order-last">
                        <h3>@yield('title') {{ $inhabitant->nama }}</h3>
                        <p class="text-subtitle text-muted">We use 'simple-datatables' made by @fiduswriter. You can check
                            the full documentation <a href="https://github.com/fiduswriter/Simple-DataTables/wiki">here</a>.
                        </p>
                    </div>
                    <div class="col-12 col-md-6 order-md-2 order-first">
                        <nav aria-label="breadcrumb" class='breadcrumb-header'>
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="{{ route('dashboard') }}">Dashboard</a></li>
                                <li class="breadcrumb-item active" aria-current="page">@yield('title')</li>
                            </ol>
                        </nav>
                    </div>
                </div>
            </div>
            <section id="basic-input-groups">
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-content">
                                <div class="card-body">
                                    <form action="{{ route('activity.store', $inhabitant->id) }}" method="POST">
                                        @csrf
                                        <div class="row">
                                            <div class="col-lg-12">
                                                <div class="form-group">
                                                    <label for="kegiatan">Kegiatan</label>
                                                    <div class="input-group">
                                                        <select class="form-select round" id="kegiatan"
                                                            name="nama_kegiatan">
                                                            <option selected>Pilih...</option>
                                                            <option value="Penghayatan dan Pengamalan Pancasila">Penghayatan
                                                                dan
                                                                Pengamalan Pancasila</option>
                                                            <option value="Kerja Bakti">Kerja Bakti</option>
                                                            <option value="Rukun Kematian">Rukun Kematian</option>
                                                            <option value="Kegiatan Keagamaan">Kegiatan Keagamaan</option>
                                                            <option value="Jimpitan">Jimpitan</option>
                                                            <option value="Arisan">Arisan</option>
                                                            <option value="Lain-lain">Lain-lain</option>
                                                        </select>
                                                    </div>

                                                    @error('kegiatan')
                                                        <div class="text-danger">{{ $message }}</div>
                                                    @enderror
                                                </div>
                                            </div>
                                            <div class="col-lg-12">
                                                <div class="form-group">
                                                    <label for="status">Aktivitas</label>
                                                    <div class="form-check">
                                                        <input class="form-check-input" type="radio" name="status"
                                                            value="YA" id="y">
                                                        <label class="form-check-label" for="y">
                                                            YA
                                                        </label>
                                                    </div>
                                                    <div class="form-check">
                                                        <input class="form-check-input" type="radio" name="status"
                                                            value="TIDAK" id="t">
                                                        <label class="form-check-label" for="t">
                                                            TIDAK
                                                        </label>
                                                    </div>

                                                    @error('status')
                                                        <div class="text-danger">{{ $message }}</div>
                                                    @enderror
                                                </div>
                                            </div>
                                            <div class="col-lg-12">
                                                <div class="form-group">
                                                    <label for="deskripsi">Keterangan</label>
                                                    <input type="text" name="deskripsi" id="deskripsi"
                                                        class="form-control round" placeholder="Desa Wisma">

                                                    @error('deskripsi')
                                                        <div class="text-danger">{{ $message }}</div>
                                                    @enderror
                                                </div>
                                            </div>
                                            <button type="submit" class="btn btn-primary btn-block">Simpan</button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </div>
        <x-footer></x-footer>
    </div>
@endsection
