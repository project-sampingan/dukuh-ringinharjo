@section('title', 'Tambah Data Warga Pendatang')
@extends('layouts.voler')
@section('content')
<div id="main">
    <x-navbar></x-navbar>
    <div class="main-content container-fluid">
        <div class="page-title">
            <div class="row">
                <div class="col-12 col-md-6 order-md-1 order-last">
                    <h4>@yield('title')</h4>
                </div>
                <div class="col-12 col-md-6 order-md-2 order-first">
                    <nav aria-label="breadcrumb" class='breadcrumb-header'>
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="{{ route('pendatang.index') }}">Pendatang</a></li>
                            <li class="breadcrumb-item active" aria-current="page">@yield('title')</li>
                        </ol>
                    </nav>
                </div>
            </div>
        </div>

        <section id="basic-input-groups">
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-content">
                            <div class="card-body">
                                <form action="{{ route('pendatang.update', $pendatang->id) }}" method="POST">
                                    @method('PUT')
                                    @csrf
                                    <div class="row">
                                        <div class="col-lg-12">
                                            <div class="form-group">
                                                <label for="nik">NIK</label>
                                                <input type="number" name="nik" id="nik"
                                                    class="form-control round" value="{{ $pendatang->nik }}">

                                                @error('nik')
                                                    <div class="text-danger">{{ $message }}</div>
                                                @enderror
                                            </div>
                                        </div>
                                        <div class="col-lg-12">
                                            <div class="form-group">
                                                <label for="kk">KK</label>
                                                <input type="number" name="kk" id="kk"
                                                    class="form-control round" value="{{ $pendatang->kk }}">

                                                @error('kk')
                                                    <div class="text-danger">{{ $message }}</div>
                                                @enderror
                                            </div>
                                        </div>
                                        <div class="col-lg-12">
                                            <div class="form-group">
                                                <label for="nama">Nama Lengkap</label>
                                                <input type="text" name="nama" id="nama"
                                                    class="form-control round" value="{{ $pendatang->nama }}">

                                                @error('nama')
                                                    <div class="text-danger">{{ $message }}</div>
                                                @enderror
                                            </div>
                                        </div>
                                        <div class="col-lg-12">
                                            <div class="form-group">
                                                <label for="jenis_kelamin">Jenis Kelamin</label>

                                                <div class="container">
                                                    <div class="row">
                                                        <div class="form-check col-lg-2 col-md-6 col-12">
                                                            <input class="form-check-input" type="radio"
                                                                name="jenis_kelamin" value="L" id="laki-laki" {{ ($pendatang->jenis_kelamin == 'L') ? 'checked' : '' }}>
                                                            <label class="form-check-label" for="laki-laki">
                                                                Laki-Laki
                                                            </label>
                                                        </div>

                                                        <div class="form-check col-lg-2 col-md-6 col-12">
                                                            <input class="form-check-input" type="radio"
                                                                name="jenis_kelamin" value="P" id="perempuan" {{ ($pendatang->jenis_kelamin == 'P') ? 'checked' : '' }}>
                                                            <label class="form-check-label" for="perempuan">
                                                                Perempuan
                                                            </label>
                                                        </div>
                                                    </div>
                                                </div>

                                                @error('jenis_kelamin')
                                                    <div class="text-danger">{{ $message }}</div>
                                                @enderror
                                            </div>
                                        </div>
                                        <div class="col-lg-12">
                                            <div class="form-group">
                                                <label for="tgl_lahir">Tanggal Lahir</label>
                                                <input type="date" name="tgl_lahir" id="tgl_lahir"
                                                    class="form-control round" placeholder="tanggal lahir" value="{{ $pendatang->tgl_lahir }}">

                                                @error('tgl_lahir')
                                                    <div class="text-danger">{{ $message }}</div>
                                                @enderror
                                            </div>
                                        </div>
                                        <div class="col-lg-12">
                                            <div class="form-group">
                                                <label for="alamat_asal">Alamat Asal</label>
                                            </div>
                                        </div>
                                        <div class="col-lg-12">
                                            <div class="form-group">
                                                <select class="form-select round" id="provinsi" name="provinsi" onchange="get_kabupaten(true)" disabled>
                                                    <option selected value="{{ $pendatang->provinsi }}">{{ $pendatang->provinsi }}</option>
                                                </select>

                                                @error('provinsi')
                                                    <div class="text-danger">{{ $message }}</div>
                                                @enderror
                                            </div>
                                        </div>
                                        <div class="col-lg-12">
                                            <div class="form-group">
                                                <select class="form-select round" id="kabupaten" name="kabupaten" onchange="get_kecamatan(true)" disabled>
                                                    <option selected value="{{ $pendatang->kabupaten }}">Kabupaten</option>
                                                </select>

                                                @error('kabupaten')
                                                    <div class="text-danger">{{ $message }}</div>
                                                @enderror
                                            </div>
                                        </div>
                                        <div class="col-lg-12">
                                            <div class="form-group">
                                                <select class="form-select round" id="kecamatan" name="kecamatan" onchange="get_kelurahan(true)" disabled>
                                                    <option selected value="{{ $pendatang->kecamatan }}">Kecamatan</option>
                                                </select>

                                                @error('kecamatan')
                                                    <div class="text-danger">{{ $message }}</div>
                                                @enderror
                                            </div>
                                        </div>
                                        <div class="col-lg-12">
                                            <div class="form-group">
                                                <select class="form-select round" id="kelurahan" name="kelurahan" disabled>
                                                    <option selected value="{{ $pendatang->kelurahan }}">Kelurahan</option>
                                                </select>

                                                @error('kelurahan')
                                                    <div class="text-danger">{{ $message }}</div>
                                                @enderror
                                            </div>
                                        </div>
                                        <div class="col-lg-12">
                                            <div class="form-group">
                                                <label for="rt">RT</label>
                                                <input type="text" name="rt" id="rt"
                                                    class="form-control round" placeholder="rt" maxlength="9" value="{{ $pendatang->rt }}">

                                                @error('rt')
                                                    <div class="text-danger">{{ $message }}</div>
                                                @enderror
                                            </div>
                                        </div>
                                        <div class="col-lg-12">
                                            <div class="form-group">
                                                <label for="desa">Desa</label>
                                                <input type="text" name="desa" id="desa"
                                                    class="form-control round" placeholder="desa" maxlength="100" value="{{ $pendatang->desa }}">

                                                @error('desa')
                                                    <div class="text-danger">{{ $message }}</div>
                                                @enderror
                                            </div>
                                        </div>
                                        <div class="col-lg-12">
                                            <div class="form-group">
                                                <label for="alamat_lengkap">Alamat Lengkap</label>
                                                <textarea class="form-control round" name="alamat_lengkap">{{ $pendatang->alamat_lengkap }}</textarea>

                                                @error('alamat_lengkap')
                                                    <div class="text-danger">{{ $message }}</div>
                                                @enderror
                                            </div>
                                        </div>
                                        <div class="col-lg-12">
                                            <div class="form-group">
                                                <label for="domisili">Domisili</label>
                                                <textarea class="form-control round" name="domisili">{{ $pendatang->domisili }}</textarea>

                                                @error('domisili')
                                                    <div class="text-danger">{{ $message }}</div>
                                                @enderror
                                            </div>
                                        </div>
                                        <div class="col-lg-12">
                                            <div class="form-group">
                                                <label for="tgl_masuk">Tanggal Masuk</label>
                                                <input type="date" name="tgl_masuk" id="tgl_masuk"
                                                    class="form-control round" placeholder="tanggal masuk" maxlength="100" value="{{ $pendatang->tgl_masuk }}">

                                                @error('tgl_masuk')
                                                    <div class="text-danger">{{ $message }}</div>
                                                @enderror
                                            </div>
                                        </div>
                                        <div class="col-lg-12">
                                            <div class="form-group">
                                                <label for="tgl_keluar">Tanggal Keluar</label>
                                                <input type="date" name="tgl_keluar" id="tgl_keluar"
                                                    class="form-control round" placeholder="tanggal keluar" maxlength="100" value="{{ $pendatang->tgl_keluar }}">

                                                @error('tgl_keluar')
                                                    <div class="text-danger">{{ $message }}</div>
                                                @enderror
                                            </div>
                                        </div>
                                        <div class="col-lg-12">
                                            <div class="form-group">
                                                <label for="keterangan">Keterangan</label>
                                                <textarea class="form-control round" name="keterangan">{{ $pendatang->keterangan }}</textarea>

                                                @error('keterangan')
                                                    <div class="text-danger">{{ $message }}</div>
                                                @enderror
                                            </div>
                                        </div>
                                        <button type="submit" class="btn btn-primary btn-block">Simpan</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <script>
            tampil_provinsi()
            get_kabupaten()
            function tampil_provinsi() {
                var provinsi = $('#provinsi').val()

                if (provinsi) {
                    var explode_provinsi = provinsi.split(';')
                    let html = `<option selected value="${explode_provinsi[0]};${explode_provinsi[1]}">${explode_provinsi[1]}</option>`

                    $.ajax({
                        method: 'GET',
                        url: '{{ url("api/warga/get_province") }}',
                        crossDomain: true,
                        dataType: 'json',
                        data: {
                            key: '83a09db8abeafa00b2569d713890b0c7'
                        }
                    })
                    .done(function(data) {
                        data.data.forEach(element => {
                            if (explode_provinsi[1] != element.provinsi_nama) {
                                html += `<option value="${element.provinsi_id};${element.provinsi_nama}">${element.provinsi_nama}</option>`
                            }
                        });

                        $('#provinsi').html('')
                        $('#provinsi').html(html)
                        $('#provinsi').prop('disabled', false)
                        
                    })
                }
            }

            function get_kabupaten(change) {
                var provinsi = $('#provinsi').val()
                var kabupaten = $('#kabupaten').val()

                if (provinsi) {
                    var explode_provinsi = provinsi.split(';')
                    var explode_kabupaten = kabupaten.split(';')
                    $('#kabupaten').html('')
                          
                    let html = ''
                    if (change) {
                        // html += '<option selected disabled value="">Kabupaten</option>'
                        $('#kecamatan').html('<option selected disabled value="">Kecamatan</option>')
                        $('#kelurahan').html('<option selected disabled value="">Kelurahan</option>')
                    }
                    $.ajax(
                        {
                            method: 'GET',
                            url: '{{ url("api/warga/get_kabupaten") }}',
                            crossDomain: true,
                            dataType: 'json',
                            data: {
                                province: explode_provinsi[0]
                            }
                        }
                    )
                    .fail(function(error){
                        alert('error ', error.status)
                    })
                    .done(function(data) {
                        data.data.forEach(element => {
                            if (!change && element.kabupatenkota_nama == explode_kabupaten[1]) {
                                html += `<option selected value="${element.kabupatenkota_id};${element.kabupatenkota_nama}">${element.kabupatenkota_nama}</option>`
                            } else {
                                html += `<option value="${element.kabupatenkota_id};${element.kabupatenkota_nama}">${element.kabupatenkota_nama}</option>`
                            }
                        });
                        $('#kabupaten').html('')
                        $('#kabupaten').html(html)
                        $('#kabupaten').prop('disabled', false)
                        get_kecamatan()
                    })
                }
                
            }

            function get_kecamatan(change) {
                var kabupaten = $('#kabupaten').val()
                var kecamatan = $('#kecamatan').val()

                if (kabupaten) {
                    var explode_kabupaten = kabupaten.split(';')
                    var explode_kecamatan = (kecamatan) ? kecamatan.split(';') : null
                    let html = ''
                    if (change) {
                        // html += '<option selected disabled value="">Kecamatan</option>'
                        $('#kelurahan').html('<option selected disabled value="">Kelurahan</option>')
                    }
                    $.ajax(
                        {
                            method: 'GET',
                            url: '{{ url("api/warga/get_kecamatan") }}',
                            crossDomain: true,
                            dataType: 'json',
                            data: {
                                kabupaten: explode_kabupaten[0]
                            }
                        }
                    )
                    .fail(function(error){
                        alert('error ', error.status)
                    })
                    .done(function(data) {
                        data.data.forEach(element => {
                            if (!change && explode_kecamatan && element.kecamatan_nama == explode_kecamatan[1]) {
                                html += `<option selected value="${element.kecamatan_id};${element.kecamatan_nama}">${element.kecamatan_nama}</option>`
                            } else {
                                html += `<option value="${element.kecamatan_id};${element.kecamatan_nama}">${element.kecamatan_nama}</option>`
                            }
                        });
                        $('#kecamatan').html('')
                        $('#kecamatan').html(html)
                        $('#kecamatan').prop('disabled', false)
                        get_kelurahan()
                    })
                }
                
            }

            function get_kelurahan(change) {
                var kecamatan = $('#kecamatan').val()
                var kelurahan = $('#kelurahan').val()
                console.log('kecamatan ::', kecamatan)
                if (kecamatan) {
                    var explode_kecamatan = kecamatan.split(';')
                    var explode_kelurahan = (kelurahan) ? kelurahan.split(';') : null

                    $('#kelurahan').html('')
                    
                    // let html = '<option selected disabled value="">Kelurahan</option>'
                    let html = ''
                    $.ajax(
                        {
                            method: 'GET',
                            url: '{{ url("api/warga/get_kelurahan") }}',
                            crossDomain: true,
                            dataType: 'json',
                            data: {
                                kecamatan: explode_kecamatan[0]
                            }
                        }
                    )
                    .fail(function(error){
                        alert('error ', error.status)
                    })
                    .done(function(data) {
                        data.data.forEach(element => {
                            if (!change && kelurahan && element.kelurahan_nama == explode_kelurahan[1]) {
                                html += `<option selected value="${element.kelurahan_id};${element.kelurahan_nama}">${element.kelurahan_nama}</option>`
                            } else {
                                html += `<option value="${element.kelurahan_id};${element.kelurahan_nama}">${element.kelurahan_nama}</option>`
                            }
                        });
                        $('#kelurahan').html('')
                        $('#kelurahan').html(html)
                        $('#kelurahan').prop('disabled', false)
                    })
                }
                
            }
        </script>

    </div>
    <x-footer></x-footer>
</div>
@endsection