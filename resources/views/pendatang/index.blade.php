@section('title', 'Data Warga Pendatang')
@extends('layouts.voler')
@section('content')

<div id="main">
    <x-navbar></x-navbar>

    <div class="main-content container-fluid">
        <div class="page-title">
            <div class="row">
                <div class="col-12 col-md-6 order-md-1 order-last">
                    <h4>@yield('title')</h4>
                </div>
            </div>
        </div>
        <section class="section">
            <div class="card">
                <div class="card-header row">
                    <div class="col-md-1">
                        <a href="{{ route('pendatang.create') }}" class="btn icon icon-left btn-primary">
                            <i data-feather="plus"></i>
                        </a>
                    </div>
                    <div class="col-md-3">
                        <a href="{{ url('download/pendatang') }}" class="btn icon btn-success">
                            <i data-feather="download"></i>
                        </a>
                    </div>
                </div>
                <div class="card-body">
                    <table class='table' id="table1">
                        <thead>
                            <tr>
                                <th>No.</th>
                                <th>Nik</th>
                                <th>KK</th>
                                <th>Nama</th>
                                <th>RT</th>
                                <th>Lama Tinggal</th>
                                <th>Aksi</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($pendatang as $no => $row)
                                <tr>
                                    <td>{{ $no+1 }}</td>
                                    <td>{{ $row->nik }}</td>
                                    <td>{{ $row->kk }}</td>
                                    <td>{{ $row->nama }}</td>
                                    <td>{{ $row->rt }}</td>
                                    <td>
                                        @if ($row->tgl_keluar)
                                            {{ date_diff(date_create($row->tgl_masuk), date_create($row->tgl_keluar))->d }}
                                        @else
                                            {{ date_diff(date_create($row->tgl_masuk), date_create())->d }}
                                        @endif
                                        hari
                                    </td>
                                    <td>
                                        <div class="row">
                                            <div class="col-md-1">
                                                <a href="{{ route('pendatang.show', $row->id) }}"
                                                    class="icon text-info">
                                                    <i data-feather="eye"></i>
                                                </a>
                                            </div>
                                            <div class="col-md-1">
                                                <a href="{{ route('pendatang.edit', $row->id) }}"
                                                    class="icon icon-left text-warning">
                                                    <i data-feather="edit"></i>
                                                </a>
                                            </div>
                                            <div class="col-md-1">
                                                <a href="#" onclick="hapus_data({{ $row->id }})" class="text-danger">
                                                    <i data-feather="trash"></i>
                                                </a>
                                            </div>
                                        </div>
                                    </td>
                                </tr>
                            @endforeach

                            @if ($pendatang->count() == 0)
                                <tr>
                                    <td colspan="6" class="text-center">Data Kosong</td>
                                </tr>
                            @endif
                        </tbody>
                    </table>
                </div>
            </div>

        </section>
    </div>
    <script>
        function hapus_data(id) {
            swal({
                title: "Eiits !!",
                text: "Yakin nih, mau data ini ?",
                icon: "warning",
                buttons: true,
                dangerMode: true,
            })
            .then((willDelete) => {
                if (willDelete) {
                    $.ajax(
                        {
                            method: 'POST',
                            url: '{{ url("api/warga/hapus_warga_pendatang") }}',
                            crossDomain: true,
                            dataType: 'json',
                            data: {
                                id: id
                            }
                        }
                    )
                    .done(function(data) {
                        swal("Ok, data nya berhasil di hapus", {
                            icon: "success",
                        }).then(() => {
                            window.location.reload()
                        })
                    })
                } else {
                    swal("Baiklah")
                }
            })
        }
    </script>
    <x-footer></x-footer>
</div>

@endsection
