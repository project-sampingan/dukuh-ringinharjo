@section('title', 'Detail Data Warga Pendatang')
@extends('layouts.voler')
@section('content')

<div id="main">
    <x-navbar></x-navbar>

    <div class="main-content container-fluid">
        <div class="page-title">
            <div class="row">
                <div class="col-12 col-md-6 order-md-1 order-last">
                    <h4>@yield('title')</h4>
                </div>
            </div>
        </div>

        <section class="section">
            <div class="card">
                <div class="card-body">
                    <table class="table table-hover">
                        <tr>
                            <th>NIK</th>
                            <td>{{ $pendatang->nik }}</td>
                        </tr>
                        <tr>
                            <th>KK</th>
                            <td>{{ $pendatang->kk }}</td>
                        </tr>
                        <tr>
                            <th>Nama Lengkap</th>
                            <td>{{ $pendatang->nama }}</td>
                        </tr>
                        <tr>
                            <th>Jenis Kelamin</th>
                            <td>
                                {{ ($pendatang->jenis_kelamin == 'L') ? 'Laki-Laki' : 'Perempuan' }}
                            </td>
                        </tr>
                        <tr>
                            <th>Tanggal Lahir</th>
                            <td>{{ $pendatang->tgl_lahir }}</td>
                        </tr>
                        <tr>
                            <th colspan="6" class="text-center">Alamat Asal</th>
                        </tr>
                        <tr>
                            <th>Provinsi</th>
                            <td>{{ explode(';', $pendatang->provinsi)[1] }}</td>
                        </tr>
                        <tr>
                            <th>Kabupaten</th>
                            <td>{{ explode(';', $pendatang->kabupaten)[1] }}</td>
                        </tr>
                        <tr>
                            <th>Kecamatan</th>
                            <td>{{ explode(';', $pendatang->kecamatan)[1] }}</td>
                        </tr>
                        <tr>
                            <th>Kelurahan</th>
                            <td>{{ explode(';', $pendatang->kelurahan)[1] }}</td>
                        </tr>
                        <tr>
                            <th>Alamat Lengkap</th>
                            <td>{{ $pendatang->alamat_lengkap }}</td>
                        </tr>
                        <tr>
                            <th colspan="6" class="text-center">Alamat Domisili</th>
                        </tr>
                        <tr>
                            <th>RT</th>
                            <td>{{ $pendatang->rt }}</td>
                        </tr>
                        <tr>
                            <th>Desa</th>
                            <td>{{ $pendatang->desa }}</td>
                        </tr>
                        <tr>
                            <th>Domisili</th>
                            <td>{{ $pendatang->domisili }}</td>
                        </tr>
                        <tr>
                            <th>Tanggal Masuk</th>
                            <td>{{ $pendatang->tgl_masuk }}</td>
                        </tr>
                        <tr>
                            <th>Tanggal Keluar</th>
                            <td>{{ $pendatang->tgl_keluar }}</td>
                        </tr>
                        <tr>
                            <th>Lama Tinggal</th>
                            <td>
                                @if ($pendatang->tgl_keluar)
                                    {{ date_diff(date_create($pendatang->tgl_masuk), date_create($pendatang->tgl_keluar))->d }}
                                @else
                                    {{ date_diff(date_create($pendatang->tgl_masuk), date_create())->d }}
                                @endif
                                hari
                            </td>
                        </tr>
                        <tr>
                            <th>Keterangan</th>
                            <td>{{ $pendatang->keterangan }}</td>
                        </tr>
                    </table>
                </div>
                <div class="card-footer">
                    <a href="{{ route('pendatang.index') }}" class="btn btn-secondary">Back</a>
                </div>
            </div>
        </section>
    </div>

    <x-footer></x-footer>
</div>

@endsection