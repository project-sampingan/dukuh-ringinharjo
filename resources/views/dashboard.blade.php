@section('title', 'Dashboard')
@extends('layouts.voler')
@section('content')
    <div id="main">
        <x-navbar></x-navbar>

        <div class="main-content container-fluid">
            <div class="page-title">
                <h3>Profil Padukuhan Mandingan</h3>
                <p class="text-subtitle text-muted">Mandingan, Kalurahan Ringinharjo, Kapanewon Bantul, Kabupaten Bantul, Provinsi Daerah Istimewa Yogyakarta </p>
            </div>
            <section class="section">
                <div class="row mb-2">
                    <div class="col-12 col-md-4">
                        <div class="card card-statistic">
                            <div class="card-body p-0">
                                <div class="d-flex flex-column">
                                    <div class='px-3 py-3 d-flex justify-content-between'>
                                        <h3 class='card-title'>Total Keluarga</h3>
                                        <div class="card-right d-flex align-items-center">
                                            <p>{{ $dfamilies }}</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-12 col-md-4">
                        <div class="card card-statistic">
                            <div class="card-body p-0">
                                <div class="d-flex flex-column">
                                    <div class='px-3 py-3 d-flex justify-content-between'>
                                        <h3 class='card-title'>Total Orang</h3>
                                        <div class="card-right d-flex align-items-center">
                                            <p>{{ $peoples }}</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-12 col-md-4">
                        <div class="card card-statistic">
                            <div class="card-body p-0">
                                <div class="d-flex flex-column">
                                    <div class='px-3 py-3 d-flex justify-content-between'>
                                        <h3 class='card-title'>Total Warga TP-PKK</h3>
                                        <div class="card-right d-flex align-items-center">
                                            <p>{{ $inhabitants }}</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            <div class="col-lg">
                <div class="card">
                    <div class="card-content">
                        <!-- <img src="{{ asset('voler/assets/images/samples/mandingan.png') }}"
                            class="card-img-top img-fluid" alt="singleminded"> -->
                        <div class="card-body">
                            <!-- <h5 class="card-title">Peta Satelit Mandingan</h5> -->
                            <div style="width: 100%"><iframe width="100%" height="600" frameborder="0" scrolling="no" marginheight="0" marginwidth="0" src="https://maps.google.com/maps?width=100%25&amp;height=600&amp;hl=en&amp;q=Ringin%20Harjo,%20village,%20Ringinharjo,%20Indonesia+(Dusun%20Ringin%20Harjo)&amp;t=k&amp;z=14&amp;ie=UTF8&amp;iwloc=B&amp;output=embed"><a href="https://www.maps.ie/distance-area-calculator.html">measure distance on map</a></iframe></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <x-footer></x-footer>
    </div>
@endsection
