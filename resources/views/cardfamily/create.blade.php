@section('title', 'Tambah KK')
@extends('layouts.voler')
@section('content')
    <div id="main">
        <x-navbar></x-navbar>

        <div class="main-content container-fluid">
            <div class="page-title">
                <div class="row">
                    <div class="col-12 col-md-6 order-md-1 order-last">
                        <h3>@yield('title') {{ $family->nama_kk }}</h3>
                    </div>
                    <div class="col-12 col-md-6 order-md-2 order-first">
                        <nav aria-label="breadcrumb" class='breadcrumb-header'>
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="{{ route('dashboard') }}">Dashboard</a></li>
                                <li class="breadcrumb-item active" aria-current="page">@yield('title')</li>
                            </ol>
                        </nav>
                    </div>
                </div>
            </div>
            <section id="basic-input-groups">
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-content">
                                <div class="card-body">
                                    <form action="{{ route('cardfamily.store', $family->id) }}" method="POST">
                                        @csrf
                                        <div class="row">
                                            <div class="col-lg-12">
                                                <div class="form-group">
                                                    <label for="no_reg">NO. REG</label>
                                                    <input type="number" name="no_reg" id="no_reg"
                                                        class="form-control round" placeholder="NO. REG">

                                                    @error('no_reg')
                                                        <div class="text-danger">{{ $message }}</div>
                                                    @enderror
                                                </div>
                                            </div>
                                            <div class="col-lg-12">
                                                <div class="form-group">
                                                    <label for="nama_anggota_keluarga">Nama Anggota Keluarga</label>
                                                    <input type="text" name="nama_anggota_keluarga"
                                                        id="nama_anggota_keluarga" class="form-control round"
                                                        placeholder="Nama Anggota Keluarga">

                                                    @error('nama_anggota_keluarga')
                                                        <div class="text-danger">{{ $message }}</div>
                                                    @enderror
                                                </div>
                                            </div>
                                            <div class="col-lg-12">
                                                <div class="form-group">
                                                    <label for="status_dlm_keluarga">Status Dalam Keluarga</label>
                                                    <input type="text" name="status_dlm_keluarga"
                                                        id="status_dlm_keluarga" class="form-control round"
                                                        placeholder="Status Dalam Keluarga">

                                                    @error('status_dlm_keluarga')
                                                        <div class="text-danger">{{ $message }}</div>
                                                    @enderror
                                                </div>
                                            </div>
                                            <div class="col-lg-12">
                                                <div class="form-group">
                                                    <label for="status_dlm_perkawinan">Status Dalam Perkawinan</label>
                                                    <div class="form-check">
                                                        <input class="form-check-input" type="radio"
                                                            name="status_dlm_perkawinan" value="Kawin" id="kawin">
                                                        <label class="form-check-label" for="kawin">
                                                            Kawin
                                                        </label>
                                                    </div>
                                                    <div class="form-check">
                                                        <input class="form-check-input" type="radio"
                                                            name="status_dlm_perkawinan" value="Belum Kawin"
                                                            id="belum kawin">
                                                        <label class="form-check-label" for="belum kawin">
                                                            Belum Kawin
                                                        </label>
                                                    </div>

                                                    @error('status_dlm_perkawinan')
                                                        <div class="text-danger">{{ $message }}</div>
                                                    @enderror
                                                </div>
                                            </div>
                                            <div class="col-lg-12">
                                                <div class="form-group">
                                                    <label for="status_dlm_keluarga">Jenis Kelamin</label>
                                                    <div class="form-check">
                                                        <input class="form-check-input" type="radio" name="jenis_kelamin"
                                                            value="Laki-laki" id="lk">
                                                        <label class="form-check-label" for="lk">
                                                            Laki-laki
                                                        </label>
                                                    </div>
                                                    <div class="form-check">
                                                        <input class="form-check-input" type="radio" name="jenis_kelamin"
                                                            value="Perempuan" id="pr">
                                                        <label class="form-check-label" for="pr">
                                                            Perempuan
                                                        </label>
                                                    </div>

                                                    @error('jenis_kelamin')
                                                        <div class="text-danger">{{ $message }}</div>
                                                    @enderror
                                                </div>
                                            </div>
                                            <div class="col-lg-12">
                                                <div class="form-group">
                                                    <label for="lahir_umur">TGL Lahir / Umur</label>
                                                    <input type="date" name="lahir_umur" id="lahir_umur"
                                                        class="form-control round" placeholder="TGL Lahir / Umur">

                                                    @error('lahir_umur')
                                                        <div class="text-danger">{{ $message }}</div>
                                                    @enderror
                                                </div>
                                            </div>
                                            <div class="col-lg-12">
                                                <div class="form-group">
                                                    <label for="pendidikan">Pendidikan</label>
                                                    <select class="form-select round" id="pendidikan" name="pendidikan">
                                                        <option selected>Pilih...</option>
                                                        <option value="Tidak Tamat SD">Tidak Tamat SD</option>
                                                        <option value="SD/MI">SD/MI</option>
                                                        <option value="SMP/Sederajat">SMP/Sederajat</option>
                                                        <option value="SMU/SMK/Sederajat">SMU/SMK/Sederajat</option>
                                                        <option value="Diploma">Diploma</option>
                                                        <option value="S1">S1</option>
                                                        <option value="S2">S2</option>
                                                        <option value="S3">S3</option>
                                                    </select>

                                                    @error('pendidikan')
                                                        <div class="text-danger">{{ $message }}</div>
                                                    @enderror
                                                </div>
                                            </div>
                                            <div class="col-lg-12">
                                                <div class="form-group">
                                                    <label for="pekerjaan">Pekerjaan</label>
                                                    <select class="form-select round" id="pekerjaan" name="pekerjaan">
                                                        <option selected>Pilih...</option>
                                                        <option value="Belum/Tidak Bekerja">Belum/Tidak Bekerja</option>
                                                        <option value="Pelajar/Mahasiswa">Pelajar/Mahasiswa</option>
                                                        <option value="Buruh Tidak Tetap">Buruh Tidak Tetap</option>
                                                        <option value="Buruh Tani">Buruh Tani</option>
                                                        <option value="Petani">Petani</option>
                                                        <option value="Pekerja Negeri Sipil (PNS)">Pekerja Negeri Sipil
                                                            (PNS)</option>
                                                        <option value="Guru">Guru</option>
                                                        <option value="Dosen">Dosen</option>
                                                        <option value="TNI/POLRI">TNI/POLRI</option>
                                                        <option value="Wirausaha">Wirausaha</option>
                                                        <option value="Wiraswasta">Wiraswasta</option>
                                                        <option value="Lainnya">Lainnya</option>
                                                    </select>

                                                    @error('pekerjaan')
                                                        <div class="text-danger">{{ $message }}</div>
                                                    @enderror
                                                </div>
                                            </div>
                                            <button type="submit" class="btn btn-primary">Simpan</button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </div>
        <x-footer></x-footer>
    </div>
@endsection
