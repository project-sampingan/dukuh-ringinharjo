@section('title', 'Kartu Keluarga')
@extends('layouts.voler')
@section('content')
    <div id="main">
        <x-navbar></x-navbar>

        <div class="main-content container-fluid">
            <div class="page-title">
                <div class="row">
                    <div class="col-12 col-md-6 order-md-1 order-last">
                        <h3>@yield('title') {{ $family->nama_kk }}</h3>
                    </div>
                    <div class="col-12 col-md-6 order-md-2 order-first">
                        <nav aria-label="breadcrumb" class='breadcrumb-header'>
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="{{ route('families.index') }}">Data Keluarga</a></li>
                                <li class="breadcrumb-item active" aria-current="page">@yield('title')</li>
                            </ol>
                        </nav>
                    </div>
                </div>
            </div>
            <section class="section">
                <div class="card">
                    <div class="card-header">
                        <!-- <a href="{{ route('cardfamily.create', $family->id) }}" class="btn icon icon-left btn-primary">
                            <i data-feather="plus"></i>
                            <span>Tambah KK</span>
                        </a> -->
                        <div class="row">
                            <div id="alert_nik_exist" class="col-lg-12"></div>
                            <div class="col-lg-6">
                                <input type="text" name="search_nik" id="search_nik"
                                class="form-control round"
                                placeholder="Masukkan Nik">
                                <input type="hidden" name="family_id" id="family_id" value="{{ $family->id }}">
                            </div>
                            <div class="col-lg-5">
                                <button type="button" class="btn btn-info btn-sm" id="search_by_nik">Tambah NIK</button>
                            </div>
                        </div>
                    </div>
                    <div class="card-body">
                        <table class='table' id="table1">
                            <thead>
                                <tr>
                                    <th>NO</th>
                                    <th>NO. REG</th>
                                    <th>NAMA ANGGOTA KELUARGA</th>
                                    <th>STATUS DLM KELUARGA</th>
                                    <th>STATUS DLM PERKAWINAN</th>
                                    <th>JENIS KELAMIN</th>
                                    <th>TGL LAHIR/UMUR</th>
                                    <th>PENDIDIKAN</th>
                                    <th>PEKERJAAN</th>
                                    <th>AKSI</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($cardfamily as $card)
                                    <tr>
                                        <td>{{ $loop->iteration }}</td>
                                        <td>{{ $card->no_reg }}</td>
                                        <td>{{ $card->nama_anggota_keluarga }}</td>
                                        <td>{{ $card->status_dlm_keluarga }}</td>
                                        <td>{{ $card->status_dlm_perkawinan }}</td>
                                        <td>{{ $card->jenis_kelamin }}</td>
                                        <td>{{ $card->lahir_umur }}</td>
                                        <td>{{ $card->pendidikan }}</td>
                                        <td>{{ $card->pekerjaan }}</td>
                                        <td>
                                            <!-- <a href="{{ route('cardfamily.edit', [$family->id, $card->id]) }}"
                                                class="btn icon icon-left btn-warning btn-sm">
                                                <i data-feather="edit"></i>
                                            </a> -->

                                            <form action="{{ route('cardfamily.destroy', [$family->id, $card->id]) }}"
                                                method="POST" class="d-inline">
                                                @csrf
                                                @method('delete')
                                                <button type="submit" class="btn icon icon-left btn-danger btn-sm"
                                                    onclick="return confirm('Yakin ingin menghapus data?')">
                                                    <i data-feather="trash"></i>
                                                </button>
                                            </form>
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>

            </section>
        </div>
        <x-footer></x-footer>
    </div>

    <script>
    $('#alert_nik_exist').css('display', 'none');
    $('#search_by_nik').click(() => {
        $('#search_by_nik').prop('disabled', true)
        $('#search_by_nik').html('')
        html = `<div class="spinner-grow spinner-grow-sm" role="status">
                    <span class="visually-hidden">Loading...</span>
                </div>`
        $('#search_by_nik').html(html)
        
        if ($('#search_nik').val() == "") {
            $('#alert_nik_exist').css('display', 'block')
            html_alert = `<div class="alert alert-danger alert-dismissible fade show" role="alert">
                                Masukkan Nik Terlebih dahulu
                                <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
                            </div>`  
            $('#alert_nik_exist').html(html_alert)
            $('#search_by_nik').prop('disabled', '')
            $('#search_by_nik').html('Tambah NIK')
            return false
        }

        $.ajax({
            method: 'POST',
            url: '{{ url("api/kartu_keluarga/tambah_anggota") }}',
            data: { 
                nik: $('#search_nik').val() ,
                family_id: $('#family_id').val()
            }
        })
        .done(function(data) {
            $('#search_by_nik').prop('disabled', '')
            $('#search_by_nik').html('Tambah NIK')
            
            $('#alert_nik_exist').css('display', 'block')
            if (data.status == false) {
                html_alert = `<div class="alert alert-danger alert-dismissible fade show" role="alert">
                                ${data.message}
                                <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
                            </div>`  
            } else {
                html_alert = `<div class="alert alert-info alert-dismissible fade show" role="alert">${data.message}</div>`
                location.reload()
            }
            $('#alert_nik_exist').html(html_alert) 
        })
    })
</script>
@endsection
