<div id="sidebar" class='active'>
    <div class="sidebar-wrapper active">
        <div class="sidebar-header">
            <img src="{{ asset('voler/assets/images/logo.svg') }}" alt="" srcset="">
        </div>
        <div class="sidebar-menu">
            <ul class="menu">
                <li class='sidebar-title'>Main Menu</li>
                <li class="sidebar-item {{ request()->is('dashboard') ? 'active' : '' }}">
                    <a href="{{ route('dashboard') }}" class='sidebar-link'>
                        <i data-feather="home" width="20"></i>
                        <span>Dashboard</span>
                    </a>
                </li>

                <li class="sidebar-item {{ request()->is('families*') ? 'active' : '' }}">
                    <a href="{{ route('families.index') }}" class='sidebar-link'>
                        <i data-feather="users" width="20"></i>
                        <span>Data Keluarga</span>
                    </a>
                </li>

                <li class="sidebar-item {{ request()->is('inhabitants*') ? 'active' : '' }}">
                    <a href="{{ route('inhabitants.index') }}" class='sidebar-link'>
                        <i data-feather="user" width="20"></i>
                        <span>Data Warga</span>
                    </a>
                </li>

                <li class="sidebar-item {{ request()->is('pendatang*') ? 'active' : '' }}">
                    <a href="{{ route('pendatang.index') }}" class='sidebar-link'>
                        <i data-feather="user-check" width="20"></i>
                        <span>Data Warga Pendatang</span>
                    </a>
                </li>

                <li class="sidebar-item {{ request()->is('masterdasawisma*') ? 'active' : '' }}">
                    <a href="{{ route('masterdasawisma.index') }}" class='sidebar-link'>
                        <i data-feather="book" width="20"></i>
                        <span>Master Dasa Wisma</span>
                    </a>
                </li>
            </ul>
        </div>
        <button class="sidebar-toggler btn x"><i data-feather="x"></i></button>
    </div>
</div>
