<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('families', function (Blueprint $table) {
            $table->id();
            $table->string('desa');
            $table->integer('rt');
            $table->integer('rw');
            $table->string('dusun');
            $table->string('kel');
            $table->string('kep');
            $table->string('kab');
            $table->string('prov');
            $table->string('nama_kk');
            $table->string('ttl_anggota');
            $table->integer('jml_lk');
            $table->integer('jml_pr');
            $table->integer('jml_kk');
            $table->integer('jml_balita');
            $table->integer('jml_anak');
            $table->integer('jml_pus');
            $table->integer('jml_wus');
            $table->integer('jml_buta');
            $table->integer('jml_bumil');
            $table->integer('jml_busu');
            $table->integer('jml_lansia');
            $table->string('makanan_pokok');
            $table->string('jamban');
            $table->string('sumber_air');
            $table->string('pembuangan_sampah');
            $table->string('pembuangan_air_limbah');
            $table->string('stiker_p4k');
            $table->string('kriteria_rumah');
            $table->string('up2k');
            $table->string('kukl');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('families');
    }
};
