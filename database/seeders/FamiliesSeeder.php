<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class FamiliesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('families')->insert([
            'desa' => 'Konohagakure',
            'rt' => '001',
            'rw' => '001',
            'dusun' => 'Api',
            'kel' => 'Manga',
            'kep' => 'Anime',
            'kab' => 'Tokyo',
            'prov' => 'Jepang',
            'nama_kk' => 'Naruto Uzumaki',
            'ttl_anggota' => '1',
            'jml_lk' => '3',
            'jml_pr' => '2',
            'jml_kk' => '1',
            'jml_balita' => '0',
            'jml_anak' => '3',
            'jml_pus' => '0',
            'jml_wus' => '0',
            'jml_buta' => '0',
            'jml_bumil' => '0',
            'jml_busu' => '0',
            'jml_lansia' => '0',
            'makanan_pokok' => 'Ramen',
            'jamban' => '2 WC Duduk',
            'sumber_air' => 'PDAM',
            'pembuangan_sampah' => 'Ada',
            'pembuangan_air_limbah' => 'Ada',
            'stiker_p4k' => 'Ada',
            'kriteria_rumah' => 'Rumah Pribadi',
            'up2k' => 'Tidak',
            'kukl' => 'Iya',
            'created_at' => now(),
            'updated_at' => now()
        ]);
    }
}
