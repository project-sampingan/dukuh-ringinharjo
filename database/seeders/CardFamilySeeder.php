<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;


class CardFamilySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('card_family')->insert([
            'family_id' => '1',
            'no_reg' => '327700000000001',
            'nama_anggota_keluarga' => 'Naruto Uzumaki',
            'status_dlm_keluarga' => 'Suami',
            'status_dlm_perkawinan' => 'Kawin',
            'jenis_kelamin' => 'Laki-laki',
            'lahir_umur' => now(),
            'pendidikan' => 'Jonin',
            'pekerjaan' => 'Hokage',
            'created_at' => now(),
            'updated_at' => now()
        ]);

        DB::table('card_family')->insert([
            'family_id' => '1',
            'no_reg' => '327700000000002',
            'nama_anggota_keluarga' => 'Hinata Hyuga',
            'status_dlm_keluarga' => 'Istri',
            'status_dlm_perkawinan' => 'Kawin',
            'jenis_kelamin' => 'Perempuan',
            'lahir_umur' => now(),
            'pendidikan' => 'Jonin',
            'pekerjaan' => 'Ibu Rumah Tangga',
            'created_at' => now(),
            'updated_at' => now()
        ]);

        DB::table('card_family')->insert([
            'family_id' => '1',
            'no_reg' => '327700000000003',
            'nama_anggota_keluarga' => 'Boruto Uzumaki',
            'status_dlm_keluarga' => 'Anak',
            'status_dlm_perkawinan' => 'Belum Kawin',
            'jenis_kelamin' => 'Laki-laki',
            'lahir_umur' => now(),
            'pendidikan' => 'Genin',
            'pekerjaan' => 'Belum Bekerja',
            'created_at' => now(),
            'updated_at' => now()
        ]);

        DB::table('card_family')->insert([
            'family_id' => '1',
            'no_reg' => '327700000000004',
            'nama_anggota_keluarga' => 'Himawari Uzumaki',
            'status_dlm_keluarga' => 'Anak',
            'status_dlm_perkawinan' => 'Belum Kawin',
            'jenis_kelamin' => 'Perempuan',
            'lahir_umur' => now(),
            'pendidikan' => 'Genin',
            'pekerjaan' => 'Belum Bekerja',
            'created_at' => now(),
            'updated_at' => now()
        ]);

        DB::table('card_family')->insert([
            'family_id' => '1',
            'no_reg' => '327700000000005',
            'nama_anggota_keluarga' => 'Doraemon',
            'status_dlm_keluarga' => 'Anak',
            'status_dlm_perkawinan' => 'Belum Kawin',
            'jenis_kelamin' => 'Laki-laki',
            'lahir_umur' => now(),
            'pendidikan' => 'Genin',
            'pekerjaan' => 'Belum Bekerja',
            'created_at' => now(),
            'updated_at' => now()
        ]);
    }
}
