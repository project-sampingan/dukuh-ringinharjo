<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Inhabitant;
use App\Models\Activity;

class ActivityController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Inhabitant $inhabitant)
    {
        return view('activitie.create', compact('inhabitant'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, Inhabitant $inhabitant, Activity $activity)
    {
        $validation = $request->validate([
            'nama_kegiatan' => 'required',
            'status' => 'required',
            'deskripsi' => 'required',
        ]);

        $activity = new Activity;
        $activity->inhabitant_id = $inhabitant->id;
        $activity->nama_kegiatan = $request->nama_kegiatan;
        $activity->status = $request->status;
        $activity->deskripsi = $request->deskripsi;
        $activity->save();

        return redirect()->route('activity.show', [$inhabitant->id, $inhabitant->id])->with('toast_success', 'Aktivitas berhasil ditambahkan');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Inhabitant $inhabitant, $id)
    {
        $inhabitant = Inhabitant::find($id);
        $activities = Activity::where('inhabitant_id', $id)->get();
        return view('activitie.show', compact('inhabitant', 'activities'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Inhabitant $inhabitant, $id)
    {
        $inhabitant = Inhabitant::find($inhabitant->id);
        $activity = Activity::find($id);
        return view('activitie.edit', compact('inhabitant', 'activity'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Inhabitant $inhabitant, $id)
    {
        $validation = $request->validate([
            'nama_kegiatan' => 'required',
            'status' => 'required',
            'deskripsi' => 'required',
        ]);

        $activity = Activity::find($id);
        $activity->inhabitant_id = $inhabitant->id;
        $activity->nama_kegiatan = $request->nama_kegiatan;
        $activity->status = $request->status;
        $activity->deskripsi = $request->deskripsi;
        $activity->save();

        return redirect()->route('activity.show', [$inhabitant->id, $inhabitant->id])->with('toast_success', 'Aktivitas berhasil diubah');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Inhabitant $inhabitant, $id)
    {
        $inhabitant = Inhabitant::find($inhabitant->id);
        $activity = Activity::find($id);
        $activity->delete();

        // dd($activity);

        return redirect()->route('activity.show' , [$inhabitant->id, $inhabitant->id])->with('toast_success', 'Aktivitas berhasil dihapus');
    }
}
