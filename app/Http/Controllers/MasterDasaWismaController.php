<?php

namespace App\Http\Controllers;

use App\Models\MasterDasaWisma;
use Illuminate\Http\Request;

class MasterDasaWismaController extends Controller
{
    
    public function index() {

        $dasawisma = MasterDasaWisma::orderBy('id', 'desc')->get();

        return view('masterdasawisma.index', compact('dasawisma'));

    }

    public function store(Request $request, MasterDasaWisma $masterDasaWisma) {

        $request->validate(['desa' => 'required']);

        $masterDasaWisma = new MasterDasaWisma();
        $masterDasaWisma->name = $request->desa;
        $masterDasaWisma->save();

        return redirect()->route('masterdasawisma.index')->with('toast_success', 'Data berhasil ditambahkan');

    }

    public function update(Request $request, $id) {

        $request->validate(['desa' => 'required']);

        if (empty($request->desa)) {
            return redirect()->route('masterdasawisma.index')->with('toast_error', 'Data gagal diupdate');
        } else {
            $masterDasaWisma = MasterDasaWisma::find($id);
            $masterDasaWisma->name = $request->desa;
            $masterDasaWisma->save();

            return redirect()->route('masterdasawisma.index')->with('toast_success', 'Data berhasil diupdate');
        }

    }

    public function destroy($id)
    {
        $masterDasaWisma = MasterDasaWisma::find($id);
        $masterDasaWisma->delete();

        return redirect()->route('masterdasawisma.index')->with('toast_success', 'Data berhasil dihapus');
    }
    
}
