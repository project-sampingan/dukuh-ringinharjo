<?php

namespace App\Http\Controllers;

use App\Models\DownloadPendatang;
use App\Models\MasterDasaWisma;
use App\Models\Pendatang;
use Illuminate\Http\Request;

class PendatangController extends Controller
{
    
    public function index()
    {
        $pendatang = Pendatang::all();
        
        return view('pendatang.index', compact('pendatang'));
    }

    public function create()
    {

        $dasawisma = MasterDasaWisma::orderBy('id', 'desc')->get();

        return view('pendatang.create', compact('dasawisma'));
    }

    public function store(Request $request, Pendatang $pendatang)
    {
        $request->validate([
            'nik' => 'required',
            'kk' => 'required',
            'nama' => 'required',
            'jenis_kelamin' => 'required',
            'tgl_lahir' => 'required',
            'provinsi' => 'required',
            'kabupaten' => 'required',
            'kecamatan' => 'required',
            'kelurahan' => 'required',
            'rt' => 'required',
            'desa' => 'required',
            'alamat_lengkap' => 'nullable',
            'domisili' => 'required',
            'tgl_masuk' => 'required',
            // 'tgl_keluar' => 'required',
            'keterangan' => 'required',
        ]);

        $pendatang->nik = $request->nik;
        $pendatang->kk = $request->kk;
        $pendatang->nama = $request->nama;
        $pendatang->nik = $request->nik;
        $pendatang->nama = $request->nama;
        $pendatang->jenis_kelamin = $request->jenis_kelamin;
        $pendatang->tgl_lahir = $request->tgl_lahir;
        $pendatang->provinsi = $request->provinsi;
        $pendatang->kabupaten = $request->kabupaten;
        $pendatang->kecamatan = $request->kecamatan;
        $pendatang->kelurahan = $request->kelurahan;
        $pendatang->rt = $request->rt;
        $pendatang->desa = $request->desa;
        $pendatang->alamat_lengkap = $request->alamat_lengkap;
        $pendatang->domisili = $request->domisili;
        $pendatang->tgl_masuk = $request->tgl_masuk;
        $pendatang->tgl_keluar = $request->tgl_keluar;
        $pendatang->rw = 0;
        $pendatang->keterangan = $request->keterangan;
        $pendatang->save();

        return redirect()->route('pendatang.index')->with('toast_success', 'Data Warga berhasil ditambahkan');
    }

    public function edit($id) {
        $pendatang = Pendatang::find($id);

        return view('pendatang.edit', compact('pendatang'));
    }

    public function update(Request $request, $id)
    {

        $request->validate([
            'nik' => 'required',
            'kk' => 'required',
            'nama' => 'required',
            'jenis_kelamin' => 'required',
            'tgl_lahir' => 'required',
            'provinsi' => 'required',
            'kabupaten' => 'required',
            'kecamatan' => 'required',
            'kelurahan' => 'required',
            'rt' => 'required',
            'desa' => 'required',
            'alamat_lengkap' => 'nullable',
            'domisili' => 'required',
            'tgl_masuk' => 'required',
            'tgl_keluar' => 'required',
            'keterangan' => 'required',
        ]);

        $pendatang = Pendatang::find($id);
        $pendatang->nik = $request->nik;
        $pendatang->kk = $request->kk;
        $pendatang->nama = $request->nama;
        $pendatang->nik = $request->nik;
        $pendatang->nama = $request->nama;
        $pendatang->jenis_kelamin = $request->jenis_kelamin;
        $pendatang->tgl_lahir = $request->tgl_lahir;
        $pendatang->provinsi = $request->provinsi;
        $pendatang->kabupaten = $request->kabupaten;
        $pendatang->kecamatan = $request->kecamatan;
        $pendatang->kelurahan = $request->kelurahan;
        $pendatang->rt = $request->rt;
        $pendatang->desa = $request->desa;
        $pendatang->alamat_lengkap = $request->alamat_lengkap;
        $pendatang->domisili = $request->domisili;
        $pendatang->tgl_masuk = $request->tgl_masuk;
        $pendatang->tgl_keluar = $request->tgl_keluar;
        $pendatang->rw = 0;
        $pendatang->keterangan = $request->keterangan;
        $pendatang->save();

        return redirect()->route('pendatang.index')->with('toast_success', 'Data Warga Pendatang berhasil diubah');
    }

    public function show($id)
    {
        $pendatang = Pendatang::find($id);

        return view('pendatang.detail', compact('pendatang'));
    }

    public function download()
    {
        return (new DownloadPendatang)->download('pendatang.xlsx');
    }
    
}