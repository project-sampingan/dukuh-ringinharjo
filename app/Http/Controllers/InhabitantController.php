<?php

namespace App\Http\Controllers;

use App\Models\DownloadInhabitant;
use App\Models\MasterDasaWisma;
use Illuminate\Http\Request;
use App\Models\Inhabitant;
use App\Models\Activity;
use RealRashid\SweetAlert\Facades\Alert;

class InhabitantController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $inhabitants = Inhabitant::all();
        return view('inhabitant.index', compact('inhabitants'));
    }

    public function download()
    {
        return (new DownloadInhabitant)->download('inhabitant.xlsx');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

        $dasawisma = MasterDasaWisma::orderBy('id', 'desc')->get();

        return view('inhabitant.create', compact('dasawisma'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, Inhabitant $inhabitant)
    {
        $validation = $request->validate([
            'desa' => 'required',
            // 'nama_kk' => 'required',
            'no_reg' => 'required',
            'nik' => 'required',
            'nama' => 'required',
            'jabatan' => 'nullable',
            'jenis_kelamin' => 'required',
            'tempat_lahir' => 'required',
            'tanggal_lahir' => 'required',
            'status_kawin' => 'required',
            'status_dlm_keluarga' => 'required',
            'agama' => 'required',
            'alamat' => 'required',
            'pendidikan' => 'required',
            'pekerjaan' => 'required',
            'akseptor_kb' => 'required',
            'posyandu' => 'required',
            'bina_keluarga_balita' => 'required',
            'paud' => 'required',
            'koperasi' => 'required',
        ]);

        $inhabitant = new Inhabitant();
        $inhabitant->desa = $request->desa;
        $inhabitant->nama_kk = $request->nama_kk;
        $inhabitant->no_reg = $request->no_reg;
        $inhabitant->nik = $request->nik;
        $inhabitant->nama = $request->nama;
        $inhabitant->jabatan = $request->jabatan;
        $inhabitant->jenis_kelamin = $request->jenis_kelamin;
        $inhabitant->tempat_lahir = $request->tempat_lahir;
        $inhabitant->tanggal_lahir = $request->tanggal_lahir;
        $inhabitant->status_kawin = $request->status_kawin;
        $inhabitant->status_dlm_keluarga = $request->status_dlm_keluarga;
        $inhabitant->agama = $request->agama;
        $inhabitant->alamat = $request->alamat;
        $inhabitant->pendidikan = $request->pendidikan;
        $inhabitant->pekerjaan = $request->pekerjaan;
        $inhabitant->akseptor_kb = $request->akseptor_kb;
        $inhabitant->posyandu = $request->posyandu;
        $inhabitant->bina_keluarga_balita = $request->bina_keluarga_balita;
        $inhabitant->paud = $request->paud;
        $inhabitant->koperasi = $request->koperasi;
        $inhabitant->save();

        return redirect()->route('inhabitants.index')->with('toast_success', 'Data Warga berhasil ditambahkan');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $inhabitants = Inhabitant::find($id);
        $activities = Activity::where('inhabitant_id', $id)->get();
        $dateofbirth = $inhabitants->tanggal_lahir;
        $age = date('Y') - date('Y', strtotime($dateofbirth));

        return view('inhabitant.show', compact('inhabitants', 'activities', 'age'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $inhabitant = Inhabitant::find($id);
        $dasa_wisma = MasterDasaWisma::query()->where('name', '!=', $inhabitant->desa)->get();

        return view('inhabitant.edit', compact('inhabitant','dasa_wisma'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validation = $request->validate([
            'desa' => 'required',
            'nama_kk' => 'required',
            'no_reg' => 'required',
            'nik' => 'required',
            'nama' => 'required',
            'jabatan' => 'required',
            'jenis_kelamin' => 'required',
            'tempat_lahir' => 'required',
            'tanggal_lahir' => 'required',
            'status_kawin' => 'required',
            'status_dlm_keluarga' => 'required',
            'agama' => 'required',
            'alamat' => 'required',
            'pendidikan' => 'required',
            'pekerjaan' => 'required',
            'akseptor_kb' => 'required',
            'posyandu' => 'required',
            'bina_keluarga_balita' => 'required',
            'paud' => 'required',
            'koperasi' => 'required',
        ]);

        $inhabitant = Inhabitant::find($id);
        $inhabitant->desa = $request->desa;
        $inhabitant->nama_kk = $request->nama_kk;
        $inhabitant->no_reg = $request->no_reg;
        $inhabitant->nik = $request->nik;
        $inhabitant->nama = $request->nama;
        $inhabitant->jabatan = $request->jabatan;
        $inhabitant->jenis_kelamin = $request->jenis_kelamin;
        $inhabitant->tempat_lahir = $request->tempat_lahir;
        $inhabitant->tanggal_lahir = $request->tanggal_lahir;
        $inhabitant->status_kawin = $request->status_kawin;
        $inhabitant->status_dlm_keluarga = $request->status_dlm_keluarga;
        $inhabitant->agama = $request->agama;
        $inhabitant->alamat = $request->alamat;
        $inhabitant->pendidikan = $request->pendidikan;
        $inhabitant->pekerjaan = $request->pekerjaan;
        $inhabitant->akseptor_kb = $request->akseptor_kb;
        $inhabitant->posyandu = $request->posyandu;
        $inhabitant->bina_keluarga_balita = $request->bina_keluarga_balita;
        $inhabitant->paud = $request->paud;
        $inhabitant->koperasi = $request->koperasi;
        $inhabitant->save();

        return redirect()->route('inhabitants.index')->with('toast_success', 'Data Warga berhasil diubah');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $inhabitant = Inhabitant::find($id);
        $inhabitant->delete();

        return redirect()->route('inhabitants.index')->with('toast_success', 'Data Warga berhasil dihapus');
    }
}
