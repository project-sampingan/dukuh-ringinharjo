<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\CardFamily;
use App\Models\Inhabitant;
use Illuminate\Http\Request;
use Zend\Diactoros\Response;

class KartuKeluargaApi extends Controller
{

    public function tambah_anggota(Request $request) {

        $cek_exist = CardFamily::query()->where('nik', $request->nik)->first();

        if ($cek_exist) {
            return Response([
                'status' => false,
                'message' => 'Maaf, Nik sudah di tambah',
                'data' => $cek_exist
            ]);
        }
        $query = Inhabitant::query();
        $query->where('nik', $request->nik);
        $warga = $query->limit(1)->first();

        if ($warga) {
            
            $card_family = new CardFamily();
            $card_family->family_id = $request->family_id;
            $card_family->no_reg = $warga->no_reg;
            $card_family->nik = $warga->nik;
            $card_family->nama_anggota_keluarga = $warga->nama;
            $card_family->status_dlm_keluarga = $warga->status_dlm_keluarga;
            $card_family->status_dlm_perkawinan = $warga->status_kawin;
            $card_family->jenis_kelamin = $warga->jenis_kelamin;
            $card_family->lahir_umur = $warga->tanggal_lahir;
            $card_family->pendidikan = $warga->pendidikan;
            $card_family->pekerjaan = $warga->pekerjaan;
            $card_family->save();

            return Response([
                'status' => true,
                'message' => 'Berhasil tambah data anggota keluarga',
                'data' => $card_family->id
            ]);
            
        } else {

            return Response([
                'status' => false,
                'message' => 'Data Warga tidak di temukan, silahkan tambah data di menu data warga',
                'data' => null
            ]);

        }

    }
    
}
