<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\Inhabitant;
use App\Models\Pendatang;
use Illuminate\Http\Request;
use Zend\Diactoros\Response;

class WargaApi extends Controller
{
    
    public function get_one(Request $request) {

        $query = Inhabitant::query();
        $query->where('nik', $request->cari);
        if ($request->cari) {
           $query->orWhere('nama', 'LIKE', '%' . $request->cari . '%');
        }
        $result = $query->get();

        return Response([
            'status' => true,
            'message' => 'success get data',
            'data' => $result
        ]);

    }

    public function by_nik(Request $request) {
        
        $query = Inhabitant::query();
        $query->where('nik', $request->nik);
        $result = $query->limit(1)->first();

        return Response([
            'status' => true,
            'message' => 'success get data',
            'data' => $result
        ]);

    }

    public function get_province(Request $request) {
        $curl = curl_init();

        $param = '?key=83a09db8abeafa00b2569d713890b0c7';

        if (!empty($request->id)) {
            $param .= "&id=" . $request->id;
        }

        curl_setopt_array($curl, array(
            // CURLOPT_URL => "http://api.rajaongkir.com/starter/province" . $param,
            CURLOPT_URL => "http://api.iksgroup.co.id/apilokasi/provinsi" . $param,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "GET",
            // CURLOPT_HTTPHEADER => $param,
            // array(
            //     "key: 83a09db8abeafa00b2569d713890b0c7"
            // ),
        ));

        $response = curl_exec($curl);
        $err = curl_error($curl);

        curl_close($curl);

        if (!empty($response)) {
            return response([
                'status' => true,
                'message' => 'data success get',
                // 'data' => json_decode($response)->rajaongkir->results
                'data' => json_decode($response)->data
            ]);
        } else {
            return response([
                'status' => false,
                'message' => 'failed get data',
                'data' => $err
            ]);
        }
    }

    public function get_kabupaten(Request $request) {
        $curl = curl_init();

        $param = '?key=83a09db8abeafa00b2569d713890b0c7';
        $param .= (!empty($request->id)) ? "&id=" . $request->id : "" ;
        $param .= (!empty($request->province)) ? "&provinsi=" . $request->province : "" ;

        curl_setopt_array($curl, array(
            CURLOPT_URL => "http://api.iksgroup.co.id/apilokasi/kabupaten" . $param,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "GET",
        ));

        $response = curl_exec($curl);
        $err = curl_error($curl);

        curl_close($curl);

        if (!empty($response)) {
            return response([
                'status' => true,
                'message' => 'data success get',
                //'data' => json_decode($response)->rajaongkir->results
                'data' => json_decode($response)->data
            ]);
        } else {
            return response([
                'status' => false,
                'message' => 'failed get data',
                'data' => $err
            ]);
        }
    }

    public function get_kecamatan(Request $request) {
        $curl = curl_init();

        $param = (!empty($request->kabupaten)) ? "?kabupaten=" . $request->kabupaten : "" ;

        curl_setopt_array($curl, array(
            CURLOPT_URL => "http://api.iksgroup.co.id/apilokasi/kecamatan" . $param,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "GET",
        ));

        $response = curl_exec($curl);
        $err = curl_error($curl);

        curl_close($curl);

        if (!empty(json_decode($response)->data)) {
            return response([
                'status' => true,
                'message' => 'data success get',
                'data' => json_decode($response)->data
            ]);
        } else {
            return response([
                'status' => false,
                'message' => 'failed get data',
                'data' => $err
            ]);
        }
    }

    public function get_kelurahan(Request $request) {
        $curl = curl_init();

        $param = (!empty($request->kecamatan)) ? "?kecamatan=" . $request->kecamatan : "" ;

        curl_setopt_array($curl, array(
            CURLOPT_URL => "http://api.iksgroup.co.id/apilokasi/kelurahan" . $param,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "GET",
        ));

        $response = curl_exec($curl);
        $err = curl_error($curl);

        curl_close($curl);

        if (!empty(json_decode($response)->data)) {
            return response([
                'status' => true,
                'message' => 'data success get',
                'data' => json_decode($response)->data
            ]);
        } else {
            return response([
                'status' => false,
                'message' => 'failed get data',
                'data' => $err
            ]);
        }
    }

    public function hapus_warga_pendatang(Request $request)
    {

        if (empty($request->id)) {
            return response([
                'status' => false,
                'message' => 'gagal hapus data warga pendatang',
                'data' => null
            ]);
        }
        
        $pendatang = Pendatang::find($request->id);
        $pendatang->delete();

        return response([
            'status' => true,
            'message' => 'Data Warga berhasil dihapus',
            'data' => null
        ]);
    }
    
}
