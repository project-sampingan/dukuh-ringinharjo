<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\CardFamily;
use App\Models\Family;
use RealRashid\SweetAlert\Facades\Alert;

class CardFamilyController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Family $family)
    {
        return view('cardfamily.create', compact('family'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, Family $family, CardFamily $cardfamily)
    {
        $validation = $request->validate([
            'no_reg' => 'required',
            'nama_anggota_keluarga' => 'required|',
            'status_dlm_keluarga' => 'required',
            'status_dlm_perkawinan' => 'required',
            'jenis_kelamin' => 'required',
            'lahir_umur' => 'required',
            'pendidikan' => 'required',
            'pekerjaan' => 'required',
        ]);

        $family = Family::find($family->id);
        $cardfamily = new CardFamily();
        $cardfamily->family_id = $family->id;
        $cardfamily->no_reg = $request->no_reg;
        $cardfamily->nama_anggota_keluarga = $request->nama_anggota_keluarga;
        $cardfamily->status_dlm_keluarga = $request->status_dlm_keluarga;
        $cardfamily->status_dlm_perkawinan = $request->status_dlm_perkawinan;
        $cardfamily->jenis_kelamin = $request->jenis_kelamin;
        $cardfamily->lahir_umur = $request->lahir_umur;
        $cardfamily->pendidikan = $request->pendidikan;
        $cardfamily->pekerjaan = $request->pekerjaan;
        $cardfamily->save();

        return redirect()->route('cardfamily.show', [$family->id, $family->id])->with('toast_success', 'Data berhasil ditambahkan');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Family $family ,$id)
    {
        $family = Family::find($family->id);
        $cardfamily = CardFamily::where('family_id', $id)->get();
        return view('cardfamily.show', compact('family', 'cardfamily'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Family $family ,$id)
    {
        $family = Family::find($family->id);
        $cardfamily = CardFamily::find($id);

        return view('cardfamily.edit', compact('family', 'cardfamily'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Family $family, $id)
    {
        $validation = $request->validate([
            'no_reg' => 'required',
            'nama_anggota_keluarga' => 'required|',
            'status_dlm_keluarga' => 'required',
            'status_dlm_perkawinan' => 'required',
            'jenis_kelamin' => 'required',
            'lahir_umur' => 'required',
            'pendidikan' => 'required',
            'pekerjaan' => 'required',
        ]);

        $family = Family::find($family->id);
        $cardfamily = CardFamily::find($id);
        $cardfamily->family_id = $family->id;
        $cardfamily->no_reg = $request->no_reg;
        $cardfamily->nama_anggota_keluarga = $request->nama_anggota_keluarga;
        $cardfamily->status_dlm_keluarga = $request->status_dlm_keluarga;
        $cardfamily->status_dlm_perkawinan = $request->status_dlm_perkawinan;
        $cardfamily->jenis_kelamin = $request->jenis_kelamin;
        $cardfamily->lahir_umur = $request->lahir_umur;
        $cardfamily->pendidikan = $request->pendidikan;
        $cardfamily->pekerjaan = $request->pekerjaan;
        $cardfamily->save();

        return redirect()->route('cardfamily.show', [$family->id, $family->id])->with('toast_success', 'Data Berhasil Diubah');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Family $family, $id)
    {
        $family = Family::find($family->id);
        $cardfamily = CardFamily::find($id);
        $cardfamily->delete();

        return redirect()->route('cardfamily.show', [$family->id, $family->id])->with('toast_success', 'Data Berhasil Dihapus');
    }
}
