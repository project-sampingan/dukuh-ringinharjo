<?php

namespace App\Http\Controllers;

use App\Models\DownloadFamily;
use App\Models\Family;
use App\Models\CardFamily;
use App\Models\Inhabitant;
use App\Models\MasterDasaWisma;
use Illuminate\Http\Request;
// use Maatwebsite\Excel\Facades\Excel;
use RealRashid\SweetAlert\Facades\Alert;

class FamilyController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $families = Family::all();
        $lk = Family::sum('jml_lk');
        $pr = Family::sum('jml_pr');
        return view('families.index', compact('families', 'lk', 'pr'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $dasawisma = MasterDasaWisma::orderBy('id', 'desc')->get();
        
        return view('families.create', compact('dasawisma'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, Family $family)
    {
        $validation = $request->validate([
            'desa' => 'required',
            'rt' => 'required',
            'rw' => 'required',
            'dusun' => 'required',
            'kel' => 'required',
            // 'kep' => 'required',
            // 'kab' => 'required',
            // 'prov' => 'required',
            'nama_kk' => 'required',
            'ttl_anggota' => 'required',
            'jml_lk' => 'required',
            'jml_pr' => 'required',
            'jml_kk' => 'required',
            'jml_balita' => 'required',
            'jml_anak' => 'required',
            'jml_pus' => 'required',
            'jml_wus' => 'required',
            'jml_buta' => 'required',
            'jml_bumil' => 'required',
            'jml_busu' => 'required',
            'jml_lansia' => 'required',
            'makanan_pokok' => 'required',
            'jamban' => 'required',
            'sumber_air' => 'required',
            'pembuangan_sampah' => 'required',
            'pembuangan_air_limbah' => 'required',
            'stiker_p4k' => 'required',
            'kriteria_rumah' => 'required',
            'up2k' => 'required',
            'kukl' => 'required',
        ]);

        $family = new Family();
        $family->desa = $request->desa;
        $family->rt = $request->rt;
        $family->rw = $request->rw;
        $family->dusun = $request->dusun;
        $family->kel = $request->kel;
        $family->kep = ($request->kep) ?: "Ringinharjo";
        $family->kab = ($request->kab) ?: "Bantul";
        $family->prov = ($request->prov) ?: "Di Yogyakarta" ;
        $family->nama_kk = $request->nama_kk;
        $family->ttl_anggota = $request->ttl_anggota;
        $family->jml_lk = $request->jml_lk;
        $family->jml_pr = $request->jml_pr;
        $family->jml_kk = $request->jml_kk;
        $family->jml_balita = $request->jml_balita;
        $family->jml_anak = $request->jml_anak;
        $family->jml_pus = $request->jml_pus;
        $family->jml_wus = $request->jml_wus;
        $family->jml_buta = $request->jml_buta;
        $family->jml_bumil = $request->jml_bumil;
        $family->jml_busu = $request->jml_busu;
        $family->jml_lansia = $request->jml_lansia;
        $family->makanan_pokok = $request->makanan_pokok;
        $family->jamban = $request->jamban;
        $family->sumber_air = $request->sumber_air;
        $family->pembuangan_sampah = $request->pembuangan_sampah;
        $family->pembuangan_air_limbah = $request->pembuangan_air_limbah;
        $family->stiker_p4k = $request->stiker_p4k;
        $family->kriteria_rumah = $request->kriteria_rumah;
        $family->up2k = $request->up2k;
        $family->kukl = $request->kukl;
        $family->save();

        $warga = Inhabitant::find($request->id_kk);
        $cardfamily = new CardFamily();
        $cardfamily->family_id = $family->id;
        $cardfamily->no_reg = $warga->no_reg;
        $cardfamily->nik = $warga->nik;
        $cardfamily->nama_anggota_keluarga = $request->nama_kk;
        $cardfamily->status_dlm_keluarga = $warga->status_dlm_keluarga;
        $cardfamily->status_dlm_perkawinan = $warga->status_kawin;
        $cardfamily->jenis_kelamin = $warga->jenis_kelamin;
        $cardfamily->lahir_umur = $warga->tanggal_lahir;
        $cardfamily->pendidikan = $warga->pendidikan;
        $cardfamily->pekerjaan = $warga->pekerjaan;
        $cardfamily->save();

        return redirect()->route('families.index')->with('toast_success', 'Data berhasil ditambahkan');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $family = Family::find($id);
        $cardfamily = CardFamily::where('family_id', $id)->get();
        return view('families.show', compact('family', 'cardfamily'));
    }

    public function download()
    {
        return (new DownloadFamily)->download('family.xlsx');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $family = Family::find($id);
        return view('families.edit', compact('family'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validation = $request->validate([
            'desa' => 'required',
            'rt' => 'required',
            'rw' => 'required',
            'dusun' => 'required',
            'kel' => 'required',
            'kep' => 'required',
            'kab' => 'required',
            'prov' => 'required',
            'nama_kk' => 'required',
            'ttl_anggota' => 'required',
            'jml_lk' => 'required',
            'jml_pr' => 'required',
            'jml_kk' => 'required',
            'jml_balita' => 'required',
            'jml_anak' => 'required',
            'jml_pus' => 'required',
            'jml_wus' => 'required',
            'jml_buta' => 'required',
            'jml_bumil' => 'required',
            'jml_busu' => 'required',
            'jml_lansia' => 'required',
            'makanan_pokok' => 'required',
            'jamban' => 'required',
            'sumber_air' => 'required',
            'pembuangan_sampah' => 'required',
            'pembuangan_air_limbah' => 'required',
            'stiker_p4k' => 'required',
            'kriteria_rumah' => 'required',
            'up2k' => 'required',
            'kukl' => 'required',
        ]);

        $family = Family::find($id);
        $family->desa = $request->desa;
        $family->rt = $request->rt;
        $family->rw = $request->rw;
        $family->dusun = $request->dusun;
        $family->kel = $request->kel;
        $family->kep = $request->kep;
        $family->kab = $request->kab;
        $family->prov = $request->prov;
        $family->nama_kk = $request->nama_kk;
        $family->ttl_anggota = $request->ttl_anggota;
        $family->jml_lk = $request->jml_lk;
        $family->jml_pr = $request->jml_pr;
        $family->jml_kk = $request->jml_kk;
        $family->jml_balita = $request->jml_balita;
        $family->jml_anak = $request->jml_anak;
        $family->jml_pus = $request->jml_pus;
        $family->jml_wus = $request->jml_wus;
        $family->jml_buta = $request->jml_buta;
        $family->jml_bumil = $request->jml_bumil;
        $family->jml_busu = $request->jml_busu;
        $family->jml_lansia = $request->jml_lansia;
        $family->makanan_pokok = $request->makanan_pokok;
        $family->jamban = $request->jamban;
        $family->sumber_air = $request->sumber_air;
        $family->pembuangan_sampah = $request->pembuangan_sampah;
        $family->pembuangan_air_limbah = $request->pembuangan_air_limbah;
        $family->stiker_p4k = $request->stiker_p4k;
        $family->kriteria_rumah = $request->kriteria_rumah;
        $family->up2k = $request->up2k;
        $family->kukl = $request->kukl;
        $family->save();

        return redirect()->route('families.index')
            ->with('toast_success', 'Data berhasil diubah');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $family = Family::find($id);
        $family->delete();

        return redirect()->route('families.index')
            ->with('toast_success', 'Data berhasil dihapus');
    }
}
