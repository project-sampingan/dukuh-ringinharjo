<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class CardFamily extends Model
{
    use HasFactory;

    protected $table = 'card_family';

    public function family()
    {
        return $this->belongsTo(Family::class);
    }

}
