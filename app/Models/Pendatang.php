<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Pendatang extends Model
{
    use HasFactory;

    protected $table = "pendatang";

    public function pendatang()
    {
        return $this->belongsTo(Pendatang::class);
    }

}
