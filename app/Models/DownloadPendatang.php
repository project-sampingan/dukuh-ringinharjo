<?php

namespace App\Models;

use Maatwebsite\Excel\Concerns\FromQuery;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithMapping;

class DownloadPendatang implements FromQuery, WithHeadings, WithMapping
{
    use Exportable;

    public function query()
    {
        return Pendatang::query()->select('nik', 'kk', 'nama', 'rt', 'tgl_masuk', 'tgl_keluar');
    }
	/**
	 * @return array
	 */
	public function headings(): array
    {
        return [
            'NIK',
            'KK',
            'Nama',
            'RT',
            'Tanggal Masuk',
            'Tanggal Keluar',
            'Lama Tinggal'
        ];
	}
	/**
	 * @param mixed $row
	 * @return array
	 */
	public function map($row): array
    {
        if ($row->tgl_keluar) {
            $lama_tinggal = date_diff(date_create($row->tgl_masuk), date_create($row->tgl_keluar))->d;
        } else {
            $lama_tinggal = date_diff(date_create($row->tgl_masuk), date_create())->d;
        }
        return [
            $row->nik,
            $row->kk,
            $row->nama,
            $row->rt,
            $row->tgl_masuk,
            $row->tgl_keluar,
            $lama_tinggal . " hari",
        ];
	}
}