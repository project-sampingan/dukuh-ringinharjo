<?php

namespace App\Models;

use Maatwebsite\Excel\Concerns\FromQuery;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\WithHeadings;

class DownloadFamily implements FromQuery, WithHeadings
{
    use Exportable;

    public function query()
    {
        return Family::query()->select('desa', 'rt', 'rw', 'nama_kk');
    }

    public function headings(): array
    {
        return [
            'Dasa Wisma',
            'RT',
            'RW',
            'Nama Kepala Keluarga',
        ];
	}

}