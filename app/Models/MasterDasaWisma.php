<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class MasterDasaWisma extends Model
{
    use HasFactory;

    protected $table = "m_dasawisma";

    public function masterdasawisma() 
    {
        return $this->belongsTo(MasterDasaWisma::class);
    }

}
