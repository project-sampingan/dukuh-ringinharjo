<?php

namespace App\Models;

use Maatwebsite\Excel\Concerns\FromQuery;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\WithHeadings;

class DownloadInhabitant implements FromQuery, WithHeadings
{
    use Exportable;

    public function query()
    {
        return Inhabitant::query()->select('desa', 'no_reg', 'nama', 'jabatan');
    }

    public function headings(): array
    {
        return [
            'Dasa Wisma',
            'Nomor Registrasi',
            'Nama',
            'Jabatan',
        ];
	}

}